package com.awakenwhitelabeling.others;

import android.content.Context;
import android.content.Intent;


import com.awakenwhitelabeling.base.BaseActivity;

public class IntentUtils {

    public static Intent getIntent(Context context, Class<? extends BaseActivity> toClass){
        return new Intent(context, toClass.getClass());
    }

//    @Nullable
//    public static Intent getIntent(@NotNull MyFirebaseMessagingService myFirebaseMessagingService, @NotNull KClass<NotificationActivity> kClass) {
//        return null;
//    }
}
