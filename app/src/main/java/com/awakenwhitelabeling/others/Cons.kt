package com.awakenwhitelabeling.others

import com.awakenwhitelabeling.R
import java.text.SimpleDateFormat
import java.util.*

class Cons {
    companion object {
        const val VALID_URL = "Url is not valid"
        const val HYPER_TITLE = "Title Required"
        const val SELECTION_TYPE = "Please select one of them"
        const val SELECT_DATE_TIME = "Please select data and time"
        const val NOTE_EMPTY_MSG = "Notes can not be empty."
        const val FREE = "free"
        const val GOOGLE_IN_APP = "google-inapp"
        const val IOS_IN_APP = "ios-inapp"
        const val ANDROID: String = "android"
        const val GOOGLE_INAPP: String = "google-inapp"
        const val Message = "message"
        const val MSGCount = "msg_count"
        const val FCM_TOKEN = "fcm_token"
        val NA: CharSequence = "N/A"
        const val REWARDS = "REWARDS"
        //const val BASE_URL = "http://awaken.kitlabs.us/" // phase 3 dv


        const val IMAGE_URL = "/Awaken"
        const val INPROGRESSMSG = "Please complete the In-Progress task to start the new one."
        const val NO_ACTION = -1
        const val WEB_TITLE = "WEB_TITLE"
        const val PRIVACY_POLICY_TITLE = "PRIVACY_POLICY_TITLE"
        const val WEB_URL = "WEB_URL"
        const val NOTE_DETAIL = "NOTE_DETAIL"
        const val NOTES_DESCRIPTION = "notes_description"
        const val PUBLISH_DATE = "publish_date"
        const val POST_TYPE = "post_type"
        const val STATUS = "status"
        const val CATEGORY = "category"
        const val NOTE_ID = "note_id"
        const val PAUSE_VIDEO = "PAUSE_VIDEO"
        const val InvalideURL = "Add Media not configured. Please try again later!"


        //Register
        const val LOGIN_TYPE = "register_type"
        const val EMAIL = "email"
        const val PASSWORD = "password"
        const val IBONumber = "ibo_number"
        const val PHONE = "phone"
        const val FNAME = "first_name"
        const val LNAME = "last_name"
        const val NATIVE = "Native"
        const val FACEBOOK = "facebook"
        const val FACEBOOK_FB = "Facebook"

        const val GMAIL = "gmail"
        const val videoName = "videoName"
        const val LINKEDIN = "linkedIn"
        const val GOOGLE_SIGN_IN = 1001
        const val WorkoutDetail = "data.workout"
        const val WorkoutName = "data.workout.name"
        const val WorkoutDescription = "data.workout.desc"
        const val WorkoutImage = "data.workout.img"
        const val WorkoutFeatureVideo = "data.workout.video"
        const val WorkoutRemoteVideo = "data.workout.remoteVideo"
        const val workoutid = "workout_id"
        const val sessionData = "sessionData"
        const val sessionNumber = "sessionNumber"
        const val fragContainerId = R.id.fragment_container


        //"created_at": "2020-08-05 00:49:57"
        const val IMAGE_DIRECTORY = "/Awaken"

        const val username = "user_login"
        const val firstName = "firstName"
        const val lastName = "lastName"
        const val user_email = "user_email"
        const val token = "token"
        const val logintype = "logintype"
        const val IS_MEMBERSHIP_NOTPURCHASED = "is_membership_not_purchased"
        const val phone = "phone"
        const val countryCode = "countryCode"
        const val Order_id = "orderID"
        const val MembershipDetail = "MembershipDetail"
        const val MEMBERSHIPID = "MembershipiD"
        const val MEMBERSHIP_AMOUNT = "MEMBERSHIP_AMOUNT"
        const val MEMBERSHIP_Frequency = "MEMBERSHIP_Frequency"
        const val AMOUNT = "AMOUNt"
        const val PAYMENT_RESPONSE = 14566
        const val TRANSACTIONID = "TRANSACTIONID"
        const val DROP_IN_REQUEST = 1
        const val MEMBERSHIP_END_DATE = "membership_Enddate"
        const val isExpired = "isExpired"
        const val id = "id"
        const val image_path = "image_path"
        const val socialLogID = "socialLogID"

        const val CartPaymentDetail = "CartPaymentDetail"

        const val COUPON_RESPONSE = "coupon_response"
        const val IS_COUPON_APPLIED = "is_cpn_applied"
        const val BESTSELLING = "Best Selling"
        const val FEATURED = "Featured"
        const val SALESPRODUCT = "Sale"

        const val MEMBERSHIP_DATA = "MEMBERSHIP_DATA"
        /* YouTube link matcher*/
        const val YOUTUBE_LINK_MATCHER = "https://www.youtube.com/watch?v=";
        const val VIMEO_LINK_MATCHER = "vimeo.com";
        const val SHORT_YOUTUBE_LINK_MATCHER = "https://youtu.be/";
        const val GOOGLE_DRIVE_VIDEO = "drive.google.com";

        const val VIMEO_URL = "VIMEO_URL"
        const val IMAGE_AVAILABLE = R.drawable.ic_image_available
        const val IMAGE_BROKEN = R.drawable.ic_broken_image_300
        const val USER_IMAGE = R.drawable.image_profile
        const val COMPLETED = "Completed"
        const val REDEEM = ""
        const val FACEBOOK_MESSENGER = "Facebook Messenger"
        const val PERSONAL_FACEBOOK_PROFILE = "Personal Facebook Profile"
        const val NOTES = ""
        const val NOTES_CATEGORY = ""
        const val NOTES_IMAGESOURCE = ""
        const val Notes_Data = "notes_data"
        const val FACEBOOK_GROUP = ""

        /*Rewards data from adapter to RewardsRedeemfragment*/
        const val ID = "ID"
        const val COIN = "COIN"
        const val DESC = "DESC"
        const val IMG = "IMG"
        const val PUBLISH_NOW = "publish"
        const val NOTEPUBLISH_TYPE = "Publish"
        const val NOTESCHEDULE_TYPE = "Schedule"
        const val SCHEDULE = "scheduled"
        const val BLANK = ""
        const val Notification = "notification"


        val INPUT_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val OUTPUT_DATE_FORMAT = SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault())
        val INPUT_DATE_TIME_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val CHILD_TEMP_INPUT_TIME_FORMAT = SimpleDateFormat("'at' hh:mm a", Locale.getDefault())
        val INPUT_DATE_FORMAT1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val INPUT_TIME_FORMAT1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val OUTPUT_DATE_FORMAT1 = SimpleDateFormat("MMM dd, yy", Locale.getDefault())
        val USER_DATE_FORMAT = SimpleDateFormat("EEE dd, MM 'at' hh:mm a", Locale.getDefault())
        val SERVER_POST_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

        //  val SERVER_POST_FORMAT_LOCAL = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", CalendarContract.Instance)
        val REPORT_CREATED_AT_FORMAT =
            SimpleDateFormat("'Created at' dd, MMMM yyyy", Locale.getDefault())


    }
}