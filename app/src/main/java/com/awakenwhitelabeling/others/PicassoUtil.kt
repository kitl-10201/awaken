package com.awakenwhitelabeling.others
//
//import android.text.TextUtils
//import android.widget.ImageView
//import androidx.annotation.DrawableRes
//
//object PicassoUtil {
//
//    const val COMMUNITY_NULL_IMAGE: String = "set_user_pic"
//    private val glide: RequestManager = Glide.with(App.get())
//
//    fun loadImage(image: ImageView, url: String?) {
//        glide
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.IMAGE_BROKEN)
//            .into(image);
//    }
//
//    fun loadInboxImage(image: ImageView, url: String?) {
//        glide
//            .load(url)
//            .centerCrop()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.IMAGE_BROKEN)
//            .into(image);
//
//        /*Picasso.get()
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .fit()
//            .noFade()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.IMAGE_BROKEN)
//            .into(image)*/
//    }
//
//    fun loadCommonImage(image: ImageView, url: String?) {
//        glide
//            .load(url)
//            .centerCrop()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.IMAGE_BROKEN)
//            .into(image);
//    }
//
//    fun loadProfileImage(image: ImageView, url: String?) {
//
//        glide
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.USER_IMAGE)
//            .into(image);
//        /*Picasso.get()
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .fit()
//            .noFade()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.USER_IMAGE)
//            .into(image)*/
//    }
//
//    fun loadUserImage(image: ImageView, url: String?) {
//        if (Utils.isEmptyString(url)) {
//            Utils.viewGone(image)
//            return
//        }
//        glide
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.USER_IMAGE)
//            .into(image);
//
//        /*Picasso.get()
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .fit()
//            .noFade()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.USER_IMAGE)
//            .into(image)*/
//    }
//
//    fun loadCommunityUserPicImage(image: ImageView, url: String?) {
//        if (TextUtils.equals(url, COMMUNITY_NULL_IMAGE)) {
//            image.setImageResource(Cons.USER_IMAGE)
//            return
//        }
//
//        if (Utils.isEmptyString(url)) {
//            Utils.viewGone(image)
//            return
//        }
//        glide
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.USER_IMAGE)
//            .into(image);
//
//        /*Picasso.get()
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .fit()
//            .noFade()
//            .placeholder(R.drawable.progress_circle)
//            .error(Cons.USER_IMAGE)
//            .into(image)*/
//    }
//
//
//    fun loadImageWithLogo(image: ImageView, url: String?) {
//        glide
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .placeholder(R.drawable.progress_circle)
//            .error(R.drawable.app_logo_transparent_512)
//            .into(image);
//        /*Picasso.get()
//            .load(Utils.getWorkoutImageUrl(url))
//            .centerCrop()
//            .fit()
//            .noFade()
//            .placeholder(R.drawable.progress_circle)
//            .error(R.drawable.app_logo_transparent_512)
//            .into(image)*/
//    }
//
//    fun loadDrawableImage(ivImage: ImageView, @DrawableRes userImage: Int) {
//
//        glide
//            .load(userImage)
//            .centerCrop()
//            .into(ivImage);
//        /*Picasso.get()
//            .load(userImage)
//            .centerCrop()
//            .into(ivImage)*/
//    }
//}