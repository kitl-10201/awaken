package com.awakenwhitelabeling.others

import android.text.TextUtils
import android.util.Log
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.App
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.squareup.picasso.Picasso


object PicassoUtil {

    const val COMMUNITY_NULL_IMAGE: String = "set_user_pic"
    private val glide: RequestManager = Glide.with(App.get())

    fun loadImage(image: ImageView, url: String?) {
        glide
            .load(Utils.getWorkoutImageUrl(url))
            .centerCrop()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.IMAGE_BROKEN)
            .into(image);
    }

    fun loadFromWeb(image: ImageView, url: String) {
        Log.e( "loadFromWeb: ",url )
        glide
            .load(url)
            .centerCrop()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.IMAGE_BROKEN)
            .into(image);
    }
    fun loadProfileImage(image: ImageView, url: String) {
        glide
            .load(RetrofitClient.PROFILE_IMAGE_URL + url)
            .centerCrop()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.IMAGE_BROKEN)
            .into(image);

    }



    fun loadQRImage(image: ImageView, url: String) {
        glide
            .load(RetrofitClient.BASE_URL + url)
            .centerCrop()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.IMAGE_BROKEN)
            .into(image);

    }


    fun loadServerimage(image: ImageView, url: String) {
        glide
            .load(RetrofitClient.WORKOUT_IMAGE_URL + url)
            .centerCrop()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.IMAGE_BROKEN)
            .into(image);


    }

    fun loadUserImage(image: ImageView, url: String?) {
        if (Utils.isEmptyString(url)) {
            Utils.viewGone(image)
            return
        }
        glide
            .load(Utils.getWorkoutImageUrl(url))
            .centerCrop()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.USER_IMAGE)
            .into(image);

        /*Picasso.get()
            .load(Utils.getWorkoutImageUrl(url))
            .centerCrop()
            .fit()
            .noFade()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.USER_IMAGE)
            .into(image)*/
    }

    fun loadCommunityUserPicImage(image: ImageView, url: String?) {
        if (TextUtils.equals(url, COMMUNITY_NULL_IMAGE)) {
            image.setImageResource(Cons.USER_IMAGE)
            return
        }

        if (Utils.isEmptyString(url)) {
            Utils.viewGone(image)
            return
        }
        glide
            .load(Utils.getWorkoutImageUrl(url))
            .centerCrop()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.USER_IMAGE)
            .into(image);

        /*Picasso.get()
            .load(Utils.getWorkoutImageUrl(url))
            .centerCrop()
            .fit()
            .noFade()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.USER_IMAGE)
            .into(image)*/
    }


    fun loadImageWithLogo(image: ImageView, url: String?) {
        glide
            .load(Utils.getWorkoutImageUrl(url))
            .centerCrop()
            .placeholder(R.drawable.progress_circle)
            .error(R.drawable.avatar)
            .into(image);
        /*Picasso.get()
            .load(Utils.getWorkoutImageUrl(url))
            .centerCrop()
            .fit()
            .noFade()
            .placeholder(R.drawable.progress_circle)
            .error(R.drawable.app_logo_transparent_512)
            .into(image)*/
    }

    fun loadDrawableImage(ivImage: ImageView, @DrawableRes userImage: Int) {

        glide
            .load(userImage)
            .centerCrop()
            .into(ivImage);
        /*Picasso.get()
            .load(userImage)
            .centerCrop()
            .into(ivImage)*/
    }
    fun loadPicassoImage(ivImage: ImageView, userImage:  String?) {

        Picasso.get()
            .load(Utils.getWorkoutImageUrl(userImage))
            .centerCrop()
            .fit()
            .noFade()
            .placeholder(R.drawable.progress_circle)
            .error(Cons.IMAGE_BROKEN)
            .into(ivImage)
    }

}