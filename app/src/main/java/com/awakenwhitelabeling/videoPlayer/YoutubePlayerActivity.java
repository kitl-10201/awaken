package com.awakenwhitelabeling.videoPlayer;


import com.awakenwhitelabeling.R;
/*

public class YoutubePlayerActivity extends YouTubeFailureRecoveryActivity implements
        YouTubePlayer.OnFullscreenListener, MyYoutubePlayerListener {

    private static final String TAG = "YoutubePlayerActivity";

    private static final String EXTRA_VIDEO_URL = "extra.video.url";

    public static void start(Context context, String url) {
        context.startActivity(new Intent(context, YoutubePlayerActivity.class)
                .putExtra(EXTRA_VIDEO_URL, url));
    }

    private static final int PORTRAIT_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;

    private YouTubePlayerView playerView;
    private YouTubePlayer player;

    private boolean fullscreen;

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        if (checkifUrlIsValid() && playerView != null) {
            playerView.initialize(DeveloperKey.DEVELOPER_KEY, this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    private String videoUrl;
    //https://www.youtube.com/watch?v= (value of Cons.YOUTUBE_LINK_MATCHER)
    private String YOUTUBE_LINK_MATCHER = Cons.YOUTUBE_LINK_MATCHER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoUrl = getIntent().getStringExtra(EXTRA_VIDEO_URL);
        setContentView(R.layout.fullscreen_demo);
        playerView = findViewById(R.id.player);
    }


    private boolean checkifUrlIsValid() {
        if (Utils.Companion.isEmptyString(videoUrl)) {
            Toaster.INSTANCE.shortToast("Empty YouTube link ...");
            finish();
            return false;
        }
        if (videoUrl.startsWith(YOUTUBE_LINK_MATCHER)
                || videoUrl.startsWith(Cons.SHORT_YOUTUBE_LINK_MATCHER)) {
            return true;
        }
        Toaster.INSTANCE.shortToast("Invalid YouTube link ...");
        finish();
        return false;
    }

    private void fullScreen() {
        if (player != null) player.setFullscreen(true);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        this.player = player;
        // Specify that we want to handle fullscreen behavior ourselves.
        player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
        player.setPlayerStateChangeListener(this); //MyYoutubePlayerListener
        player.setOnFullscreenListener(this); // YouTubePlayer.OnFullscreenListener
        fullScreen();
        if (!wasRestored) {
            String code = "";
            if (videoUrl.startsWith(YOUTUBE_LINK_MATCHER)){
                code = videoUrl.replace(YOUTUBE_LINK_MATCHER, "");
            }else if (videoUrl.startsWith(Cons.SHORT_YOUTUBE_LINK_MATCHER)){
                code = videoUrl.replace(Cons.SHORT_YOUTUBE_LINK_MATCHER, "");
            } else{
                code = videoUrl;
            }
            player.cueVideo(code);
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return playerView;
    }

    @Override
    public void onFullscreen(boolean isFullscreen) {
        fullscreen = isFullscreen;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (player != null) {
            player.release();
            player = null;
        }
    }

    @Override
    public void onVideoEnded() {
        onBackPressed();
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {
        Log.e(TAG, "onError: "+errorReason.name() );
        Toaster.INSTANCE.shortToast("Error in playing video : "+errorReason.name());
        onBackPressed();
    }
}*/
