package com.awakenwhitelabeling.vimeoPlayable

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity
import com.awakenwhitelabeling.dashboard.workoutdetails.bean.TrainingDetailByIdBean
import com.awakenwhitelabeling.others.Cons
import com.awakenwhitelabeling.others.PicassoUtil
import com.devs.readmoreoption.ReadMoreOption
import com.google.gson.Gson

class VideoDetailsActivity : BaseActivity() {
    private lateinit var preScreenData: TrainingDetailByIdBean.Data.TrainingSessions.Data
    private lateinit var progressBar: ProgressBar
    private lateinit var ivBackVideoDetail: ImageView
    private lateinit var ivPlay: ImageView
    private lateinit var ivTrainingimage: ImageView
    private lateinit var tvVideiTitle: TextView
    private lateinit var tvWorkoutName: TextView
    private lateinit var tvDuration: TextView
    private lateinit var tvDescription: TextView
    private var VideoUrlForFullScreenVideo: String? = null

    companion object {
        private const val EXTRA_WORKOUT = "extra.workout.data"
        private const val EXTRA_FULL_DETAIL = "extra.workout.full.detail"
        fun trainingVideo(
            context: Context,
            data: TrainingDetailByIdBean.Data.TrainingSessions.Data
        ) {
            context.startActivity(
                Intent(context, VideoDetailsActivity::class.java)
                    .putExtra(EXTRA_WORKOUT, data)
                    .putExtra(EXTRA_FULL_DETAIL, true)
            )
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_videodetails)
        parseBundle()
        setVideoDetailsRefrence()
        Log.e("WorkoutDeta", Gson().toJson(preScreenData))
        //tvDescription.text = preScreenData.training_description
        Log.d("fsdhjhhh", "" + preScreenData.training_video_url)
    }

    private fun setVideoDetailsRefrence() {
        ivPlay = findViewById(R.id.ivPlay)
        tvDescription = findViewById(R.id.tvDescription)
        tvVideiTitle = findViewById(R.id.tvVideiTitle)
        tvWorkoutName = findViewById(R.id.tvWorkoutName)
        ivTrainingimage = findViewById(R.id.ivTrainingimage)
        ivBackVideoDetail = findViewById(R.id.ivDetailsBack)
        ivBackVideoDetail.setOnClickListener {
            this.onBackPressed()
        }

        val mLink = preScreenData.training_platform
        if (mLink == "1")
            VideoUrlForFullScreenVideo = preScreenData.training_video_url
        else
            VideoUrlForFullScreenVideo = preScreenData.training_remote_video
        Log.d("video type..", "VideoType: " + mLink.toString())
        ivPlay.setOnClickListener {
            //if (preScreenData.training_video_url.isNullOrEmpty()) return@setOnClickListener
            val intent = Intent(this, FullScreenVideo::class.java)
            intent.putExtra(Cons.VIMEO_URL, VideoUrlForFullScreenVideo)
            startActivity(intent)
            Log.d("fsdhjhhh", "" + preScreenData.training_video_url)
        }
        tvDuration = findViewById(R.id.tvDuration)
        PicassoUtil.loadImage(ivTrainingimage, preScreenData.training_featured_image)
        tvVideiTitle.text = preScreenData.training_name
        tvWorkoutName.text = preScreenData.training_name
        // Toaster.shortToast(tvVideiTitle.toString())
        tvDuration.text = "Duration : " + preScreenData.duration + " mins"
        var readMoreOption: ReadMoreOption = ReadMoreOption.Builder(this@VideoDetailsActivity)
            .textLength(5, ReadMoreOption.TYPE_LINE) //OR
            //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
            .moreLabel("Read More")
            .lessLabel("Read Less")
            .moreLabelColor(Color.BLUE)
            .lessLabelColor(Color.BLUE)
            .labelUnderLine(true)
            .expandAnimation(true)
            .build();
        readMoreOption.addReadMoreTo(tvDescription, preScreenData.training_description);
    }

    private fun parseBundle() {
        if (intent.extras != null) {
            if (intent.hasExtra(EXTRA_WORKOUT)) {
                preScreenData =
                    intent.getSerializableExtra(EXTRA_WORKOUT) as TrainingDetailByIdBean.Data.TrainingSessions.Data
            }
        }
    }

}
