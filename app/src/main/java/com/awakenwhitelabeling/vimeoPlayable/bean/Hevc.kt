package com.awakenwhitelabeling.vimeoPlayable.bean

data class Hevc(
    val dvh1: List<Any>,
    val hdr: List<Any>,
    val sdr: List<Any>
)