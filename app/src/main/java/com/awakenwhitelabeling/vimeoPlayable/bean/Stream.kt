package com.awakenwhitelabeling.vimeoPlayable.bean

data class Stream(
    val fps: Int,
    val id: String,
    val profile: Any,
    val quality: String
)