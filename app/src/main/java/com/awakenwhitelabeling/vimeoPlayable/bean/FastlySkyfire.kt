package com.awakenwhitelabeling.vimeoPlayable.bean

data class FastlySkyfire(
    val avc_url: String,
    val origin: String,
    val url: String
)