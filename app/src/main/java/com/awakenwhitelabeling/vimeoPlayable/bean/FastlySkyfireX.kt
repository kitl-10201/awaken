package com.awakenwhitelabeling.vimeoPlayable.bean

data class FastlySkyfireX(
    val avc_url: String,
    val origin: String,
    val url: String
)