package com.awakenwhitelabeling.vimeoPlayable.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VimeoBean{


    public class Download {

        @SerializedName("quality")
        @Expose
        public String quality;
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
        @SerializedName("expires")
        @Expose
        public String expires;
        @SerializedName("link")
        @Expose
        public String link;
        @SerializedName("created_time")
        @Expose
        public String createdTime;
        @SerializedName("fps")
        @Expose
        public Integer fps;
        @SerializedName("size")
        @Expose
        public Integer size;
        @SerializedName("md5")
        @Expose
        public String md5;

    }



        @SerializedName("uri")
        @Expose
        public String uri;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("description")
        @Expose
        public Object description;
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("link")
        @Expose
        public String link;
        @SerializedName("duration")
        @Expose
        public Integer duration;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("language")
        @Expose
        public Object language;
        @SerializedName("height")
        @Expose
        public Integer height;
        @SerializedName("created_time")
        @Expose
        public String createdTime;
        @SerializedName("modified_time")
        @Expose
        public String modifiedTime;
        @SerializedName("release_time")
        @Expose
        public String releaseTime;
        @SerializedName("license")
        @Expose
        public Object license;
        @SerializedName("parent_folder")
        @Expose
        public Object parentFolder;
        @SerializedName("last_user_action_event_date")
        @Expose
        public String lastUserActionEventDate;
        @SerializedName("files")
        @Expose
        public List<File> files = null;
        @SerializedName("download")
        @Expose
        public List<Download> download = null;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("resource_key")
        @Expose
        public String resourceKey;




    public class File {

        @SerializedName("quality")
        @Expose
        public String quality;
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
        @SerializedName("link")
        @Expose
        public String link;
        @SerializedName("created_time")
        @Expose
        public String createdTime;
        @SerializedName("fps")
        @Expose
        public Integer fps;
        @SerializedName("size")
        @Expose
        public Integer size;
        @SerializedName("md5")
        @Expose
        public String md5;

    }
}