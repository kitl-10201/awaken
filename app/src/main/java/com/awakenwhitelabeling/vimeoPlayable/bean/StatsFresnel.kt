package com.awakenwhitelabeling.vimeoPlayable.bean

data class StatsFresnel(
    val `data`: DataXX,
    val group: Boolean,
    val track: Boolean
)