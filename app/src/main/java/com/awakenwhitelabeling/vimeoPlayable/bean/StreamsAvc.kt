package com.awakenwhitelabeling.vimeoPlayable.bean

data class StreamsAvc(
    val fps: Int,
    val id: String,
    val profile: Any,
    val quality: String
)