package com.awakenwhitelabeling.vimeoPlayable.bean

data class AkfireInterconnectQuicX(
    val avc_url: String,
    val origin: String,
    val url: String
)