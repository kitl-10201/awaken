package com.awakenwhitelabeling.vimeoPlayable.bean

data class Cookie(
    val captions: Any,
    val hd: Int,
    val quality: Any,
    val scaling: Int,
    val volume: Double
)