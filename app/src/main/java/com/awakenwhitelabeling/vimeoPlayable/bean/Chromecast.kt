package com.awakenwhitelabeling.vimeoPlayable.bean

data class Chromecast(
    val `data`: Data,
    val group: Boolean,
    val track: Boolean
)