package com.awakenwhitelabeling.vimeoPlayable.bean

data class Version(
    val available: List<Available>,
    val current: Any
)