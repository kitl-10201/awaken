package com.awakenwhitelabeling.vimeoPlayable.bean

data class Client(
    val ip: String
)