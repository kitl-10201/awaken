package com.awakenwhitelabeling.vimeoPlayable.bean

data class Thumbs(
    val `640`: String,
    val `960`: String,
    val base: String
)