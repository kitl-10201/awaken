package com.awakenwhitelabeling.vimeoPlayable.bean

data class Build(
    val backend: String,
    val js: String
)