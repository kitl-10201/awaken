package com.awakenwhitelabeling.vimeoPlayable.bean

data class Owner(
    val account_type: String,
    val id: Int,
    val img: String,
    val img_2x: String,
    val name: String,
    val url: String
)