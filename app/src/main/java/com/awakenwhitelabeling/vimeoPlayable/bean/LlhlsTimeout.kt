package com.awakenwhitelabeling.vimeoPlayable.bean

data class LlhlsTimeout(
    val `data`: DataX,
    val group: Boolean,
    val track: Boolean
)