package com.awakenwhitelabeling.vimeoPlayable.bean

data class FileCodecs(
    val av1: List<Any>,
    val avc: List<String>,
    val hevc: Hevc
)