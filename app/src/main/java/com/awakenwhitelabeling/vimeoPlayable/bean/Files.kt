package com.awakenwhitelabeling.vimeoPlayable.bean

data class Files(
    val dash: Dash,
    val hls: Hls,
    val progressive: List<Progressive>
)