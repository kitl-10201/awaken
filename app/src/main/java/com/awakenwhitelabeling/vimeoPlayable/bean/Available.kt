package com.awakenwhitelabeling.vimeoPlayable.bean

data class Available(
    val file_id: Long,
    val id: Int,
    val is_current: Int
)