package com.awakenwhitelabeling.vimeoPlayable.bean

data class AkfireInterconnectQuic(
    val avc_url: String,
    val origin: String,
    val url: String
)