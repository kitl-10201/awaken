package com.awakenwhitelabeling.vimeoPlayable.bean

data class Progressive(
    val cdn: String,
    val fps: Int,
    val height: Int,
    val id: String,
    val mime: String,
    val origin: String,
    val profile: Any,
    val quality: String,
    val url: String,
    val width: Int
)