package com.awakenwhitelabeling.vimeoPlayable.bean

data class CdnsX(
    val akfire_interconnect_quic: AkfireInterconnectQuicX,
    val fastly_skyfire: FastlySkyfireX
)