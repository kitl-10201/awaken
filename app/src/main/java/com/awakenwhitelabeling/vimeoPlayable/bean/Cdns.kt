package com.awakenwhitelabeling.vimeoPlayable.bean

data class Cdns(
    val akfire_interconnect_quic: AkfireInterconnectQuic,
    val fastly_skyfire: FastlySkyfire
)