package com.awakenwhitelabeling.vimeoPlayable.bean

data class AbTests(
    val chromecast: Chromecast,
    val llhls_timeout: LlhlsTimeout,
    val stats_fresnel: StatsFresnel
)