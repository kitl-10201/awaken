package com.awakenwhitelabeling.vimeoPlayable.bean

data class Sentry(
    val debug_enabled: Boolean,
    val debug_intent: Int,
    val enabled: Boolean,
    val url: String
)