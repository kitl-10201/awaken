package com.awakenwhitelabeling.vimeoPlayable.bean

data class Hls(
    val cdns: CdnsX,
    val default_cdn: String,
    val separate_av: Boolean
)