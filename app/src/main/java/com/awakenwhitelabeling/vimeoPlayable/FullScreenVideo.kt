package com.awakenwhitelabeling.vimeoPlayable

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.others.Cons
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.vimeoPlayable.bean.VimeoBean
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import java.lang.Exception

class FullScreenVideo : AppCompatActivity() {
    lateinit var videoID: String
    var videoURl: String? = null
    private var completeUrl: String? = null
    var player: SimpleExoPlayer? = null
    var playerView: PlayerView? = null
    private var fullscreenButton: ImageView? = null
    var fullscreen = true
    var progressBar: ProgressBar? = null
    var close: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_video)
        progressBar = findViewById(R.id.progressBar)
        close = findViewById(R.id.tvClose)
        close?.setOnClickListener {
            onBackPressed()
        }
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        player = ExoPlayerFactory.newSimpleInstance(applicationContext)
        playerView = findViewById(R.id.player)
        fullscreenButton = playerView?.findViewById(R.id.exo_fullscreen_icon)
        setVideoID()
    }


    private fun setVideoID() {
        //  Log.e("Bundle", Gson().toJson(intent.extras) )
        if (intent.hasExtra(Cons.VIMEO_URL)) {
            videoURl = intent.getStringExtra(Cons.VIMEO_URL)
            if (videoURl?.contains("vimeo.com") == true) {
                val list = intent.getStringExtra(Cons.VIMEO_URL)?.split("/")
                videoID = list?.get(3).toString()
                getVideoPlayableLink()
            } else {
                completeUrl =
                    RetrofitClient.WORKOUT_VIDEO_URL + intent.getStringExtra(Cons.VIMEO_URL)
                configureVideoPlayer()
            }
        }
    }

/*
    private fun getVideoPlayableLink() {
        RetrofitClient.getRequest().getPublicVimeoPlayableLink(videoID)
            .enqueue(object : Callback<JsonObject?> {
                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                    if (response.isSuccessful && response.code() == 200) {
                        val vimeoDetails =
                            Gson().fromJson(response.body(), VimeoVideoDetail::class.java)
                        Log.e("onResponse: ", Gson().toJson(vimeoDetails))
                        val progressiveSize = vimeoDetails.request.files.progressive.size
                        var maxWidth = -1
                        for (i in vimeoDetails.request.files.progressive) {
                            if (maxWidth < i.width) {
                                maxWidth = i.width
                            }
                        }

                        completeUrl =
                            vimeoDetails.request.files.progressive.maxByOrNull { it.width }?.url
                        configureVideoPlayer()

                    }
                }

                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                }
            })
    }
*/

    private fun getVideoPlayableLink() {
        //  showLoader()
        RetrofitClient.getVimeoAccess("7c6c7d0ddab13693fb4b72684c11a337")
            .create(RetrofitApi::class.java).getVimeoVideoDetails(videoID)
            .enqueue(object : retrofit2.Callback<JsonObject?> {
                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                    try {
                        if (response.isSuccessful && response.code() == 200) {

                            val vimeoThumbnail = Gson().fromJson<VimeoBean>(
                                response.body(),
                                VimeoBean::class.java
                            )
                            var maxWidth = 0
                            var maxImagePosition = 0
                            for (i in 0 until vimeoThumbnail.files.size) {
                                if (i == 0) {
                                    if (vimeoThumbnail.files[i].width != null) {
                                        maxWidth = vimeoThumbnail.files[i].width
                                        maxImagePosition = i
                                    }
                                }
                                if (vimeoThumbnail.files[i].width != null) {
                                    if (maxWidth < vimeoThumbnail.files[i].width) {
                                        maxWidth = vimeoThumbnail.files[i].width
                                        maxImagePosition = i
                                    }
                                }
                            }
                            Log.e(
                                "fdssdf",
                                "getVideoPlayableLink: invoke " + vimeoThumbnail.files[maxImagePosition].link
                            )
                            completeUrl = vimeoThumbnail.files[maxImagePosition].link
                            configureVideoPlayer()
                        }
                    } catch (e: Exception) {
                        Log.e("fdssdf", "onException: ${e.localizedMessage}")
                    }
                }

                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    Toaster.shortToast(t.localizedMessage.toString())
                    Log.d("getErrotType", "ErrorURL" + t.localizedMessage)
                }
            })
    }


    override fun onPause() {
        /*super.onPause()
        playerView?.onPause()*/
        super.onPause()
        try {
            player?.stop()
            //  playerView?.setUseController(true)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            configureVideoPlayer()
            player?.setPlayWhenReady(true)
            /*    playerView?.setUseController(true)
                player?.playWhenReady = true*/
            //           player?.release()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
/*
    override fun onStart() {
        super.onStart()
        try {
            configureVideoPlayer()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }*/


    private fun configureVideoPlayer() {
        fullscreenButton?.setOnClickListener {
            if (fullscreen) {
                setPortraitView()
            } else {
                setLandscapeView()
            }
        }
        playerView?.player = player
        playerView?.setUseController(true)
        val dataSourceFactory: DataSource.Factory =
            DefaultDataSourceFactory(
                applicationContext,
                Util.getUserAgent(
                    applicationContext,
                    applicationContext.getString(R.string.app_name)
                )
            )

        val videoSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(completeUrl))
        Log.d("VodeoUrl", "Video COmeplete Url: " + videoSource.toString())
        player?.prepare(videoSource)
        player?.playWhenReady = true
        player?.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                if (playbackState == Player.STATE_BUFFERING) {
                    Utils.viewVisible(progressBar)
                } else if (playbackState == Player.STATE_IDLE) {
                    Utils.viewVisible(progressBar)
                } else if (playbackState == Player.STATE_ENDED) {
                    close?.performClick()
                } else if (!playWhenReady) {
                    Utils.viewGone(progressBar)
                } else {
                    Utils.viewGone(progressBar)
                }
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                super.onLoadingChanged(isLoading)
                if (isLoading) {
                    Utils.viewVisible(progressBar)
                } else {
                    Utils.viewGone(progressBar)

                }
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                super.onPlayerError(error)
                Toaster.shortToast("This video does not exist.")
                Log.d("dggdgdg", "fhhhfdhhf" + error.toString())
            }
        })


    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation === Configuration.ORIENTATION_LANDSCAPE) {
            setLandscapeView()
        } else if (newConfig.orientation === Configuration.ORIENTATION_PORTRAIT) {
            setPortraitView()
        }
    }


    private fun setLandscapeView() {
        fullscreenButton?.setImageDrawable(
            ContextCompat.getDrawable(
                this,
                R.drawable.ic_fullscreen_close
            )
        )
        window.decorView.systemUiVisibility =
            (View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
        val params = playerView?.layoutParams as RelativeLayout.LayoutParams
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.MATCH_PARENT
        playerView?.layoutParams = params
        fullscreen = true
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }

    private fun setPortraitView() {
        fullscreenButton?.setImageDrawable(
            ContextCompat.getDrawable(
                this,
                R.drawable.ic_fullscreen_open
            )
        )
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        val params = playerView?.layoutParams as RelativeLayout.LayoutParams
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = (200 * applicationContext.resources.displayMetrics.density).toInt()
        playerView?.layoutParams = params
        fullscreen = false
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

}