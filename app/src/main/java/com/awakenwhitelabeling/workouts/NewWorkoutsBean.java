package com.awakenwhitelabeling.workouts;



import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class NewWorkoutsBean implements Serializable {
    @SerializedName("success")
    @Expose
    public String success;
    @SerializedName("data")
    @Expose
    public Data data;

    public NewWorkoutsBean(Data data) {
        this.data = data;
    }

    public NewWorkoutsBean() {
    }

    public class Data implements Serializable{

        public Data() {
        }

        public Data(List<Datum> data) {
            this.data = data;
        }

        @SerializedName("current_page")
        @Expose
        public Integer currentPage;
        @SerializedName("data")
        @Expose
        public List<Datum> data;
        @SerializedName("first_page_url")
        @Expose
        public String firstPageUrl;
        @SerializedName("from")
        @Expose
        public Integer from;
        @SerializedName("last_page")
        @Expose
        public Integer lastPage;
        @SerializedName("last_page_url")
        @Expose
        public String lastPageUrl;
        @SerializedName("next_page_url")
        @Expose
        public Object nextPageUrl;
        @SerializedName("path")
        @Expose
        public String path;
        @SerializedName("per_page")
        @Expose
        public Integer perPage;
        @SerializedName("prev_page_url")
        @Expose
        public String prevPageUrl;
        @SerializedName("to")
        @Expose
        public Integer to;
        @SerializedName("total")
        @Expose
        public Integer total;

    }

    public class Datum implements Serializable{

        public Datum() {
        }

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("active_status")
        @Expose
        public String activeStatus;
        @SerializedName("category_name")
        @Expose
        public String categoryName;
        @SerializedName("category_description")
        @Expose
        public String categoryDescription;

        @SerializedName("category_platform")
        @Expose
        public String categoryPlatform;
        @SerializedName("category_featured_image")
        @Expose
        public String workoutFeaturedImage;
        @SerializedName("category_featured_video")
        @Expose
        public String categoryFeaturedVideo;

        @SerializedName("category_session_video_url")
        public String workout_session_video_url;

        @SerializedName("category_remote_video")
        @Expose
        public String categoryRemoteVideo;

        @SerializedName("category_meeting_id")
        @Expose
        public String categoryMeetingId;

        @SerializedName("category_meeting_password")
        @Expose
        public String categoryMeetingPassword;

        @SerializedName("category_meeting_url")
        @Expose
        public String categoryMeetingUrl;
        @SerializedName("duration")
        @Expose
        public String duration;
        @SerializedName("tags")
        @Expose
        public String tags;
        @SerializedName("category_type")
        @Expose
        public String categoryType;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;

        @SerializedName("count")
        @Expose
        public String count;


        @SerializedName("category_sessions_count")
        @Expose
        public int categorySessionsCount;

        // created for local reference
        public boolean showBookMark;

        @SerializedName("is_fav_session")
        public String is_fav_session;

        @SerializedName("is_complete_session")
        public String is_complete_session;


        @SerializedName("added_in_calendar")
        public String added_in_calendar;

        public boolean IsSessionFav() {
            return TextUtils.equals(is_fav_session, "1");
        }

        public void setSessionFav(boolean val){
            is_fav_session = val ? "1" : "0";
        }

        public boolean Is_complete_session() {
            return TextUtils.equals(is_complete_session, "1");
        }

        public void set_complete_session(boolean val) {
            is_complete_session = val ? "1" : "0";
        }

        public boolean getAdded_in_calendar() {
            return TextUtils.equals(added_in_calendar, "1");
        }

        public void setAdded_in_calendar(boolean val) {
            added_in_calendar = val ? "1" : "0";
        }


    }

}
