package com.awakenwhitelabeling.membership.bean

data class PurchasedMembershipBean(
    val `data`: Data,
    val message: String,
    val status: String
) {
    data class Data(
        val amountPaid: Double,
        val description: String,
        val isValid: String,
        val membership_frequency: String,
        val name: String,
        val status: String,
        val subscription_status: String,
        val trial_end_day: String,
        val trial_taken: Int,
        val validFrom: String,
        val validTo: String,
        val is_recurring:String,
        val payment_platform:String,
        var google_actual_plan_id: String
    )
}