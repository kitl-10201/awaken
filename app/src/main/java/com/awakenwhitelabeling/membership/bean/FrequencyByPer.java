package com.awakenwhitelabeling.membership.bean;

public enum FrequencyByPer {
    DAILY("Day"),
    WEEKLY("Week"),
    MONTHLY("Month"),
    BI_YEARLY("Bi-Year"),
    YEARLY("Year"),
    NULL("--");

    private final String text;

    FrequencyByPer(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
