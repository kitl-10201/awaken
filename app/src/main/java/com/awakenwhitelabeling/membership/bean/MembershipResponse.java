package com.awakenwhitelabeling.membership.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MembershipResponse implements Serializable {


    @SerializedName("success")
    @Expose
    public String success;
    @SerializedName("data")
    @Expose
    public List<MembershipBean> data = null;

    public class MembershipBean implements Serializable {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("user_id")
        @Expose
        public Integer userId;
        @SerializedName("active_status")
        @Expose
        public String activeStatus;
        @SerializedName("name")
        @Expose
        public String membershipName;
        @SerializedName("description")
        @Expose
        public String membershipDescription;
        @SerializedName("price")
        @Expose
        public String membershipPrice;
        //ProductId
        @SerializedName("google_actual_plan_id")
        @Expose
        public String google_actual_plan_id;
        @SerializedName("membership_frequency")
        @Expose
        public String membership_frequency;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;

        @SerializedName("is_recurring")
        public int isRecurring;

        public boolean IsRecurring(){
            return isRecurring == 1;
        }

        @SerializedName("includes_trial")
        public int includesTrial;

        public boolean IsIncludeTrail(){
            return  includesTrial == 1;
        }

        public String getFrequencyString() {
            return membership_frequency;
        }

        public Frequency getFrequency() {

            switch (membership_frequency) {
                case "perday":
                    return Frequency.DAILY;

                case "weekly":
                    return Frequency.WEEKLY;

                case "monthly":
                    return Frequency.MONTHLY;

                case "bi-yearly":
                    return Frequency.BI_YEARLY;

                case "yearly":
                    return Frequency.YEARLY;

                default:
                    return Frequency.NULL;

            }
        }


        public FrequencyByPer getFrequencyByPer() {

            switch (membership_frequency) {
                case "perday":
                    return FrequencyByPer.DAILY;

                case "weekly":
                    return FrequencyByPer.WEEKLY;

                case "monthly":
                    return FrequencyByPer.MONTHLY;

                case "bi-yearly":
                    return FrequencyByPer.BI_YEARLY;

                case "yearly":
                    return FrequencyByPer.YEARLY;

                default:
                    return FrequencyByPer.NULL;

            }
        }
    }


}


