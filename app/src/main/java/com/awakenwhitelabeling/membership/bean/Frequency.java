package com.awakenwhitelabeling.membership.bean;

public enum Frequency {
    DAILY("Daily"),
    WEEKLY("Weekly"),
    MONTHLY("Monthly"),
    BI_YEARLY("Bi-Yearly"),
    YEARLY("Yearly"),
    NULL("--");

    private final String text;

    Frequency(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}