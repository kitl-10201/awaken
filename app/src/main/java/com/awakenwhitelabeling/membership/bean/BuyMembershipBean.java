package com.awakenwhitelabeling.membership.bean;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuyMembershipBean {

    public class Data {

        @SerializedName("userID")
        @Expose
        public Integer userID;
        @SerializedName("membership_frequency")
        @Expose
        public String membershipFrequency;
        @SerializedName("membership_id")
        @Expose
        public String membershipId;
        @SerializedName("membership_start_date")
        @Expose
        public String membershipStartDate;
        @SerializedName("membership_end_date")
        @Expose
        public String membershipEndDate;
        @SerializedName("transactionNo")
        @Expose
        public String transactionNo;
        @SerializedName("amountPaid")
        @Expose
        public String amountPaid;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("id")
        @Expose
        public Integer id;

    }

/*{
    "success": "Data Found",
    "data": [
        {
            "id": 1,
            "user_id": 1,
            "active_status": 1,
            "name": "Weekly Plan",
            "description": "<ul>\r\n    <li class=\"first-child\">No Trial Days</li>\r\n    <li>One Time Payment</li>\r\n</ul>",
            "price": "24.99",
            "membership_frequency": "weekly",
            "is_recurring": 0,
            "braintree_actual_plan_id": "",
            "google_actual_plan_id": "",
            "apple_actual_plan_id": "com.Awaken.weeklyPlan",
            "includes_trial": "0",
            "trial_days": 0,
            "created_at": "2020-07-09 09:31:00",
            "updated_at": "2021-09-30 03:49:53"
        },
        {
            "id": 2,
            "user_id": 1,
            "active_status": 1,
            "name": "Monthly Plan",
            "description": "<ul>\r\n    <li class=\"first-child\">Includes 7 Days Free Trial&nbsp;</li>\r\n    <li>Renews Automatically after month</li>\r\n</ul>",
            "price": "99.99",
            "membership_frequency": "monthly",
            "is_recurring": 1,
            "braintree_actual_plan_id": "",
            "google_actual_plan_id": "com.awaken.monthly.with_trail",
            "apple_actual_plan_id": "com.Awaken_monthlyPlanWithTrial",
            "includes_trial": "1",
            "trial_days": 7,
            "created_at": "2020-07-09 09:32:00",
            "updated_at": "2021-10-18 03:06:14"
        },
        {
            "id": 5,
            "user_id": 1,
            "active_status": 1,
            "name": "Bi-Yearly Plan",
            "description": "<ul>\r\n    <li class=\"first-child\">Includes 7 Days Free Trial&nbsp;</li>\r\n    <li>Renews Automatically after 6 months</li>\r\n</ul>",
            "price": "149.99",
            "membership_frequency": "bi-yearly",
            "is_recurring": 1,
            "braintree_actual_plan_id": "",
            "google_actual_plan_id": "",
            "apple_actual_plan_id": "com.Awaken_bi_yearlyPlanWithTrial",
            "includes_trial": "1",
            "trial_days": 7,
            "created_at": "2020-07-09 09:33:00",
            "updated_at": "2021-09-30 03:50:21"
        },
        {
            "id": 3,
            "user_id": 1,
            "active_status": 1,
            "name": "Yearly Plan",
            "description": "<ul>\r\n    <li class=\"first-child\">Includes 7 Days Free Trial&nbsp;</li>\r\n    <li>Renews Automatically after One Year</li>\r\n</ul>",
            "price": "199.99",
            "membership_frequency": "yearly",
            "is_recurring": 1,
            "braintree_actual_plan_id": "",
            "google_actual_plan_id": "",
            "apple_actual_plan_id": "com.Awaken_YearlyPlanWithTrial",
            "includes_trial": "1",
            "trial_days": 7,
            "created_at": "2020-07-09 09:34:00",
            "updated_at": "2021-09-30 03:50:28"
        },
        {
            "id": 6,
            "user_id": 1,
            "active_status": 1,
            "name": "Monthly Plan",
            "description": "<ul>\r\n    <li class=\"first-child\">No Trial</li>\r\n    <li>Renews Automatically after month</li>\r\n</ul>",
            "price": "99.99",
            "membership_frequency": "monthly",
            "is_recurring": 0,
            "braintree_actual_plan_id": "",
            "google_actual_plan_id": "",
            "apple_actual_plan_id": "com.Awaken_monthlyPlan",
            "includes_trial": "0",
            "trial_days": 0,
            "created_at": "2021-09-21 03:36:22",
            "updated_at": "2021-09-30 03:50:37"
        },
        {
            "id": 7,
            "user_id": 1,
            "active_status": 1,
            "name": "Bi-Yearly Months Plan",
            "description": "<ul>\r\n    <li class=\"first-child\">No Trial</li>\r\n    <li>Renews Automatically after 6 months</li>\r\n</ul>",
            "price": "149.99",
            "membership_frequency": "bi-yearly",
            "is_recurring": 0,
            "braintree_actual_plan_id": "",
            "google_actual_plan_id": "",
            "apple_actual_plan_id": "com.Awaken_bi_yearlyPlan",
            "includes_trial": "0",
            "trial_days": 0,
            "created_at": "2021-09-21 03:37:28",
            "updated_at": "2021-09-30 03:50:49"
        },
        {
            "id": 8,
            "user_id": 1,
            "active_status": 1,
            "name": "Yearly Plan",
            "description": "<ul>\r\n    <li class=\"first-child\">No Trial</li>\r\n    <li>Renews Automatically after One Year</li>\r\n</ul>",
            "price": "199.99",
            "membership_frequency": "yearly",
            "is_recurring": 0,
            "braintree_actual_plan_id": "",
            "google_actual_plan_id": "",
            "apple_actual_plan_id": "com.Awaken_YearlyPlan",
            "includes_trial": "0",
            "trial_days": 0,
            "created_at": "2021-09-21 03:38:01",
            "updated_at": "2021-09-30 03:50:58"
        }
    ]
}*/
    @SerializedName("success")
    @Expose
    public String success;
    @SerializedName("data")
    @Expose
    public Data data;


}
