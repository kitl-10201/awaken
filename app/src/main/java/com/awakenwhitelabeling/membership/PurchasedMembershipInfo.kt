package com.awakenwhitelabeling.membership


import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri

import android.os.Bundle
import android.util.Log

import android.view.Window
import android.view.WindowManager
import android.webkit.WebView
import android.widget.*
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.*
import com.awakenwhitelabeling.membership.bean.PurchasedMembershipBean
import com.awakenwhitelabeling.others.Cons
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class PurchasedMembershipInfo(context: Context) : BaseDialog(context) {
    private lateinit var ivCloseDialog: ImageView
    private lateinit var tvPlanType: TextView
    private lateinit var descMem: WebView
    private lateinit var startDate: TextView
    private lateinit var endDate: TextView
    private lateinit var startTime: TextView
    private lateinit var endTime: TextView
    private lateinit var tvMemCancelMsg: TextView
    private lateinit var rlCancelMem: RelativeLayout
    private lateinit var rlMemPlan: LinearLayout
    private lateinit var btnCancelMembership: Button
    private lateinit var memMsg: LinearLayout
    var purchId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.membership_plan_dialog)
        memMsg = findViewById(R.id.memMsg)
        ivCloseDialog = findViewById(R.id.ivCloseDialog)
        tvPlanType = findViewById(R.id.tvPlanType)
        descMem = findViewById(R.id.tvPlaneDesc)
        startDate = findViewById(R.id.tvPlaneSDate)
        endDate = findViewById(R.id.tvPlaneEDate)
        startTime = findViewById(R.id.tvPlaneSTime)
        endTime = findViewById(R.id.tvPlaneETime)
        tvMemCancelMsg = findViewById(R.id.tvMemCancelMsg)
        rlMemPlan = findViewById(R.id.rlMemPlan)
        rlCancelMem = findViewById(R.id.rlCancelMem)
        btnCancelMembership = findViewById(R.id.btnCancelMembership)
        setDimBlur(window)
        setCancelable(false)
        ivCloseDialog.setOnClickListener {
            this@PurchasedMembershipInfo.dismiss()
        }
        window?.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        getPurchasedMemDetails()

    }

    private fun getPurchasedMemDetails() {

        //   showProgressBar()
        var MemData = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        MemData.getPurchasedMembershipDetail().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                //        hideProgressBar()
                Log.e("onFailure", t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                //   hideProgressBar()
                if (response.isSuccessful) {
                    var MemData =
                        Gson().fromJson(response.body(), PurchasedMembershipBean::class.java)
                    Log.d("MemDataResponse", "MemData\n" + response.body())
                    tvPlanType.text = MemData.data.name

                    purchId = MemData.data.google_actual_plan_id
                    Log.d("fgdfgdgdgdgd", "fgdgdggdgdg" + purchId.toString())
                    if (MemData.data.is_recurring == "0" && MemData.data.payment_platform == Cons.FREE) {
                        Utils.viewGone(rlCancelMem)
                        Utils.viewGone(tvMemCancelMsg)
                    } else if (MemData.data.is_recurring == "1" && MemData.data.payment_platform == Cons.GOOGLE_IN_APP) {
                        Utils.viewVisible(rlCancelMem)
                        btnCancelMembership.setOnClickListener {
                            openPlaystoreAccount()
                        }

                    } else if (MemData.data.payment_platform == Cons.IOS_IN_APP) {
                        Utils.viewVisible(tvMemCancelMsg)
                    }
                    descMem.loadDataWithBaseURL(
                        null,
                        MemData.data.description,
                        "text/html",
                        "utf-8",
                        null
                    )
                    startDate.text = MemData.data.validFrom
                    endDate.text = MemData.data.validTo
                    var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    var setDateFormat = SimpleDateFormat("MMM d, yyyy");
                    var setTimeFormat = SimpleDateFormat("hh:mm a");
                    startDate.text =
                        setDateFormat.format(getDateFormat.parse(MemData.data.validFrom))
                    endDate.text = setDateFormat.format(getDateFormat.parse(MemData.data.validTo))
                } else {
                    val jObjError = JSONObject(response.errorBody()?.string())
                    Toaster.shortToast(
                        jObjError.getString("message")
                    )
                }
            }
        })

    }

    private fun openPlaystoreAccount() {
        var packageName = context.getPackageName()
        try {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/account/subscriptions?google_actual_plan_id=$purchId&package=$packageName")
            )
            context.startActivity(intent)
            //  startActivity(Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/account/subscriptions?sku=$purchId&package=$packageName")))
        } catch (e: ActivityNotFoundException) {
            Toaster.somethingWentWrong()
            e.printStackTrace()
        }
    }
}
