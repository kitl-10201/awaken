package com.awakenwhitelabeling.membership

import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseDialog
import com.awakenwhitelabeling.base.SharedPref
import com.awakenwhitelabeling.others.CallBack


class ConfirmMembershipDialog(
    context: Activity,
    private val membershipAmount: String,
    private val callBack: CallBack<Int>
) :
    BaseDialog(context) {
    private val mCtx = context
    lateinit var sharedPref: SharedPref
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        sharedPref = SharedPref(mCtx)
        setContentView(R.layout.dialog_purchase_membership)
        setDimBlur(window)
        val tvMemMessage: TextView = findViewById(R.id.tvMemMessage) as TextView
        val btMemCancel: TextView = findViewById(R.id.btMemCancel) as TextView
        val buy_membership: Button = findViewById(R.id.buy_membership) as Button
        tvMemMessage.text = "Do you want to buy the membership plan of $$membershipAmount ?"
        btMemCancel.setOnClickListener {
            this@ConfirmMembershipDialog.cancel()
            callBack.onSuccess(0)
        }
            buy_membership.setOnClickListener {
            this@ConfirmMembershipDialog.cancel()
            callBack.onSuccess(1)
        }

    }
}