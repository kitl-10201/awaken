package com.awakenwhitelabeling.membership

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.widget.TextView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseDialog
import com.awakenwhitelabeling.base.DataCache
import com.awakenwhitelabeling.base.SharedPref
import com.awakenwhitelabeling.userAction.UserActivity


class NoMembership(val context: Activity) : BaseDialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_no_membership)
        var tvYes=findViewById<TextView>(R.id.tvYes)
        var tvNo=findViewById<TextView>(R.id.tvNo)

        setDimBlur(window)
        setCancelable(false)
        tvYes.setOnClickListener {
            this@NoMembership.dismiss()
           // TO redirect from NoMembership to Home Screen.......................
            context.startActivity(Intent(context, MembershipActivity::class.java))
           // context.startActivity(Intent(context, DashboardActivity::class.java))
        }

        tvNo.setOnClickListener {
            this@NoMembership.dismiss()
            DataCache.get().clear()
            SharedPref.get().clearAll()
            context.startActivity(Intent(context, UserActivity::class.java))
            context.finishAffinity()
        }
    }
}