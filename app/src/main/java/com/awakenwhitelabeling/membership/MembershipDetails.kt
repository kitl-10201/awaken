package com.awakenwhitelabeling.membership

import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.membership.bean.MembershipResponse
import com.awakenwhitelabeling.membership.helper.GooglePlayActivity
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.Cons

class MembershipDetails : GooglePlayActivity() {
    var selectedPosition: Int = -1

    lateinit var membershipBean: MembershipResponse.MembershipBean
    private val TAG = "MembershipFragment"
    private val MEETING_KEY: String = "extra.meeting.key"
    lateinit var title: TextView
    lateinit var amount: TextView
    lateinit var desc: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_membership_details)
        var back = findViewById<ImageView>(R.id.backDesc)
        var btnBuyDetail = findViewById<Button>(R.id.btnBuyDetail)
        title = findViewById(R.id.tvMemDuration)
        amount = findViewById(R.id.tvAmountDesc)
        desc = findViewById(R.id.tvMsgMem)

        back.setOnClickListener {
            var mIntent = Intent(this, MembershipActivity::class.java)
            startActivity(mIntent)

        }

        btnBuyDetail.setOnClickListener {

            //      Toaster.shortToast("Please buy membership!!")
            ConfirmMembershipDialog(
                this,
                membershipBean.membershipPrice,
                object : CallBack<Int>() {
                    override fun onSuccess(t: Int?) {
                        if (t == 1) {
                            currentSelectedMembershipData=membershipBean

                           setupGooglePlayFlow(membershipBean)

                        }
                    }
                }).show()
        }




        parseBundle()
    }



    private fun parseBundle() {

        if (intent.hasExtra(Cons.MEMBERSHIP_DATA)) {
            membershipBean =
                intent.getSerializableExtra(Cons.MEMBERSHIP_DATA) as MembershipResponse.MembershipBean



            title.setText(membershipBean.membershipName)
            amount.setText("$ "+membershipBean.membershipPrice)
           // desc.setText(Html.fromHtml(membershipBean.membershipDescription))

            desc.loadDataWithBaseURL(null, membershipBean.membershipDescription, "text/html", "utf-8", null)



        }
    }
}