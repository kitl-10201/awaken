package com.awakenwhitelabeling.membership.adapter

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.membership.bean.MembershipResponse
import com.awakenwhitelabeling.others.CallBack

class MembershipAdapter(
    val callBack: CallBack<Int>,
    val data: List<MembershipResponse.MembershipBean>,
    val width: Int
) : RecyclerView.Adapter<MembershipAdapter.ViewHolder>() {
    var selectedPosition = -1
    lateinit var context: Context
    lateinit var membershipBean: MembershipResponse
    var isRight: Boolean = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.membership_plan, parent, false)
        return ViewHolder(v)
    }
    override fun getItemCount(): Int {
        return data?.size ?: 0
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var memberShip = data?.get(position)!!
        setWidthToItem(holder.rlMain)
        val item = data[position]
        holder.tvAmount.text = String.format("$ %s", item.membershipPrice ?: "--")
        // holder.tvPeriod.text = item.frequency.toString()
        // holder.tvStartTrial.text=memberShip.membershipDescription.toString()
        //  holder.tvStartTrial.visibility = if (item.IsIncludeTrail()) View.VISIBLE else View.INVISIBLE
        // holder.tvPeriodWithPer.text = String.format("Per %s", item.frequencyByPer.toString())
        holder.tvPeriodWithPer.text = Html.fromHtml(memberShip.membershipDescription.toString())
        holder.tvPeriod.text = Html.fromHtml(memberShip.membershipName)
//        holder.rlMain.background = ResourceUtils.getDrawable(R.drawable.bg_membership_selected)


/*
        if (item.google_actual_plan_id=="")
        {
            holder.rlMain.isVisible=false
        }
*/

        holder.itemView.setOnClickListener {
/*
            selectedPosition = position;
            callBack.onSuccess(selectedPosition)
*/


//            var mIntent =Intent(context, MembershipDetails::class.java)
//            mIntent.putExtras(bundleOf(Pair(Cons.MEMBERSHIP_DATA,item)))
//            context.startActivity(mIntent)
//            notifyDataSetChanged()

            //  holder.rlMain.background = ResourceUtils.getDrawable(R.drawable.bg_membership_selected)

            /*if (isRight) {

                holder.rlMain.background = ResourceUtils.getDrawable(R.drawable.bg_membership_deselect)
                isRight = false
            } else {
                holder.rlMain.background = ResourceUtils.getDrawable(R.drawable.bg_membership_selected)
                isRight = true
            }*/

        }

        holder.tvStartTrial.setOnClickListener {
            selectedPosition = position;
            callBack.onSuccess(selectedPosition)
            //  Toaster.shortToast("Membership Taken")
            /*  context.startActivity(
                  Intent(context, MembershipDetails::class.java)

              )*/
            //  this used for openning membership details page............
/*
            var mIntent =Intent(context, MembershipDetails::class.java)
            mIntent.putExtras(bundleOf(Pair(Cons.MEMBERSHIP_DATA,item)))
            context.startActivity(mIntent)
*/


            //  Toaster.shortToast("Under maintenances!!")

            //To redirect from MemActivity to to Home Screen.................
            /*var mIntent =Intent(context, DashboardActivity::class.java)
           // mIntent.putExtras(bundleOf(Pair(Cons.MEMBERSHIP_DATA,item)))
            context.startActivity(mIntent)
*/

/*ConfirmMembershipDialog(
                context as Activity,
                membershipBean.data[selectedPosition].membershipPrice,
                object : CallBack<Int>()
                {
                    override fun onSuccess(t: Int?) {
                        if (t == 1) {
                            val bundle = Bundle()
                            bundle.putSerializable(
                                KeyValue.MembershipDetail,
                                membershipBean.data[selectedPosition]
                            )
                            bundle.putString(
                                KeyValue.AMOUNT,
                                membershipBean.data[selectedPosition].membershipPrice.toString()
                            )
                            bundle.putString(
                                KeyValue.MEMBERSHIPID,
                                membershipBean.data[selectedPosition].id.toString()
                            )
                            context.applicationContext?.let {
                                BraintreePaymentActivity.initPayment(
                                    it as Activity,
                                    bundle
                                )
                            }
                        }
                    }
                }).show()
*/
        }

        /*if (selectedPosition == position) {

           // Toaster.shortToast("Under maintenances!")

            holder.rlMain.background = ResourceUtils.getDrawable(R.drawable.bg_membership_selected)
        } else {
            holder.rlMain.background = null;
        }*/

    }

    private fun selection(
        holder: ViewHolder,
        position: Int
    ) {
        selectedPosition = position
        notifyDataSetChanged()
        callBack.onSuccess(position)

    }
    private fun setWidthToItem(rlMain: CardView?) {
        if (rlMain != null) {
            rlMain.layoutParams =
                FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.MATCH_PARENT
                )
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rlMain = itemView.findViewById<CardView>(R.id.rlMain)
        val tvPeriod = itemView.findViewById<TextView>(R.id.tvPeriod)
        val tvPeriodWithPer = itemView.findViewById<TextView>(R.id.tvDurationMem)
        val tvAmount = itemView.findViewById<TextView>(R.id.tvAmount)
        val tvStartTrial = itemView.findViewById<TextView>(R.id.btnBuy)
    }


}