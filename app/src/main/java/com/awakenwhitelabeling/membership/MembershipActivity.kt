package com.awakenwhitelabeling.membership

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.KeyValue
import com.awakenwhitelabeling.base.SharedPref
import com.awakenwhitelabeling.membership.adapter.MembershipAdapter
import com.awakenwhitelabeling.membership.bean.BuyMembershipBean
import com.awakenwhitelabeling.membership.bean.MembershipResponse
import com.awakenwhitelabeling.membership.helper.GooglePlayActivity
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.ErrorUtils
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.userAction.UserActivity
import com.google.gson.Gson
import com.google.gson.JsonObject

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import kotlin.math.roundToInt

class MembershipActivity : GooglePlayActivity() {
    lateinit var membershipBean: MembershipResponse
    lateinit var mBeans: MembershipResponse.MembershipBean
    var selectedPosition: Int = -1
    var endDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var TAG = "MembershipActivity"
    lateinit var btContinue: Button
    lateinit var rvPrograms: RecyclerView
    lateinit var progressBar: ProgressBar
    lateinit var tvLogout: TextView
    lateinit var tvNoMemAvailable: TextView
    // lateinit var ivBackMem:ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_membership)

        progressBar = findViewById(R.id.progressBar)
        // btContinue = findViewById(R.id.btContinue)
        rvPrograms = findViewById(R.id.rvPrograms)
        tvLogout = findViewById(R.id.tvLogout)
        tvNoMemAvailable=findViewById(R.id.tvNoMemAvailable)
        //ivBackMem=findViewById(R.id.ivBackMem)


        tvLogout.setOnClickListener {
            /* LogoutDialog(this@MembershipActivity, object : CallBack<Int>() {
                 override fun onSuccess(t: Int?) {
                     if (t == 0) {

                         SharedPref.get().clearAll()
                         DataCache.get().clear();



                     }
                 }
             }).show()*/


            SharedPref.get().clearAll()
            this.startActivity(Intent(this, UserActivity::class.java))
            this.finishAffinity()

        }
        getMemberships()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == KeyValue.PAYMENT_RESPONSE && resultCode == Activity.RESULT_OK && data != null) {

            val isSuccess = data.getBooleanExtra(KeyValue.SUCCESS, false)
            if (isSuccess) {
                val transactionId = data.getStringExtra(KeyValue.TRANSACTIONID)
                val memberShipid = data.getIntExtra(KeyValue.MEMBERSHIPID, 0).toString()
                val amount = data.getStringExtra(KeyValue.AMOUNT)
                val frequency = data.getStringExtra(KeyValue.MEMBERSHIP_Frequency)
                addMembership(transactionId, memberShipid, amount, frequency)
            }
        }
    }

    private fun getMemberships() {

        val retrofitApi = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        retrofitApi.getMembershipDetail().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Utils.viewGone(progressBar)
                Log.e("onFailure", t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                Utils.viewGone(progressBar)
                if (response.isSuccessful) {
                    membershipBean = Gson().fromJson(response.body(), MembershipResponse::class.java)
                    val memDisble=membershipBean.data.size

                    if (response.body() == null) {
                      //  Toaster.shortToast("No list available")
                        return
                    }else  if (memDisble.equals(0))
                    {
                        Utils.viewVisible(tvNoMemAvailable)
                        Utils.viewGone(rvPrograms)

                    }else{
                        try {

                            Log.d("sdasdad","sdasjldjakdja: "+membershipBean.data.size)



                            rvPrograms.adapter = MembershipAdapter(object : CallBack<Int>() {
                                override fun onSuccess(position: Int) {
                                    selectedPosition = position
                                    mBeans = membershipBean.data[selectedPosition]

                                    ConfirmMembershipDialog(
                                        this@MembershipActivity,
                                        mBeans.membershipPrice,
                                        object : CallBack<Int>() {
                                            override fun onSuccess(t: Int?) {
                                                if (t == 1) {
                                                    currentSelectedMembershipData = mBeans
                                                    setupGooglePlayFlow(mBeans)

                                                }
                                            }
                                        }).show()
                                }
                            }, membershipBean.data, getWidth())
                        } catch (e: Exception) {
                            Log.e("onException", e.localizedMessage)
                        }
                    }

                } else {
                    Log.e("Un success", response.errorBody().toString())
                }
            }
        })
    }

    private fun getWidth(): Int {
        val displayMetrics = DisplayMetrics()
        windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        return (width * .42).roundToInt()
    }

    private fun addMembership(
        transactionId: String?,
        memberShipid: String,
        amount: String?,
        frequency: String?
    ) {
        try {
            val sharedPref = SharedPref(this)
            val retrofitApi = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
            val hashMap = HashMap<String, String?>()
            if (!Utils.isEmptyString(transactionId)
                && !Utils.isEmptyString(memberShipid)
            ) {

                hashMap["transactionNo"] = transactionId.toString()
                hashMap["membership_id"] = memberShipid
                hashMap["membership_frequency"] = frequency
                hashMap["amountPaid"] = amount.toString()

            }
            val callPurchase = retrofitApi.purchaseMemberShip(hashMap)
            //   showLoader()
            callPurchase.enqueue(object : Callback<JsonObject> {
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    // hideLoader()
                    ErrorUtils.onFailure(t)
                    Log.e(TAG, "on failure ${t.message}")
                }

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    // hideLoader()
                    Log.e(TAG, "on response ")
                    if (response.code() == 200 && response.isSuccessful) {
                        var bean = Gson().fromJson(response.body(), BuyMembershipBean::class.java)
                        sharedPref.save(KeyValue.IS_MEMEBERSHIP_PURCHASE, true)
                        sharedPref.save(
                            KeyValue.MEMBERSHIP_END_DATE,
                            endDate.parse(bean.data.membershipEndDate).time
                        )
                        Toaster.shortToast("Membership Purchased Successfully")
                        /* val it = Intent(this@MembershipActivity, Dashboard::class.java)
                         intent.extras?.let { it1 -> it.putExtras(it1) }
                         startActivity(it)
                         finish()*/
                    } else {
                        ErrorUtils.parseError(response.errorBody()?.string())
                    }
                }
            })
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
            Toaster.somethingWentWrong()
        }
    }
}


