package com.awakenwhitelabeling;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.awakenwhitelabeling.dashboard.spin_wheel.bean.SpinHideBean;
import com.awakenwhitelabeling.retrofit.RetrofitClient;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BackgroundService extends Service {

    public Context context = this;
    public Handler handler = null;
    String spinAudio;
    int audioLen;
    MediaPlayer mp;
    public static Runnable runnable = null;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();

        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                Toast.makeText(context, "Service is still running", Toast.LENGTH_LONG).show();
                handler.postDelayed(runnable, 10000);
            }
        };

        handler.postDelayed(runnable, 15000);

        getSpinAudio();
        prepareAudio(audioLen);



    }

    private int prepareAudio(int audioLen) {
        try {
            if (spinAudio != "" && spinAudio != null) {
                mp=new MediaPlayer();
                mp.setDataSource(RetrofitClient.BASE_URL + spinAudio);
                Log.d("audioooooooLink", "link" + RetrofitClient.BASE_URL + spinAudio);
                mp.prepare();
                audioLen = mp.getDuration();
            } else {
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
return audioLen;
    }


    private void getSpinAudio() {
        Call<SpinHideBean> call = RetrofitClient.getRequest().hideWheelInJava();
        call.enqueue(new Callback<SpinHideBean>() {
            @Override
            public void onResponse(Call<SpinHideBean> call, Response<SpinHideBean> response) {
                if (response.code() == 200) {
                     spinAudio = response.body().getData().getSpinWheelSound();

                }

            }

            @Override
            public void onFailure(Call<SpinHideBean> call, Throwable t) {

                Log.d("BackgroundService","Error while hitting enable Spin for AudioURL"+t.getLocalizedMessage().toString());
            }

        });
    }

    @Override
    public void onDestroy() {
        /* IF YOU WANT THIS SERVICE KILLED WITH THE APP THEN UNCOMMENT THE FOLLOWING LINE */
        //handler.removeCallbacks(runnable);
        Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startid) {
        Toast.makeText(this, "Service started by user.", Toast.LENGTH_LONG).show();
    }
}
