package com.awakenwhitelabeling.Sample;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.awakenwhitelabeling.R;
import com.awakenwhitelabeling.others.Toaster;
import com.awakenwhitelabeling.others.Utils;

public class VideoPlay extends AppCompatActivity {
    private static String EXTRA_URL = "extra.video.url";
    public static void start(Context context, String url) {
        context.startActivity(new Intent(context, VideoPlay.class)
                .putExtra(EXTRA_URL, url));
    }

    private static final String TAG = "VideoPlay";
    //String url = "https://drive.google.com/uc?id=1OIPidxapO631XkolkbYl9Xk0UHjP99at&export=download";
    String url;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        VideoView videoView = findViewById(R.id.videoView);
        url = getIntent().getStringExtra(EXTRA_URL);
        MediaController mediacontroller = new MediaController(VideoPlay.this);
        mediacontroller.setAnchorView(videoView);
        Uri video = Uri.parse(url);
        ProgressBar bar = findViewById(R.id.progress_bar);
        videoView.setMediaController(mediacontroller);
        videoView.setOnCompletionListener(mp -> onBackPressed());
        videoView.setOnPreparedListener(mp -> {
            mp.setOnBufferingUpdateListener((mp1, percent) -> {
                Log.d(TAG, "Buffering % : " + percent);
            });
            Utils.Companion.viewGone(bar);
        });
        videoView.setOnErrorListener((mp, what, extra) -> {
            Toaster.INSTANCE.somethingWentWrong();
            onBackPressed();
            return false;
        });
        videoView.setVideoURI(video);
        videoView.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

}
