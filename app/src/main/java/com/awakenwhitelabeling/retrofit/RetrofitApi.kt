package com.awakenwhitelabeling.retrofit

import com.awakenwhitelabeling.dashboard.dailyworkoutflow.bean.DailyWorkFlowBean
import com.awakenwhitelabeling.dashboard.notes.NotesBean
import com.awakenwhitelabeling.dashboard.notes.noteDetails.notesById.NotesDetailsById
import com.awakenwhitelabeling.dashboard.spin_wheel.NewSpinBean
import com.awakenwhitelabeling.dashboard.spin_wheel.SpinWheelBean
import com.awakenwhitelabeling.dashboard.spin_wheel.bean.SpinHideBean


import com.awakenwhitelabeling.userAction.forgot.ForgotBean
import com.google.gson.JsonObject
import okhttp3.RequestBody

import retrofit2.Call
import retrofit2.http.*

interface RetrofitApi {


    //Register
    @FormUrlEncoded
    @POST("api/register")
    fun register(@FieldMap map: HashMap<String, String>): Call<JsonObject>

    //LoginwithEmail
    @FormUrlEncoded
    @POST("api/login")
    fun loginWithEmail(@FieldMap dff: HashMap<String, String>): Call<JsonObject>


    @FormUrlEncoded
    @POST("api/update_task_status")
    fun updateTaskStatus(@FieldMap dff: HashMap<String, Any>): Call<JsonObject>

    @FormUrlEncoded
    @POST("api/redeem_rewards")
    fun redeem_rewards(@FieldMap rewards_id: HashMap<String, Any>): Call<JsonObject>

    //Membership Detail
    @GET("api/memberships")
    fun getMembershipDetail(): Call<JsonObject>


    @GET("api/user_membership_plan")
    fun getPurchasedMembershipDetail(): Call<JsonObject>


/*
    @GET("api/get_rewards")
    fun getRewards(@Query("offset") page: Int): Call<RewardsBean>
*/

    @GET("api/user_rewards")
    fun getSpinRewards(@Query("offset") page: Int): Call<NewSpinBean>




    @GET("api/get_spinwheel_rewards")
   // fun getWheelRewards():Call<JsonObject>
    fun getWheelRewards(): Call<SpinWheelBean?>?


    @GET("api/enable_spinwheel")
    fun hideWheel(): Call<JsonObject>?


    @GET("api/enable_spinwheel")
    fun hideWheelInJava(): Call<SpinHideBean>?
//commented for future
//    @FormUrlEncoded
//    @POST("api/redeem_rewards")
//    fun spinRedeemRewards(@FieldMap map: HashMap<String, String>): Call<JsonObject>


    @FormUrlEncoded
    @POST("api/redeem_spinwheel")
    fun spinRedeemRewards(@FieldMap map: HashMap<String, String>): Call<JsonObject>



    //forgot password
    @FormUrlEncoded
    @POST("api/create")
    fun forgotPassword(@FieldMap f: HashMap<String, String>): Call<ForgotBean>


//    //Get Suggested Workouts
//    @GET("api/suggested_workouts")
//    fun getSuggestedWorkouts(@Query("page") page: Int): Call<JsonObject>
//
//    //Get New Workouts
//    @GET("api/new_workouts")
//    fun getNewWorkouts(@Query("page") page: Int): Call<JsonObject>
//
//    //Get New Workouts
//    @GET("api/mindset_workouts")
//    fun getMindset(@Query("page") page: Int): Call<JsonObject>
//
//    // Get Routines
//    @GET("api/routine_workouts")
//    fun getRoutines(@Query("page") page: Int): Call<JsonObject>

    @GET("api/payBraintree")
    fun sendCardDetailsToServer(@QueryMap map: HashMap<String, Any?>): Call<JsonObject>


    @GET("api/braintree_payment")
    fun sendCardDetailsToServerForCart(@QueryMap map: HashMap<String, Any?>): Call<JsonObject>

    /*Get braintree payment initiate token*/
    @POST("api/transaction_initiate")
    @FormUrlEncoded
    fun initiateTransaction(@FieldMap map: HashMap<String, Any?>): Call<JsonObject>


    /*Get braintree payment initiate token*/
    @POST("api/transaction")
    @FormUrlEncoded
    fun initiateTransactionForCart(@FieldMap map: HashMap<String, Any?>): Call<JsonObject>

    //Buy Membership
    @FormUrlEncoded
    @POST("api/buyMembership")
    fun purchaseMemberShip(@FieldMap hashMap: HashMap<String, String?>): Call<JsonObject>

    //All Workouts
    @GET("api/workouts")
    fun getAllWorkouts(@Query("page") page: Int): Call<JsonObject>


    //Get All Tags
    @GET("api/get_all_tags")
    fun getAllTagsList(): Call<JsonObject>

    //Get All Workouts By Tags
    @GET("api/get_workouts_based_on_tags")
    fun getWorkoutsByTag(@Query("tags") tags: String): Call<JsonObject>


    @GET("api/get_tasks")
    fun getGroupsList(@Query("offset") i: Int): Call<JsonObject>



    @GET("api/get_user_notes")
    fun getNotesList(@Query("offset") i: Int): Call<NotesBean.Data>



    @GET("api/get_training_details_by_category_id")
    fun getTrainingsDetailByCategoryId(
        @Query("page") page: Int,
        @Query("category_id") workout_id: String
    ): Call<JsonObject>

    @FormUrlEncoded
    @POST("api/addRemoveFavouriteSession")
    fun addRemoveFavourite(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>

    @GET("api/get_user_data")
    fun getUserDetails(): Call<JsonObject>


    @GET("api/get_top_rank_user")
    fun topUserRanking(): Call<JsonObject>


    @GET("api/get_user_notifications")
    fun getNotification(@Query("current_page") page: Int): Call<JsonObject>


    @GET("api/get_google_drive_link")
    fun getAddMediaLink(): Call<JsonObject>


/*
    @GET("api/user_points")
    fun getPoints(): Call<JsonObject>
*/


    @FormUrlEncoded
    @POST("api/changePassword")
    fun changePassword(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>


    @FormUrlEncoded
    @POST("api/save_notes")
    fun addNotes(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
/*
    @FormUrlEncoded
    @POST("api/save_notes")
    fun addNotes(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
*/

    @POST("api/save_notes")
    fun addNotesInMultipart(@Body requestBody: RequestBody): Call<JsonObject>



    @POST("api/update_notes")
    fun updateNotes(@Body requestBody: RequestBody): Call<JsonObject>




    @FormUrlEncoded
    @POST("api/read_notification_by_user")
    fun updateReadStatus(@FieldMap dff: HashMap<String, Any>): Call<JsonObject>



    //
//    @GET()
//    fun getMyFavouriteWorkoutsSession(@Url url: String): Call<FavouriteResponse>
//
//    //Get My completed sessions
//    @GET()
//    fun getMyCompletedWorkoutsSession(@Url url: String): Call<FavouriteResponse>
//
//    //Get My completed sessions
//    @GET()
//    fun getMyMonthlyDates(@Url url: String): Call<CalenderMonthlyDates>
//
//    //Get CompletedSession Workouts
//    @GET("api/getMyCompletedSessions")
//    fun getCompletedSession(): Call<JsonObject>
//
    // Update Profile
    @POST("api/updateProfile")
    fun updateProfile(@Body requestBody: RequestBody): Call<JsonObject>

    //
//    //get messages Profile
//    @GET()
//    fun getAllMessages(@Url url: String): Call<InboxListBaseBeans>
//
//    //get product category
//    @GET()
//    fun getProductCategory(@Url url: String): Call<ProductCategoryBeans>
//
//    //get cart
//    @GET()
//    fun getCartItem(@Url url: String): Call<CartItemResponse>
//
//    //update cart
//    @FormUrlEncoded
//    @POST("api/update_cart")
//    fun updateCart(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
//
//    //get products
//    @GET()
//    fun getProducts(@Url url: String): Call<ProductListingBeans>
//
//    //get products
//    @GET()
//    fun getMyOder(@Url url: String): Call<MyOrderBeans>
//
//    //get address
//    @GET("api/get_user_address")
//    fun getAddress(): Call<AddressListResponse>
//
//    //add address
//    @FormUrlEncoded
//    @POST("api/add_user_address")
//    fun addAddress(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
//
//   //add address
//    @FormUrlEncoded
//    @POST("api/update_user_address")
//    fun updateAddress(@FieldMap hashMap: HashMap<String, Any>): Call<AddressUpdateResponse>
//
//    //remove to address
//    @FormUrlEncoded
//    @POST("api/delete_user_address")
//    fun removeFromAddress(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
//
//
//    //add address
//    @FormUrlEncoded
//    @POST("api/submit_review")
//    fun postReview(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
//
//    //Get Coupon
//    @GET("api/apply_coupon")
//    fun getCouponDetail(@QueryMap map: HashMap<String, Any?>): Call<CouponResponse>
//
//    //add address
//    @FormUrlEncoded
//    @POST("api/transaction")
//    fun getPaymentToken(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
//
//
//    @GET()
//    fun getMessageDetail(@Url url: String): Call<InboxDetailBeans>
//
//    @GET()
//    fun getProductDetail(@Url url: String): Call<ProductDetailBeans>
//
//    //Add to Cart
//    @FormUrlEncoded
//    @POST("api/add_to_cart")
//    fun addToCart(@FieldMap f: HashMap<String, String>): Call<AddtocartResponse>
//
//    //cart count
//    @GET("api/cart_count")
//    fun getCartCount(): Call<JsonObject>
//
//    //Remove Item from cart
//    @FormUrlEncoded
//    @POST("api/remove_item")
//    fun deleteCartItem(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
//
//
    //get unread message count
    @GET("api/unread_count")
    fun getUnreadCount(): Call<JsonObject>

    //Membership Plan
    @GET("api/user_membership_plan")
    fun getMyMembership(): Call<JsonObject>

    //    //Search Workout
//    @GET("api/search_workouts")
//    fun searchWorkout(@Query("search_keyword") search_keyword: String): Call<JsonObject>
//
//
    // get workouts
    @GET("api/training_categories")
    fun getWorkout(@Query("page") page: Int): Call<JsonObject>


    @GET("api/get_user_notes")
    fun getNotes(@Query("page") page: Int): Call<JsonObject>


    @FormUrlEncoded
    @POST("api/delete_note")
    fun deleteNotes(@Field("note_id") note_id: Int): Call<JsonObject>

/*
@FormUrlEncoded
    @POST("api/update_notes")
    fun updateNotes(@Field("note_id") note_id: Int): Call<JsonObject>
*/


/*
    @FormUrlEncoded
    @POST("api/update_notes")
    fun updateNotes(@FieldMap hashMap: HashMap<String, Any>): Call<NotesUpdatedResponseBean>
*/


    @GET()
    fun getTasks(@Url fullUrl: String): Call<DailyWorkFlowBean>


    //
//    @POST("api/cancelSubscription")
//    @FormUrlEncoded
//    fun cancelMembership(@FieldMap hashMap: HashMap<String, Any>): Call<JsonObject>
//
    @POST("api/save_fcm_token")
    @FormUrlEncoded
    fun saveFCMToken(@FieldMap map: HashMap<String, String?>): Call<JsonObject>



    @POST("api/save_fcm_token")
    @FormUrlEncoded
    fun saveFCMTokenNew(@FieldMap map: HashMap<String, String>): Call<JsonObject>



    //
//    @GET("api/get_user_notifications")
//    fun getNotifications(): Call<JsonObject>
//
//
//    @POST("api/read_notification_by_user")
//    @FormUrlEncoded
//    fun notificationReadByUser(@Field("notification_id") notificationId: String): Call<JsonObject>
//
    @GET("api/get_unread_notifications_count")
    fun getNotificationCount(): Call<JsonObject>

//    @GET("api/get_groups_list")
//    fun getGroupsList(@Query("offset") i: Int): Call<BaseListResponse<GroupBean>>
//
//    @POST("api/exit_group")
//    @FormUrlEncoded
//    fun exitGroup(@Field("group_id") i: Long): Call<JsonObject>
//
//    @POST("api/send_message")
//    fun sendMessage(@Body msg: HashMap<String, Any?>): Call<BaseObjectResponse<MessageBean>>
//
//    @GET("api/get_messages_of_group")
//    fun getMessage(@QueryMap map: HashMap<String, String>): Call<BasePaginationResponse<MessageBean>>
//
//    @GET("api/get_spam_messages_of_group")
//    fun getSpamMessages(@QueryMap map: HashMap<String, String>): Call<BasePaginationResponse<MessageBean>>
//
//    @POST("api/delete_messages")
//    fun deleteMessage(@Body map: HashMap<String, Any?>): Call<JsonObject>
//
//    @POST("api/mark_message_as_spam")
//    fun markSpamMessage(@Body map: HashMap<String, Any?>): Call<JsonObject>
//
//    @POST("api/report_group_as_spam")
//    fun reportGroup(
//        @Query("group_id") groupid: Long,
//        @Query("report_message") report: String
//    ): Call<JsonObject>
//
//    @POST("api/send_message")
//    fun sendImageMessage(@Body request: RequestBody): Call<BaseObjectResponse<MessageBean>>
//
//    @GET("api/get_group_details")
//    fun loadGroupDetailById(@Query("group_id") mGroupId: Long): Call<BaseObjectResponse<GroupBean>>


    @GET("https://player.vimeo.com/video/{videoID}/config")
    fun getPublicVimeoPlayableLink(@Path("videoID") videoID: String): Call<JsonObject>

    @GET("https://api.vimeo.com/me/videos/{video_id}")
    fun getVimeoVideoDetails(@Path("video_id") video_id: String): Call<JsonObject>

    @FormUrlEncoded
    @POST("api/initialize_payment")
    fun initializePayment(@FieldMap map: HashMap<String, Any?>): Call<JsonObject>

    @FormUrlEncoded
    @POST("api/save_inapp_payment")
    fun saveInAppPurchase(@FieldMap map: HashMap<String, Any?>): Call<JsonObject>

    // Get Workout Details by id......
    @GET("api/get_task_details_by_id")
    fun getDailyWorkById(@Query("task_id") workout_id: String): Call<JsonObject>
    @GET("api/get_notes_details_by_id")
    fun getNotesById(@Query("notes_id") notes_id: String): Call<NotesDetailsById>


}
