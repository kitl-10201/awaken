package com.awakenwhitelabeling.retrofit;

import android.util.Log;

import com.awakenwhitelabeling.base.App;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public static final String BASE_URL = "https://awaken1app.com/";// Live

    //public static final String BASE_URL = "https://awakendev.kitlabs.us/";
    public static int count = 0;
    public static final String WORKOUT_IMAGE_URL = BASE_URL + "images/";

    public static final String WORKOUT_VIDEO_URL = BASE_URL + "videos/";
    public static final String SPIN_AUDIO = BASE_URL + "tones/";

    public static final String PROFILE_IMAGE_URL = BASE_URL + "images/profile_images/";
    public static final String QR_IMAGE_URL = BASE_URL + "images/qr_code_images/";
    private static Retrofit retrofitUser = null;

    public static Retrofit getRegisterRetrofit() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(getRegister())
                .build();

        return retrofit;
    }

    public static Retrofit getVimeoAccess(String key) {


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(getVimeoClient(key))
                .build();
        return retrofit;
    }

    private static OkHttpClient getVimeoClient(String key) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES).addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + key).build();
                        return chain.proceed(request);
                    }
                });
        return httpClient.build();
    }

    private static OkHttpClient getRegister() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);


        httpClient.connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES).addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("content-type", "application/x-www-form-urlencoded").build();
                        return chain.proceed(request);
                    }
                });
        return httpClient.build();

    }

    public static Retrofit getUserDetails() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofitUser = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(getUser())
                .build();
        return retrofitUser;
    }


    public static Retrofit getDFDdata() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        retrofitUser = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(getUser())
                .build();
        return retrofitUser;
    }


    public static Retrofit getFormData() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofitUser = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(formData())
                .build();

        return retrofitUser;
    }

    private static OkHttpClient formData() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES).addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + App.get().getToken())

                                //.addHeader("Content-Type", "application/x-www-form-urlencoded")
                                .addHeader("Content-Type", "text/plain").build();
                        return chain.proceed(request);
                    }
                });
        return httpClient.build();


    }

    private static OkHttpClient getUser() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES).addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + App.get().getToken())
                                .addHeader("Accept", "application/json").build();
                        Log.e("Login_User_Token: ", App.get().getToken());
                        Response response = chain.proceed(request);
                        if (response.code() == 401) {
                            return response;
                        }
                        return response;
                    }

                });

        return httpClient.build();
    }



/*
    private static OkHttpClient getUser() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES).addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Log.d("TAG", "intercept: " + App.get().getToken());
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + App.get().getToken())

                        //.addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            }
        });
        return httpClient.build();


    }
*/

    private static RetrofitApi apiClient;

    public static RetrofitApi getRequest() {
        count++;
        Log.d("apiHit", "---" + count);
        if (apiClient == null) {

            apiClient = RetrofitClient.getUserDetails().create(RetrofitApi.class);
        }
        return apiClient;
    }


}