package com.awakenwhitelabeling.notification

import android.util.Log
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


object SaveFCMToken {

    /*  fun saveToken(token: String?){
          val map = hashMapOf<String, String?>()
          map["fcm_token"] = token ?: ""
          RetrofitClient.getRequest().saveFCMToken(map).enqueue(object : Callback<JsonObject?> {
              override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
              }

              override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
              }
          })

      }*/


    fun saveToken(token: String?){
        val map = hashMapOf<String, String?>()
        map["fcm_token"] = token ?: ""
        RetrofitClient.getRequest().saveFCMToken(map).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
            }
            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {

                if (response.isSuccessful && response.code()==200)
                {
                    Log.d("fcm res","FCM updated token on Logout!"+response.body())
                }

            }
        })
    }
}