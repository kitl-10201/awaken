package com.awakenwhitelabeling.userAction.login.bean

data class LoginBean(
    val `data`: Data, val status: String
) {
    data class Data(
        val active_status: String,
        val created_at: String,
        val device_type: String,
        val email: String,
        val first_name: String,
        val ibo_number: String,
        val id: Int,
        val image_path: String,
        val last_name: String,
        val last_updated_by: Int,
        val location: String,
        val name: String,
        val password: String,
        val phone: String,
        val register_type: String,
        val socialLogID: String,
        val token: String,
        val updated_at: String
    )
}