package com.awakenwhitelabeling.userAction.login

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.*
import com.awakenwhitelabeling.dashboard.DashboardActivity
import com.awakenwhitelabeling.dashboard.notification.FireBaseNotificationToken
import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.others.*
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.userAction.forgot.ForgotPassword
import com.awakenwhitelabeling.userAction.login.bean.LoginBean
import com.awakenwhitelabeling.userAction.register.RegisterFragment
import com.awakenwhitelabeling.userAction.register.helper.SocialLoginHelper
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import kotlin.collections.HashMap

class LoginFragment : SocialLoginHelper(), View.OnClickListener {
    lateinit var forgot: TextView
    lateinit var login: Button
    lateinit var register: TextView
    lateinit var tilEmail: TextInputLayout
    lateinit var sharedPref: SharedPref
    lateinit var tilPassword: TextInputLayout
    lateinit var ivFacebook: ImageView
    lateinit var email: EditText
    lateinit var password: EditText
    var endDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivFacebook.setOnClickListener {
            actionLoginToFacebook()
        }
    }

    override fun onSocialLoginSuccess(
        socialId: String,
        name: String,
        email: String,
        facebookFb: String
    ) {

        val js = JSONObject()
        js.put("email", email)
        js.put("id", socialId)
        var str = js.put("name", name)
        //js.put("name", name)
        loginWithSocialID(js)
        Log.d("dfsdfhsdfsf", "dfsdfskdfjsf" + email + " " + socialId + " " + name)
    }

    private fun loginWithSocialID(js: JSONObject) {
        showLoader()
        var str: String = js.optString("name")
        var sep = " "
        val parts = str.split(sep)
        Log.d("gfdgdfgdfg", "dfgdfghdfhfd" + parts[0])
        Log.d("gfdgdfgdfg", "dfgdfdfhskdfsghdfhfd" + parts[1])
        val hashMap = HashMap<String, String>()
        hashMap["socialID"] = js.optString("id").toString()
        hashMap["first_name"] = parts[0]
        hashMap["last_name"] = parts[1]
        hashMap["email"] = js.optString("email")
        hashMap[Cons.LOGIN_TYPE] = Cons.FACEBOOK_FB.toString()
        hashMap["device_type"] = "android"
        hashMap["password"] = ""
        hashMap["c_password"] = ""
        println("My hashMap value are:....$hashMap")
        try {

            var loginRequest = RetrofitClient.getRegisterRetrofit().create(RetrofitApi::class.java)
            loginRequest.register(hashMap).enqueue(object : Callback<JsonObject?> {
                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    hideLoader()
                    Log.e(RegisterFragment.TAG, " ${t.localizedMessage} ");
                }

                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                    hideLoader()
                    if (response.isSuccessful && response.code() == 200 && response.body() != null) {
                        Log.d("FBRegister", "FaceBookRegistration Response" + response.body())
                        try {
                            onLoginSuccess(response)
                        } catch (e: Exception) {
                            // Toaster.somethingWentWrong()
                            Log.d("onException", e.localizedMessage)
                        }
                    } else {
                        ErrorUtils.parseError(response?.errorBody()?.string());
                        val jObjError = JSONObject(response.errorBody()?.string())
                        Toaster.shortToast(response.errorBody().toString())
                    }
                }
            })

        } catch (e: Exception) {
            Log.e("onException", e.toString())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        sharedPref = SharedPref(context)
        onCreate()
        setReferences(view)
        return view
    }

    private fun setReferences(view: View?) {
        ivFacebook = view?.findViewById(R.id.ivFacebook) as ImageView
        forgot = view?.findViewById(R.id.tvForgot) as TextView
        login = view.findViewById(R.id.btLogin)
        register = view.findViewById(R.id.tvSignup)
        email = view.findViewById(R.id.etEmail)
        password = view.findViewById(R.id.etPassword)
        tilEmail = view.findViewById(R.id.tilEmail)
        tilPassword = view.findViewById(R.id.tilPassword)
        forgot.setOnClickListener(this)
        register.setOnClickListener(this)
        login.setOnClickListener(this)
    }

    companion object {
        val TAG = "LoginFragment"
        fun newInstance(): Fragment {
            val bundle = Bundle()
            val fragment = LoginFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            forgot -> {
                startActivity(Intent(context, ForgotPassword::class.java))
            }
            login -> {

                if (checkValidation()) {
                    loginIntoApp()

                }
            }
            register -> {
                activity?.supportFragmentManager?.beginTransaction()
                    ?.add(R.id.flUserMain, RegisterFragment.newInstance())
                    ?.commit()
            }
        }
    }

    private fun loginIntoApp() {
        if (!App.get().isConnected()) {
            //  activity?.let { InternetConnectionDialog(it, null).show() }
            popUpInternetConnectionDialog()
            return
        }
        showLoader()
        val hashMap = HashMap<String, String>()
        hashMap[Cons.EMAIL] = email.text.toString()
        hashMap[Cons.PASSWORD] = password.text.toString()

                val loginRequest = RetrofitClient.getRegisterRetrofit().create(RetrofitApi::class.java)
        loginRequest.loginWithEmail(hashMap).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Log.e(TAG, t.localizedMessage)
                Toaster.shortToast(t.localizedMessage.toString())
                hideLoader()
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                Log.d("loginnnndata", "loginnnndata" + response.body())
                hideLoader()
                Log.d(TAG, " Response" + response.body())
                try {
                    if (response.isSuccessful) {
                        if (response.body() == null) {
                            Toaster.somethingWentWrong()
                            return
                        } else {
                            onLoginSuccess(response)
                        }
                    } else {
                        val jObjError = JSONObject(response.errorBody()?.string())
                        Toaster.shortToast(
                            jObjError.getString("message")
                        )
                    }
                } catch (e: Exception) {
                    Log.d("onException", e.localizedMessage)
                    Toaster.somethingWentWrong()
                }
            }
        })
    }

    private fun onLoginSuccess(response: Response<JsonObject?>) {

        val bean = Gson().fromJson(response.body(), LoginBean::class.java)
Log.e("UserToken: ","onLogin: "+bean.data.token)
        sharedPref.save(Cons.username, bean.data.name)
        sharedPref.save(Cons.token, bean.data.token)
        sharedPref.save(Cons.user_email, bean.data.email)
        sharedPref.save(Cons.logintype, bean.data.register_type)
        sharedPref.save(Cons.id, bean.data.id.toString())
        sharedPref.save(Cons.image_path, bean.data.image_path)
        sharedPref.save(Cons.socialLogID, bean.data.socialLogID)
        FireBaseNotificationToken.getToken()
        MembershipRequest.checkMembership(object : CallBack<UserMembershipKBean.Data>() {
            override fun onSuccess(t: UserMembershipKBean.Data?) {
                if (t == null) {
                    startActivity(Intent(context, MembershipActivity::class.java))
                    activity?.finish()
                    return
                }
                if (t?.isValid == "false") {

                    val customDialog = Dialog(requireActivity())
                    customDialog.setContentView(R.layout.dialog_membership_expired)
                    customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                    customDialog.window?.setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )

                    val btnOkAN = customDialog.findViewById(R.id.tvUpdatePlan) as Button
                    var dateValidTil = customDialog.findViewById(R.id.validDate) as TextView

                    var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    var setDateFormat = SimpleDateFormat("MMM d, yy");
                    //  var setTimeFormat = SimpleDateFormat("hh:mm a");
                    val text = setDateFormat.format(getDateFormat.parse(t?.validTo))
                    dateValidTil.text = text + " (EST)"
                    customDialog.setCancelable(false)
                    btnOkAN.setOnClickListener {
                        val intent = Intent(context, MembershipActivity::class.java)
                        startActivity(intent)
                    }
                    customDialog.show()

                } else {
                    MembershipRequest.saveMembershipValidInToLocal()
                    DashboardActivity.startActivity(requireContext())
                }
            }
        })

    }

    private fun validityExpire() {
        val customDialog = Dialog(requireActivity())
        customDialog.setContentView(R.layout.dialog_membership_expired)
        customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val btnOkAN = customDialog.findViewById(R.id.tvUpdatePlan) as TextView
        val date = customDialog.findViewById(R.id.tvCardMsg) as TextView
        customDialog.setCancelable(false)
        btnOkAN.setOnClickListener {
            Toaster.shortToast("Ok")
        }

        customDialog.show()
    }

    private fun checkValidation(): Boolean {
        var valid = false
        when {
            Utils.isEmptyString(email.text.toString()) -> {
                tilEmail.error = "Email Required"
            }
            !Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches() -> {
                tilEmail.error = "Invalid Email"
            }
            Utils.isEmptyString(password.text.toString()) -> {
                tilPassword.error = "Password required"
            }
            else -> {
                valid = true
            }
        }
        return valid
    }

}
