package com.awakenwhitelabeling.userAction.register.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterBean {
    @SerializedName("success")
    @Expose
    public String success;
    @SerializedName("data")
    @Expose
    public Data data;
    public class Data {

        @SerializedName("token")
        @Expose
        public String token;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("active_status")
        @Expose
        public Object activeStatus;
        @SerializedName("phone")
        @Expose
        public Object phone;
        @SerializedName("location")
        @Expose
        public Object location;
        @SerializedName("logintype")
        @Expose
        public String logintype;
        @SerializedName("image_path")
        @Expose
        public String imagePath;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("socialLogID")
        @Expose
        public String socialLogID;

    }





}
