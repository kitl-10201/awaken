package com.awakenwhitelabeling.userAction.register

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.base.SharedPref
import com.awakenwhitelabeling.databinding.FragmentRegisterBinding
import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.others.*
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.userAction.login.LoginFragment
import com.awakenwhitelabeling.userAction.login.bean.LoginBean
import com.google.android.material.textfield.TextInputLayout

import com.google.gson.Gson
import com.google.gson.JsonObject

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegisterFragment : BaseFragment(), View.OnClickListener {
    lateinit var sharedPref: SharedPref
    lateinit var binding: FragmentRegisterBinding
    lateinit var ivBackReg: ImageView
    lateinit var tilCity: TextInputLayout
    lateinit var etCity: EditText
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_register, container, false)
        GenerateKey.key(activity)
        sharedPref = SharedPref(context)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRegisterBinding.bind(view)
        binding.tvSignIn.setOnClickListener(this)
        binding.ivBackReg.setOnClickListener(this)
        binding.btRegister.setOnClickListener(this)
        warnings()

    }


    companion object {
        val TAG = "RegisterFragment"
        fun newInstance(): Fragment {
            val bundle = Bundle()
            val fragment = RegisterFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btRegister -> {
                if (checkValidation()) {
                    registerIntoApp()
                }
            }
            binding.tvSignIn -> {
                activity?.supportFragmentManager?.beginTransaction()
                    ?.add(R.id.flUserMain, LoginFragment.newInstance())
                    ?.commit()


            }
            binding.ivBackReg -> {
                activity?.supportFragmentManager?.beginTransaction()
                    ?.add(R.id.flUserMain, LoginFragment.newInstance())
                    ?.commit()
            }
        }
    }

    private fun registerIntoApp() {

        showLoader()
        val hashMap = HashMap<String, String>()
        hashMap[Cons.FNAME] = binding.etFName.text.toString()
        hashMap[Cons.LNAME] = binding.etLName.text.toString()
        hashMap[Cons.EMAIL] = binding.etEmail.text.toString()
        hashMap[Cons.PASSWORD] = binding.etPassword.text.toString()
        hashMap[Cons.IBONumber] = binding.etIbo.text.toString()
        hashMap["device_type"] = "Android"
        hashMap["address_line_1"] = binding.etAddress1.text.toString()
        hashMap["address_line_2"] = binding.etAddress2.text.toString()
        hashMap["state"] = binding.etState.text.toString()
        hashMap["city"] = binding.etCity.text.toString()
        hashMap["zip"] = binding.etZip.text.toString()
        hashMap[Cons.LOGIN_TYPE] = Cons.NATIVE

        var loginRequest = RetrofitClient.getRegisterRetrofit().create(RetrofitApi::class.java)
        loginRequest.register(hashMap).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()
                Log.e(TAG, " ${t.localizedMessage} ");
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                try {
                    if (response.isSuccessful) {
                        if (response.body() == null) {
                            Toaster.somethingWentWrong()
                            return

                        } else {

                            Log.d("fdgldkfsjdjfl", "glfdgdfkl" + response.body())
                            val bean = Gson().fromJson(response.body(), LoginBean::class.java)
                            sharedPref.save(Cons.username, bean.data.name)
                            sharedPref.save(Cons.token, bean.data.token)
                            sharedPref.save(Cons.user_email, bean.data.email)
                            sharedPref.save(Cons.logintype, bean.data.register_type)
                            sharedPref.save(Cons.id, bean.data.id.toString())
                            sharedPref.save(Cons.image_path, bean.data.image_path)
                            startActivity(Intent(context, MembershipActivity::class.java))
                            activity?.finish()
                        }
                    } else {
                        ErrorUtils.parseError(response?.errorBody()?.string());
                        //Toaster.shortToast(response.errorBody().toString())
                    }
                } catch (e: Exception) {
                    Toaster.somethingWentWrong()
                    Log.d("onException", e.localizedMessage)
                }

            }
        })

    }

    private fun warnings() {

        binding.etFName.addTextChangedListener(
            CustomWatcher(
                binding.etFName,
                binding.tilFName,
                "First name Required",
                CustomWatcher.EditTextType.FirstName
            )

        )

        binding.etLName.addTextChangedListener(
            CustomWatcher(
                binding.etLName,
                binding.tilLName,
                "Last Name Required",
                CustomWatcher.EditTextType.LastName
            )
        )
/*
        binding.etIbo.addTextChangedListener(
            CustomWatcher(
                binding.etIbo,
                binding.tilIbo,
                "IBO number is required",
                CustomWatcher.EditTextType.FirstName
            )
        )
*/

        binding.etEmail.addTextChangedListener(
            CustomWatcher(
                binding.etEmail,
                binding.tilEmail,
                "Email required",
                CustomWatcher.EditTextType.FirstName
            )
        )

        binding.etPassword.addTextChangedListener(
            CustomWatcher(
                binding.etPassword,
                binding.tilPassword,
                "Password required",
                CustomWatcher.EditTextType.FirstName
            )
        )

        binding.etConfirmPassword.addTextChangedListener(
            CustomWatcher(
                binding.etConfirmPassword,
                binding.tilConfirmPassword,
                "Re-password and Password doesn't match.",
                CustomWatcher.EditTextType.FirstName
            )
        )
        binding.etAddress1.addTextChangedListener(
            CustomWatcher(
                binding.etAddress1,
                binding.tilAddress1,
                "This field is required.",
                CustomWatcher.EditTextType.Address1
            )
        )
        binding.etAddress2.addTextChangedListener(
            CustomWatcher(
                binding.etAddress2,
                binding.tilAddress2,
                "This field is required.",
                CustomWatcher.EditTextType.Address2
            )
        )

        binding.etState.addTextChangedListener(
            CustomWatcher(
                binding.etState,
                binding.tilState,
                "This field is required.",
                CustomWatcher.EditTextType.State
            )
        )
        binding.etCity.addTextChangedListener(
            CustomWatcher(
                binding.etCity,
                binding.tilCity,
                "This field is required.",
                CustomWatcher.EditTextType.State
            )
        )






        binding.etZip.addTextChangedListener(
            CustomWatcher(
                binding.etZip,
                binding.tilZip,
                "This field is required",
                CustomWatcher.EditTextType.Zip
            )
        )


    }

    private fun checkValidation(): Boolean {
        var isValid = false
        when {
            Utils.isEmptyString(binding.etFName.text.toString().trim()) -> {
                binding.tilFName.error = "First Name Required"
            }

            Utils.isEmptyString(binding.etLName.text.toString().trim()) -> {
                binding.tilLName.error = "Last Name Required"
            }
            /*Utils.isEmptyString(binding.etIbo.text.toString()) -> {
                binding.tilIbo.error = "IBO number is required"
            }*/

            Utils.isEmptyString(binding.etEmail.text.toString()) -> {
                binding.tilEmail.error = "Email Required"
            }
            !Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.text.toString().trim()).matches() -> {
                binding.tilEmail.error = "Invalid Email"
            }
            Utils.isEmptyString(binding.etPassword.text.toString()) -> {
                binding.tilPassword.error = "Password required"
            }
            !isEqual(binding.etPassword, binding.etConfirmPassword) -> {
                binding.tilConfirmPassword.error = "Password and confirm password doesn't match."
            }

            Utils.isPasswordValid(binding.etPassword.text.toString()) -> {
                binding.tilPassword.error = "Password must contain at least 8 characters"

            }
            Utils.isEmptyString(binding.etAddress1.text.toString()) -> {
                binding.tilAddress1.error = "This field is required"
            }
            Utils.isEmptyString(binding.etState.text.toString()) -> {
                binding.tilState.error = "This field is required"
            }
            Utils.isEmptyString(binding.etZip.text.toString()) -> {
                binding.tilZip.error = "This field is required"
            }
            else -> {
                isValid = true
            }
        }
        return isValid

    }

    private fun isEqual(tv1: EditText, tv2: EditText): Boolean {
        return tv1.text.toString() == tv2.text.toString()
    }

}
