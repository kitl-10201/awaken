package com.awakenwhitelabeling.userAction

import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.widget.Button
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseDialog
import com.awakenwhitelabeling.base.SharedPref
import com.awakenwhitelabeling.others.CallBack


class LogoutDialog(context: Activity, private val callBack: CallBack<Int>) :
    BaseDialog(context) {
    private val mCtx = context
    lateinit var sharedPref: SharedPref
    lateinit var logout_diag: Button
    lateinit var stay_diag: Button

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setCancelable(false)



        requestWindowFeature(Window.FEATURE_NO_TITLE)
        sharedPref = SharedPref(mCtx)
        setContentView(R.layout.dialog_logout)
        setDimBlur(window)


        stay_diag = findViewById(R.id.stay_diag)
        logout_diag = findViewById(R.id.logout_diag)

        logout_diag.setOnClickListener {
            /* this@LogoutDialog.cancel()
             callBack.onSuccess(0)*/
            /*  var mIntent =Intent(context, UserActivity::class.java)
              context.startActivity(mIntent)
              //super.onBackPressed();
              ownerActivity?.finish()*/


        }
        stay_diag.setOnClickListener {
            this@LogoutDialog.cancel()
            callBack.onSuccess(1)
        }

    }
}