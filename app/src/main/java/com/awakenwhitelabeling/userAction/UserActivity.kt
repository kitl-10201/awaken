package  com.awakenwhitelabeling.userAction

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.userAction.login.LoginFragment
import com.awakenwhitelabeling.userAction.register.GenerateKey


class UserActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        GenerateKey.key(this)
        supportFragmentManager.beginTransaction()
            .add(R.id.flUserMain, LoginFragment())
            .commit()
    }

    override fun onStart() {
        super.onStart()
        //Utils.enableFullScreenView(window.decorView)
    }

    override fun onPause() {
        super.onPause()
        //Utils.disableFullScreen(window)
    }


}