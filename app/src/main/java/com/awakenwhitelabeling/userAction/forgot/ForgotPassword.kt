package com.awakenwhitelabeling.userAction.forgot

import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.android.material.textfield.TextInputLayout
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ForgotPassword : BaseActivity() {
    lateinit var tilEmail: TextInputLayout
    val vTAG = "ForgotPassword";
    lateinit var forgot: Button
    var TAG = "Forgot Password"
    lateinit var email: EditText
    lateinit var ivBack: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        setUpReferences()
        ivBack.setOnClickListener {
            onBackPressed()
        }
        forgot.setOnClickListener {
            if (checkValidation()) {
                forgotPassword()
            }
        }
    }


    override fun onStart() {
        super.onStart()
//        if (Build.VERSION.SDK_INT >= 23) {
//            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        } else {
//            // clear FLAG_TRANSLUCENT_STATUS flag:
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//
//            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//
//            // finally change the color
//            window.statusBarColor = ContextCompat.getColor(this, R.color.black)
//        }
    }

    override fun onPause() {
        super.onPause()
        //  window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    private fun checkValidation(): Boolean {
        var valid = false
        when {
            Utils.isEmptyString(email.text.toString()) -> {
                tilEmail.error = "Email Required"
            }
            !Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches() -> {
                tilEmail.error = "Invalid Email"
            }
            else -> {
                valid = true
            }
        }
        return valid
    }

    private fun setUpReferences() {
        tilEmail = findViewById(R.id.tilEmail)
        email = findViewById(R.id.etEmail)
        ivBack = findViewById(R.id.ivBack)
        forgot = findViewById(R.id.btnSend)
    }


    private fun forgotPassword() {


        showLoader()
        val retrofitApi = RetrofitClient.getRegisterRetrofit().create(RetrofitApi::class.java)
        val hashMap = HashMap<String, String>()
        hashMap["email"] = email.text.toString()
        val call = retrofitApi.forgotPassword(hashMap)
        call.enqueue(object : Callback<ForgotBean> {

            override fun onFailure(call: Call<ForgotBean>, t: Throwable) {
                hideLoader()
                Log.e(vTAG, "onFailure ${t.message}")
            }

            override fun onResponse(
                call: Call<ForgotBean>,
                response: Response<ForgotBean>
            ) {
                hideLoader()
                Log.e(vTAG, "on response")
                if (response.code() == 200) {

                    response.body()?.message?.let { Toaster.shortToast(it) }
                    fragmentManager?.popBackStack()
                } else {
                    // Toaster.shortToast("Email you entered is not registered ,please register yourself")
                    val jObjError = JSONObject(response.errorBody()?.string())
                    Toaster.shortToast(
                        jObjError.getString("message")
                    )
                }
            }
        })

    }
}
