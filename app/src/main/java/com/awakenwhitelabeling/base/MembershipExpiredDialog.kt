package com.awakenwhitelabeling.base


import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.widget.TextView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.ResourceUtils

import java.text.SimpleDateFormat

class MembershipExpiredDialog(context: Activity, private val ending: String, private val callBack: CallBack<Int>) :
    BaseDialog(context) {
    var parseDate= SimpleDateFormat("MMM d,yy")
    var endDate= SimpleDateFormat("MMM d, yy")
    var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var setDateFormat = SimpleDateFormat("MMM dd, yyyy");
    private val mCtx = context
    lateinit var sharedPref: SharedPref
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_membership_expired)
        var tvCardMsg=findViewById<TextView>(R.id.tvCardMsg)
        var tvUpdatePlan=findViewById<TextView>(R.id.tvUpdatePlan)
        val date = try {
            setDateFormat.format(getDateFormat.parse(ending))
        } catch (e: Exception) {
            "--"
        }
        sharedPref = SharedPref(mCtx)
        setDimBlur(window)
        tvCardMsg.text ="${ResourceUtils.getString(R.string.msg_membership_expired)} $date"+" (EST)"

      /*  tvTryLater.setOnClickListener {
            this@MembershipExpiredDialog.cancel()
            callBack.onSuccess(0)

        }*/
        tvUpdatePlan.setOnClickListener {
            this@MembershipExpiredDialog.cancel()
            callBack.onSuccess(1)
        }
    }

}