package com.awakenwhitelabeling.base


import android.app.Activity
import android.content.Intent
import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object MembershipRequest {
    fun checkMembership(callBack: CallBack<UserMembershipKBean.Data>){

       val retrofitClient= RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        retrofitClient.getMyMembership().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                callBack.onError("")
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>?) {
                val code = response?.code() ?: 500;
                if (code == 200 && response?.isSuccessful == true) {
                    var userMembershipBean = Gson().fromJson(response?.body(), UserMembershipKBean::class.java)
                    callBack.onSuccess(userMembershipBean.data)
                } else if (code == 400 || code == 401) {
                    callBack.onSuccess(null)
                }
            }
        })
    }


    fun showMembershipExpiredDialog(context: Activity, data: UserMembershipKBean.Data?) {
        MembershipExpiredDialog(
            context,
            data?.validTo ?: "00",
            object : CallBack<Int>() {
                override fun onSuccess(t: Int?) {
                    if (t == 0) {
                        Toaster.shortToast("Please Update the plan!!")
                        context.finishAffinity()
                    } else {
                        DataCache.get().clear()
                        context.startActivity(Intent(context, MembershipActivity::class.java))
                        context.finish()
                    }
                }
            }).show()
    }
    fun saveMembershipValidInToLocal(){
        SharedPref.get().save(KeyValue.IS_MEMEBERSHIP_PURCHASE,true)
    }
}