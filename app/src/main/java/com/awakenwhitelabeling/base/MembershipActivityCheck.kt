package com.awakenwhitelabeling.base

import com.awakenwhitelabeling.membership.NoMembership
import com.awakenwhitelabeling.others.CallBack

abstract class MembershipActivityCheck : BaseActivity() {
//abstract class MembershipActivityCheck : PermissionActivity() {

    private val TAG = "MembershipActivityCheck"
    override fun onStart() {
        super.onStart()
        if (!DataCache.get().isPreviouslyMembershipChecked && App.get().isConnected()) {
            checkMembershipData()
        }

        /*if (DataCache.get().isPreviouslyMembershipChecked) {

            if (DataCache.get().userMembershipBean == null) {

                NoMembership(this@MembershipActivityCheck).show()

            } else if (DataCache.get().userMembershipBean?.data?.isExpired == true) {

                MembershipRequest.showMembershipExpiredDialog(this@MembershipActivityCheck, DataCache.get().userMembershipBean?.data)
            }
        }*/

    }

    private fun checkMembershipData() {

        MembershipRequest.checkMembership(object : CallBack<UserMembershipKBean.Data>() {
            override fun onSuccess(data: UserMembershipKBean.Data?) {
                if (data == null) {
                    NoMembership(this@MembershipActivityCheck).show()
                } else if (data.isValid=="false") {
                        MembershipRequest.showMembershipExpiredDialog(this@MembershipActivityCheck, data)
                    }
            }

            override fun onError(error: String?) {
                super.onError(error)
            }
        })
    }







}