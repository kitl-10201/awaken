package com.awakenwhitelabeling.base;

import android.text.TextUtils;

import com.awakenwhitelabeling.membership.bean.Frequency;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserMembershipBean {

    @SerializedName("success")
    @Expose
    public String success;
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("membership_name")
        @Expose
        public String membershipName;
        @SerializedName("membership_price")
        @Expose
        public String membershipPrice;
        @SerializedName("frequency")
        @Expose
        private String frequency;
        @SerializedName("membership_start_date")
        @Expose
        public String membershipStartDate;
        @SerializedName("membership_end_date")
        @Expose
        public String membershipEndDate;

        @SerializedName("isValid")
        @Expose
        private String isExpired;

        @SerializedName("subscription_id")
        @Expose
        private String subscriptionId;
        @SerializedName("isCanceledSubscription")
        @Expose
        private String isCanceledSubscription;

        @SerializedName("membership_description")
        @Expose
        public String membershipDescription;

        public boolean IsSubscriptionCanceled(){
            return TextUtils.equals(isCanceledSubscription, "true");
        }

        public void setIsSubscriptionCanceled(boolean value){
            isCanceledSubscription = Boolean.toString(value);
        }


        @SerializedName("pending_time")
        @Expose
        private Long pendingTime;

        public Long getPendingTime() {
            return pendingTime;
        }

        public boolean getIsExpired() {
            return TextUtils.equals("true", isExpired);
        }

        public String getSubscriptionId() {
            return subscriptionId;
        }

        public void setSubscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
        }

        public Frequency getFrequency() {

            switch (frequency) {
                case "perday":
                    return Frequency.DAILY;

                case "weekly":
                    return Frequency.WEEKLY;

                case "monthly":
                    return Frequency.MONTHLY;

                case "bi-yearly":
                    return Frequency.BI_YEARLY;

                case "yearly":
                    return Frequency.YEARLY;

                default:
                    return Frequency.NULL;

            }
        }
    }


}
