package com.awakenwhitelabeling.base

import android.content.Context
import android.os.Bundle
import android.view.Window
import android.widget.Button
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.Cons


class InternetConnectionDialog(context: Context) : BaseDialog(context) {
lateinit var retry:Button

    constructor(context: Context, funValue: Int, callback: CallBack<Int>?) : this(context) {
        mCtx = context
        action = funValue
        callBack = callback
        isRetried = false
    }

    constructor(context: Context, callback: CallBack<Int>?) : this(context) {
        mCtx = context
        action = Cons.NO_ACTION
        callBack = callback
        isRetried = false
    }

    companion object {

        lateinit var mCtx: Context
        var action: Int? = null
        var callBack: CallBack<Int>? = null
        var isRetried = false

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(true)
        setCanceledOnTouchOutside(false)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_no_internet_connection)

        setDimBlur(window)
         retry = findViewById(R.id.retry)
         retry.setOnClickListener {
        // Toaster.shortToast("Clicked")
          this@InternetConnectionDialog.dismiss()
         }
    }

}