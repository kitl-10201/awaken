package com.awakenwhitelabeling.base;


import com.awakenwhitelabeling.dashboard.dailyworkoutflow.bean.DailyWorkFlowBean;
import com.awakenwhitelabeling.notification.SaveFCMToken;
import com.awakenwhitelabeling.dashboard.account.UserDetailsBean;
import com.awakenwhitelabeling.workouts.NewWorkoutsBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataCache {

    private static DataCache singleton;

    private UserDetailsBean.Data userData;
    private Map<String, List<NewWorkoutsBean.Datum>> workouts;
    private Map<String, List<DailyWorkFlowBean.Data>> tasks;
    private UserMembershipKBean userMembershipBean;
    private boolean previouslyMembershipChecked;

    private DataCache() {
        userData = null;
        workouts = new HashMap<>();
        tasks = new HashMap<>();
        previouslyMembershipChecked = false;
    }


    public synchronized static DataCache get() {
        if (singleton == null) {
            synchronized (DataCache.class) {
                if (singleton == null) {
                    singleton = new DataCache();
                }
            }
        }
        return singleton;
    }

    public UserDetailsBean.Data getUserData() {
        return userData;
    }

    public void setUserData(UserDetailsBean.Data userData) {
        this.userData = userData;
    }


    public void clear() {
        try {
            userData = null;
            workouts.clear();
            tasks.clear();
            previouslyMembershipChecked = false;
            userMembershipBean = null;
            SaveFCMToken.INSTANCE.saveToken("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveWorkout(String key, List<NewWorkoutsBean.Datum> value) {
        this.workouts.put(key, value);
    }

    public void saveTasks(String key, List<NewWorkoutsBean.Datum> value) {
        this.workouts.put(key, value);
    }


    public List<NewWorkoutsBean.Datum> getWorkout(String key) {
        return this.workouts.get(key);
    }




    public List<DailyWorkFlowBean.Data> getTasks(String key) {
        return this.tasks.get(key);
    }


    public UserMembershipKBean getUserMembershipBean() {
        return userMembershipBean;
    }

    public void setUserMembershipBean(UserMembershipKBean userMembershipBean) {
        this.userMembershipBean = userMembershipBean;
    }

    public boolean isPreviouslyMembershipChecked() {
        return previouslyMembershipChecked;
    }

    public void setPreviouslyMembershipChecked(boolean previouslyMembershipChecked) {
        this.previouslyMembershipChecked = previouslyMembershipChecked;
    }
}
