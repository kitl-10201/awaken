package com.awakenwhitelabeling.base

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.TextView
import com.awakenwhitelabeling.R

class ErrorEventHelper {
    fun Error_dialog(activity: Activity, msg: String, boolean: Boolean) {
        val dialog = Dialog(activity)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.error_dailog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val text = dialog.findViewById<TextView>(R.id.tv_message_dialog)
        text.text = msg
        val dialogButton = dialog.findViewById<TextView>(R.id.tv_ok_error_dialog)
        if (boolean) {
            dialogButton.setOnClickListener {

                dialog.dismiss()
            }
        } else
            dialogButton.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

}