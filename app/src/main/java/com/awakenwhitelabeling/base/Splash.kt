package com.awakenwhitelabeling.base

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.DashboardActivity
import com.awakenwhitelabeling.dashboard.notification.NotificationActivity
import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.others.Cons
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.userAction.UserActivity

import java.text.SimpleDateFormat


class Splash : AppCompatActivity() {
    lateinit var sharedPref: SharedPref
    private val TAG: String = "Splash"
    var endDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        sharedPref = SharedPref(applicationContext)
        // FirebaseApp.initializeApp(this)

        //WebVideoActivity.start(this);


    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }

    override fun onStart() {

        super.onStart()
//        throw RuntimeException("Test Crash")

        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        sharedPref = SharedPref(applicationContext)
        val iv = findViewById<ImageView>(R.id.ivWelcome)
        val token = sharedPref.getStringValue(Cons.token)
        Log.d(TAG, "onStart: User Token $token")
        iv.postDelayed({

            val extras = intent.extras
            if (extras != null) {
                val intent = Intent(this, NotificationActivity::class.java)
                intent.putExtra(Cons.Notification, Cons.Notification)
                startActivity(intent)
            } else {

                when {
                    Utils.isEmptyString(token) -> {
                        startActivity(Intent(this, UserActivity::class.java))
                        this.finish()
                    }
                    !SharedPref.get().get(KeyValue.IS_MEMEBERSHIP_PURCHASE, false) -> {

                        startActivity(Intent(this, MembershipActivity::class.java))
                        this.finish()
                    }

                    else -> {

                        startActivity(Intent(this, DashboardActivity::class.java))
                        this.finish()
                    }

/*
                else ->
                {
         if (!(this).isFinishing) {
                        startActivity(
                            Intent(
                                this,
                                MembershipActivity::class.java
                                //DashboardActivity::class.java
                            )
                        )
                        finish()
                        MembershipRequest.checkMembership(object :
                            CallBack<UserMembershipKBean.Data>() {
                            override fun onSuccess(data: UserMembershipKBean.Data?) {
                                when {
                                    data == null -> {

                                        NoMembership(this@Splash).show()
                                    }
                                    data.equals("false") -> {
                                        MembershipRequest.showMembershipExpiredDialog(
                                            this@Splash, data
                                        )
                                    }
                                    else -> {
                                        DashboardActivity.startActivity(this@Splash)
                                    }
                                }
                            }
                        })
                    }

                }*/


                }

            }


        }, 2000)

    }

    override fun onPause() {
        super.onPause()
        window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }
}