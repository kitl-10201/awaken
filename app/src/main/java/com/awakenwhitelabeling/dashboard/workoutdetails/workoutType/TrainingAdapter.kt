package com.awakenwhitelabeling.dashboard.workoutdetails.workoutType

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.workoutdetails.bean.TrainingDetailByIdBean
import com.awakenwhitelabeling.others.PicassoUtil
import com.awakenwhitelabeling.vimeoPlayable.VideoDetailsActivity

class TrainingAdapter(
    val data: ArrayList<TrainingDetailByIdBean.Data.TrainingSessions.Data>,
    trainingDetailsActivity: TrainingDetailsActivity
    //val data: List<Tr.Datum>?
) : RecyclerView.Adapter<TrainingAdapter.ViewHolder>() {
    var listener: LoadMoreListener? = trainingDetailsActivity


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.training_workout_details_list, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var workout = data?.get(position)
        downloadMore(position)
        holder.videoName.text = workout.training_name
        holder.videoDuration.text = "Duration : " + workout.duration + " mins"
        holder.videoDesc.text = workout.training_description
        PicassoUtil.loadImage(holder.video_image, workout.training_featured_image)
        //  PicassoUtil.loadImage(categoryImage,m t?.category_featured_image)
        holder.main.setOnClickListener {
            // TrainingDetailsActivity.startForWorkout(holder.itemView.context, workout)
            Log.d("VideoUrl", "VideoUrlToPalyInNewWindow: " + workout.toString())
            VideoDetailsActivity.trainingVideo(holder.itemView.context, workout)
        }
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var main = itemView.findViewById<CardView>(R.id.cvWorkoutMainCN)
        var video_image = itemView.findViewById<ImageView>(R.id.video_image)
        var videoName = itemView.findViewById<TextView>(R.id.VideoName)
        var videoDuration = itemView.findViewById<TextView>(R.id.videoDuration)
        var videoDesc = itemView.findViewById<TextView>(R.id.videoDesc)
    }

    interface LoadMoreListener {
        fun onLoadMore()
    }

    var previousGetCount = 0

    //@Synchronized
    private fun downloadMore(position: Int) {
        if (position > data!!.size - 3 && previousGetCount != data.size) {
            listener?.onLoadMore()
            previousGetCount = data.size

        }
/*val itemCount = itemCount
        if (position == perpage - 2 && itemCount >= perpage) {
            if (itemCount >= previousGetCount) {
                if (listener != null) {
                    listener?.onLoadMore()
                    previousGetCount = itemCount
                }
            }

        }*/

    }
}








