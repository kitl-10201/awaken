package com.awakenwhitelabeling.dashboard.workoutdetails.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TrainingWorkoutListBean implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("active_status")
    @Expose
    public Integer activeStatus;
    @SerializedName("category_name")
    @Expose
    public String categoryName;
    @SerializedName("category_slug")
    @Expose
    public String categorySlug;
    @SerializedName("category_description")
    @Expose
    public String categoryDescription;
    @SerializedName("category_featured_image")
    @Expose
    public String categoryFeaturedImage;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("training_sessions")
    @Expose
    public List<Datum> trainingSessions;

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;


    public class Data implements Serializable{

        public Data() {
        }



/*
        public Data(List<TrainingWorkoutListBean.Datum> data) {
            this.data = data;
        }*/
}
   public class Datum implements Serializable {
       public Datum() {
       }
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("user_id")
        @Expose
        public Integer userId;
        @SerializedName("active_status")
        @Expose
        public String activeStatus;
        @SerializedName("training_name")
        @Expose
        public String trainingName;
        @SerializedName("training_description")
        @Expose
        public String trainingDescription;
        @SerializedName("training_featured_image")
        @Expose
        public String trainingFeaturedImage;
        @SerializedName("training_video_url")
        @Expose
        public String trainingVideoUrl;
        @SerializedName("training_remote_video")
        @Expose
        public String trainingRemoteVideo;
        @SerializedName("associated_category")
        @Expose
        public Integer associatedCategory;
        @SerializedName("training_meeting_url")
        @Expose
        public String trainingMeetingUrl;
        @SerializedName("training_platform")
        @Expose
        public String trainingPlatform;
        @SerializedName("training_online_platform")
        @Expose
        public String trainingOnlinePlatform;
        @SerializedName("training_meeting_id")
        @Expose
        public String trainingMeetingId;
        @SerializedName("training_meeting_password")
        @Expose
        public String trainingMeetingPassword;
        @SerializedName("sort_order")
        @Expose
        public Integer sortOrder;
        @SerializedName("duration")
        @Expose
        public String duration;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;

    }

}