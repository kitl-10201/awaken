package com.awakenwhitelabeling.dashboard.workoutdetails.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SessionItem implements Serializable {

    @SerializedName("active_status")
    public String activeStatus;
    @SerializedName("associated_workout")
    public Long associatedWorkout;
    @SerializedName("created_at")
    public String createdAt;
    @SerializedName("duration")
    public String duration;
    @SerializedName("id")
    public String id;
    @SerializedName("sort_order")
    public String sortOrder;
    @SerializedName("updated_at")
    public String updatedAt;
    @SerializedName("user_id")
    public String userId;

    @SerializedName("training_description")
    public String trainingDescription;

    @SerializedName("training_featured_image")
    public String trainingFeaturedImage;

    @SerializedName("training_video_url")
    public String trainingVideoUrl;

    @SerializedName("training_remote_video")
    public String trainingRemoteVideo;



    @SerializedName("associated_category")
    public String associatedCategory;

    @SerializedName("training_name")
    public String trainingName;


    @SerializedName("training_meeting_url")
    public String trainingMeetingUrl;


    @SerializedName("training_platform")
    public String trainingPlatform;
    @SerializedName("training_online_platform")
    public String trainingOnlinePlatform;

    @SerializedName("training_meeting_id")
    public String trainingMeetingId;

    @SerializedName("complete_added_date")
    public String complete_added_date;

    @SerializedName("training_meeting_password")
    public String trainingMeetingPassword;


    @SerializedName("is_complete_session")
    public String is_complete_session;


    @SerializedName("added_in_calendar")
    public String added_in_calendar;


   /* public boolean IsSessionFav() {
        return TextUtils.equals(is_fav_session, "1");
    }
    public void setSessionFav(boolean val){
        is_fav_session = val ? "1" : "0";
    }*/
}
