package com.awakenwhitelabeling.dashboard.workoutdetails.bean

import java.io.Serializable

data class TrainingDetailByIdBean(
    val `data`: Data,
    val message: String,
    val status: String
) {
    data class Data(
        val active_status: Int,
        val category_description: String,
        val category_featured_image: String,
        val category_name: String,
        val category_slug: String,
        val created_at: String,
        val id: Int,
        val training_sessions: TrainingSessions,
        val updated_at: String
    ) {
        data class TrainingSessions(
            val current_page: Int,
            val `data`: List<Data>,
            val first_page_url: String,
            val from: Int,
            val last_page: Int,
            val last_page_url: String,
            val next_page_url: Any,
            val path: String,
            val per_page: Int,
            val prev_page_url: Any,
            val to: Int,
            val total: Int
        ) {
            data class Data(
                val active_status: String,
                val associated_category: Int,
                val created_at: String,
                val duration: String,
                val id: Int,
                val sort_order: Int,
                val training_description: String,
                val training_featured_image: String,
                val training_meeting_id: String,
                val training_meeting_password: String,
                val training_meeting_url: String,
                val training_name: String,
                val training_online_platform: String,
                val training_platform: String,
                val training_remote_video: String,
                val training_video_url: String,
                val updated_at: String,
                val user_id: Int
            ):Serializable
        }
    }
}