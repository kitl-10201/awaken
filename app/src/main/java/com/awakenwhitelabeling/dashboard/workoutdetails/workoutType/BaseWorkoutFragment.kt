/*
package com.awaken.dashboard.workoutdetails.workoutType

import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import androidx.core.text.isDigitsOnly
import com.awaken.R
import com.awaken.base.BaseFragment
import com.awaken.others.PicassoUtil
import com.awaken.others.Utils
import com.awaken.workouts.NewWorkoutsBean


abstract class BaseWorkoutFragment : BaseFragment() {

    protected lateinit var workoutData: NewWorkoutsBean.Datum

    fun onUiVisible(view: View) {
        // got CR from Client to change with Workout
        Utils.setText(view.findViewById(R.id.tvWorkoutName), "Workout")
        //Utils.setText(view.findViewById(R.id.tvWorkoutName), workoutData.workoutName)
        Utils.setText(view.findViewById(R.id.tvTitle), workoutData.categoryName)

       // Utils.setText(view.findViewById(R.id.tvDuration), workoutData.count)

        PicassoUtil.loadImage(
            view.findViewById(R.id.workout_image),
            workoutData.workoutFeaturedImage
        )

        WorkoutDetailHelper.setDurationOfWorkout(
            view.findViewById(R.id.tvDuration),
            if (!Utils.isEmptyString(workoutData.duration) && workoutData.duration.isDigitsOnly())
                workoutData.duration else "0"
        )
        view.findViewById<ImageView>(R.id.ivBack).setOnClickListener { activity?.finish() }
        Utils.setText(view.findViewById(R.id.tvDuration), workoutData.duration+" min")
        Utils.setText(view.findViewById(R.id.tvDescription), workoutData.count+" Videos")
      //  view.ivBookMark.visibility = if (workoutData.showBookMark) View.VISIBLE else View.GONE

       // setFav(workoutData?.is_fav_session)
//        setAddedToCalender(workoutData?.added_in_calendar)
//        setCompletedSession(workoutData?.is_complete_session)

    }

//    fun setCompletedSession(isCompleteSession: String?) {
//        if (isCompleteSession == "1") {
//            Utils.setImage(view?.findViewById(R.id.ivCompleted), R.drawable.ic_completed)
//            Utils.setUnclickable(view?.findViewById(R.id.ivCompleted))
//        } else {
//            Utils.setImage(view?.findViewById(R.id.ivCompleted), R.drawable.ic_not_completed)
//        }
//    }
//
//    fun setAddedToCalender(addedInCalendar: String?) {
//
//        if (addedInCalendar == "1") {
//            Utils.setImage(view?.findViewById(R.id.ivCalander), R.drawable.ic_added_calnder)
//            //  Utils.setUnclickable(view?.findViewById(R.id.ivCalander))
//        } else Utils.setImage(view?.findViewById(R.id.ivCalander), R.drawable.ic_notadded_calander)
//
//    }

//    fun setFav(is_fav_session: String?) {
//        if (is_fav_session == "1") {
//            Utils.setImage(view?.findViewById(R.id.ivFav), R.drawable.ic_baseline_favorite_24)
//        } else Utils.setImage(
//            view?.findViewById(R.id.ivFav),
//            R.drawable.ic_baseline_favorite_border_24
//        )
//    }

    override fun onStart() {
        super.onStart()
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }
}*/
