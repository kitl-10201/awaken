/*
package com.awaken.dashboard.workoutdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.awaken.R
import com.awaken.dashboard.workoutdetails.workoutType.BaseWorkoutFragment
import com.awaken.others.Toaster
import com.awaken.others.Utils
import com.awaken.workouts.NewWorkoutsBean


class DescriptionWorkout : BaseWorkoutFragment() {
lateinit var ivPlay:ImageView
    companion object {
        private const val EXTRA_WORKOUT = "extra.workout.data"
        fun newInstance(data: NewWorkoutsBean.Datum): DescriptionWorkout {
            val args = Bundle()
            args.putSerializable(EXTRA_WORKOUT, data)
            val fragment = DescriptionWorkout()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // layout is completely same with one small difference

        // ImageView (ivPlay)
        // we don't need to show in this Fragment
        // else all layout is same
       return inflater.inflate(R.layout.fragment_recorded_workout, container, false)
        //return inflater.inflate(R.layout.training_workout_details_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivPlay=view.findViewById(R.id.ivPlay)
        workoutData = arguments?.getSerializable(EXTRA_WORKOUT) as NewWorkoutsBean.Datum
        onUiVisible(view)
       // Toaster.shortToast("fragmentName = "+fragmentManager?.fragments)

        Utils.viewGone(ivPlay)
    }

}*/
