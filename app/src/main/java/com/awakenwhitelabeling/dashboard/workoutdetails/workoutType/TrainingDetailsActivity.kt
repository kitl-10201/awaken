package com.awakenwhitelabeling.dashboard.workoutdetails.workoutType

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.App
import com.awakenwhitelabeling.base.BaseActivity
import com.awakenwhitelabeling.dashboard.DashboardActivity
import com.awakenwhitelabeling.dashboard.training.bean.TrainingListBean
import com.awakenwhitelabeling.dashboard.workoutdetails.bean.TrainingDetailByIdBean
import com.awakenwhitelabeling.others.*
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.devs.readmoreoption.ReadMoreOption
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrainingDetailsActivity : BaseActivity(), TrainingAdapter.LoadMoreListener {
    private val TAG: String = "TrainingDetailsActivity"
    private lateinit var preScreenData: TrainingListBean.Data.Data
    var newWorkoutData = ArrayList<TrainingDetailByIdBean.Data.TrainingSessions.Data>()

    private lateinit var newWorkouts: RecyclerView

    private lateinit var apiEndPoint: String
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var categoryName: TextView
    private lateinit var tvTrainingVideoTitle: TextView
    private lateinit var categoryImage: ImageView
    private lateinit var ivBack: ImageView
    var pageNumber = 1
    private lateinit var trainingAdapter: TrainingAdapter

    companion object {
        const val EXTRA_WORKOUT = "extra.workout.data"
        private const val EXTRA_FULL_DETAIL = "extra.workout.full.detail"

        // method to start activity to show workout detail
        fun startForWorkout(context: Context, data: TrainingListBean.Data.Data) {
            context.startActivity(
                Intent(context, TrainingDetailsActivity::class.java)
                    .putExtra(EXTRA_WORKOUT, data)
                    .putExtra(EXTRA_FULL_DETAIL, true)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trainingdetails)
        parseBundle()
        setTrainingLstRefrence()
    }


    private fun setTrainingLstRefrence() {
        newWorkouts = findViewById(R.id.rvTrainingdetails)
        categoryName = findViewById(R.id.tvWorkoutName)
        tvTrainingVideoTitle = findViewById(R.id.tvTrainingVideoTitle)
        categoryImage = findViewById(R.id.workout_image)
        ivBack = findViewById(R.id.ivBack)
        ivBack.setOnClickListener {
            var mIntent = Intent(this, DashboardActivity::class.java)
            startActivity(mIntent)
        }
        getWorkoutDetail(
            preScreenData.id.toString(),
            object : CallBack<TrainingDetailByIdBean.Data>() {
                override fun onSuccess(t: TrainingDetailByIdBean.Data?) {

                    categoryName.text = t?.category_name
                    //   tvTrainingVideoTitle.text=t?.category_description
                    var readMoreOption: ReadMoreOption =
                        ReadMoreOption.Builder(this@TrainingDetailsActivity)
                            .textLength(5, ReadMoreOption.TYPE_LINE) //OR
                            //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
                            .moreLabel("Read More")
                            .lessLabel("Read Less")
                            .moreLabelColor(Color.BLUE)
                            .lessLabelColor(Color.BLUE)
                            .labelUnderLine(true)
                            .expandAnimation(true)
                            .build();
                    readMoreOption.addReadMoreTo(tvTrainingVideoTitle, t?.category_description);


                    t?.training_sessions?.data?.let { newWorkoutData.addAll(it) }
                    trainingAdapter.notifyDataSetChanged()
                    PicassoUtil.loadImage(categoryImage, t?.category_featured_image)

                }
            })
        trainingAdapter = TrainingAdapter(newWorkoutData, this@TrainingDetailsActivity)
        newWorkouts.apply {
            adapter = trainingAdapter
        }
    }

    private fun parseBundle() {
        if (intent.extras != null) {
            if (intent.hasExtra(EXTRA_WORKOUT)) {
                preScreenData =
                    intent.getSerializableExtra(EXTRA_WORKOUT) as TrainingListBean.Data.Data
            }
        }
    }

    fun getWorkoutDetail(id: String, callBack: CallBack<TrainingDetailByIdBean.Data>) {
        showLoader()
        if (!App.get().isConnected()) {
            Toaster.shortToast("Please connect to internet to see more details...")
            callBack.onError(null);
            return
        }
        RetrofitClient.getRequest().getTrainingsDetailByCategoryId(pageNumber, id)

            .enqueue(object : Callback<JsonObject?> {
                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    hideLoader()
                    ErrorUtils.onFailure(t)
                    callBack.onSuccess(null)
                }

                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>?) {
                    try {
                        val code = response?.code() ?: 500
                        if (code == 200 && response?.isSuccessful == true) {
                            hideLoader()
                            callBack.onSuccess(
                                Gson().fromJson(
                                    Utils.convertToJSON(response.body())?.optJSONObject("data")
                                        ?.toString() ?: "",
                                    TrainingDetailByIdBean.Data::class.java
                                )
                            )

                            Log.d("TrainingDetailsActivity", "You data: " + response.body())


                        } else {

                            callBack.onSuccess(null)
                            val jObjError = JSONObject(response?.errorBody()?.string())
                            Toaster.shortToast(jObjError.getString("message"))
                        }
                    } catch (e: Exception) {
                        callBack.onSuccess(null)
                        Toaster.somethingWentWrong()
                    }
                }
            })
    }

    override fun onLoadMore() {
        pageNumber += 1

        RetrofitClient.getRequest()
            .getTrainingsDetailByCategoryId(pageNumber, preScreenData.id.toString())

            .enqueue(object : Callback<JsonObject?> {
                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    pageNumber -= 1
                }

                override fun onResponse(
                    call: Call<JsonObject?>,
                    response: Response<JsonObject?>?
                ) {
                    try {
                        if (response != null) {
                            if (response.isSuccessful && response.code() == 200) {
                                val data =
                                    Gson().fromJson(
                                        response.body(),
                                        TrainingDetailByIdBean::class.java
                                    )
                                if (data?.data?.training_sessions?.data?.size != 0) {
                                    newWorkoutData.addAll(data.data.training_sessions.data)
                                    trainingAdapter.notifyDataSetChanged()
                                    //   addMoreNotificationsData(data.data)
                                    // isMoreDataAvailable = true
                                } else {
                                    //isMoreDataAvailable = false
                                    pageNumber -= 1
                                }

                            } else {
                                // isMoreDataAvailable = false
                                pageNumber -= 1
                            }
                        }
                    } catch (e: Exception) {
                    }
                }
            })
    }
}


