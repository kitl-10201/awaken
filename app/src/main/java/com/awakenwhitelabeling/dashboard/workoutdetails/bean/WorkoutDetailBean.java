package com.awakenwhitelabeling.dashboard.workoutdetails.bean;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WorkoutDetailBean implements Serializable {

    @SerializedName("active_status")
    public Long activeStatus;
    @SerializedName("created_at")
    public String createdAt;
    @SerializedName("duration")
    public String duration;
    @SerializedName("id")
    public Long id;
    @SerializedName("tags")
    public String tags;
    @SerializedName("updated_at")
    public String updatedAt;
    @SerializedName("user_id")
    public Long userId;
    @SerializedName("category_description")
    public String categoryDescription;
    @SerializedName("category_featured_image")
    public String categoryFeaturedImage;
    @SerializedName("category_featured_video")
    public String categoryFeaturedVideo;
    @SerializedName("category_meeting_id")
    public String categoryMeetingId;
    @SerializedName("category_meeting_password")
    public String categoryMeetingPassword;
    @SerializedName("category_meeting_url")
    public String categoryMeetingUrl;
    @SerializedName("category_name")
    public String categoryName;

    @SerializedName("count")
    public String count;

    @SerializedName("category_online_platform")
    public String categoryOnlinePlatform;
    @SerializedName("category_platform")
    public String categoryPlatform;
    @SerializedName("category_remote_video")
    public String categoryRemoteVideo;
    @SerializedName("category_sessions")
    @Expose
    //public WorkoutSessions workoutSessions;
    public List<SessionItem> sessionItems;
    @SerializedName("category_type")
    public String categoryType;

   /* @SerializedName("isUserFav")
    @Expose
    private String isUserFav;

    public boolean showBookMark;

    public boolean IsUserFav() {
        return TextUtils.equals(isUserFav, "true");
    }

    public void setUserFav(boolean val){
        isUserFav = val ? "true" : "false";
    }*/
}
