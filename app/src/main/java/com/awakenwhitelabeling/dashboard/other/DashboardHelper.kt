package com.awakenwhitelabeling.dashboard.other

import android.annotation.SuppressLint
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


@SuppressLint("StaticFieldLeak")
object DashboardHelper {

    object FragNumber {
        const val TRAINING = 0 // HomeFragment
        const val DailyWorkFlow = 1
        const val NOTES = 2
        const val ACCOUNT = 3


    }

    private lateinit var toolbar: Toolbar
    private var tvNotification: TextView? = null

    fun setupSideMenu(rvSideMenu: RecyclerView, callBack: CallBack<Int>) {
        rvSideMenu.adapter = SideNavAdapter(callBack)
    }

    fun setToolbar(toolbar: Toolbar) {
        DashboardHelper.toolbar = toolbar
    }

    fun setTitle(title: String) {
        toolbar.title = title
    }


    fun setIcon(icon: Int) {
        toolbar.setNavigationIcon(icon)
    }

    fun setTVCount(tvNotification: TextView) {
        DashboardHelper.tvNotification = tvNotification
        Utils.viewGone(DashboardHelper.tvNotification)
    }

    fun getNotificationCount() {
        RetrofitClient.getRequest().getNotificationCount().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {}

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>?) {
                val code = response?.code() ?: 500
                if (code == 200 && response?.isSuccessful == true) {
                    val json = Utils.convertToJSON(response?.body())
                    val count = json.optInt("data", 0)
                    when {
                        count <= 0 -> {
                            Utils.viewGone(tvNotification)
                        }
                        count < 100 -> {
                            Utils.viewVisible(tvNotification)
                            tvNotification?.text = count.toString()
                        }
                        else -> {
                            Utils.viewVisible(tvNotification)
                            tvNotification?.text = "99+"
                        }
                    }
                }
            }
        })
    }

}
