package com.awakenwhitelabeling.dashboard.other


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.App
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.ErrorUtils
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.gson.JsonObject

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SideNavAdapter(private val callBack: CallBack<Int>) :
    RecyclerView.Adapter<SideNavAdapter.SideItemHolder>() {
    var count: String = "0"
    private val sideMenuList = mutableListOf<SideNavModel>()

    init {

        sideMenuList.add(
            SideNavModel(
                R.drawable.ic_home, "Home", count
            )
        )
        sideMenuList.add(
            SideNavModel(
                R.drawable.ic_spin_wheel_icon, "Spin Wheel", count
            )
        )

        sideMenuList.add(
            SideNavModel(
                R.drawable.ic_e_card, "E-Business Card", count
            )
        )


        sideMenuList.add(
            SideNavModel(
                R.drawable.ic_ribbon, "Membership Plan", count
            )
        )
        sideMenuList.add(
            SideNavModel(
                R.drawable.ic_privacy_policy, "Privacy Policy", count
            )
        )
        sideMenuList.add(
            SideNavModel(
                R.drawable.ic_change_password, "Change Password", count
            )
        )
        sideMenuList.add(
            SideNavModel(
                R.drawable.ic_logout, "Logout", count
            )
        )


        //getUnreadCount()F
    }


    class SideItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvItem: TextView = itemView.findViewById(R.id.tvSideItem)
        //val tvNotificationCount: TextView = itemView.findViewById(R.id.tvNotificationCount)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SideItemHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_side_navigation, parent, false)
        return SideItemHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return sideMenuList.size
    }

    override fun onBindViewHolder(holder: SideItemHolder, position: Int) {
        val item = sideMenuList[position]
        if (item.icon != 0) {
            holder.tvItem.setCompoundDrawablesWithIntrinsicBounds(item.icon, 0, 0, 0)

        }

        holder.tvItem.text = item.title
        if (holder.tvItem.text.equals(" Inbox") && item.countttt.toInt() >= 0) {
            // holder.tvNotificationCount.text = item.countttt
            // holder.tvNotificationCount.visibility = View.VISIBLE

        }
        holder.itemView.setOnClickListener { callBack.onSuccess(holder.adapterPosition) }
    }

    data class SideNavModel(val icon: Int, val title: String, var countttt: String)

    private fun getUnreadCount() {
        if (App.get().isConnected()) {
            RetrofitClient.getRequest().getUnreadCount()
                .enqueue(object : Callback<JsonObject?> {

                    override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                        Toaster.somethingWentWrong()
                    }

                    override fun onResponse(
                        call: Call<JsonObject?>,
                        response: Response<JsonObject?>
                    ) {

                        try {
                            val code = response.code()
                            if (code == 200) {
                                val json = Utils.convertToJSON(response?.body());
                                var countt: String = json?.optString("data")?.toString() ?: ""
                                // sideMenuList?.find { it.title == " Inbox" }?.count == countt
                                sideMenuList.set(
                                    0, SideNavModel(
                                        R.drawable.ic_home,
                                        "Home",
                                        countt
                                    )
                                )
                                notifyDataSetChanged()
                                Log.e("TAG", "count >>>  = " + countt)

                            } else {
                                ErrorUtils.parseError(response?.errorBody()?.string())
                            }
                        } catch (e: Exception) {
                            Toaster.somethingWentWrong()
                        }
                    }
                })
        }
    }

}