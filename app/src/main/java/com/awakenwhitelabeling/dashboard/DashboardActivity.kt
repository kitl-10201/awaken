package com.awakenwhitelabeling.dashboard


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.ui.AppBarConfiguration
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.*
import com.awakenwhitelabeling.dashboard.account.UpdateProfileFragment
import com.awakenwhitelabeling.dashboard.account.UserDetailsBean
import com.awakenwhitelabeling.dashboard.changePassword.ChangePassword
import com.awakenwhitelabeling.dashboard.dailyworkoutflow.DailyWorkFlowFragment
import com.awakenwhitelabeling.dashboard.notes.NotesFragment
import com.awakenwhitelabeling.dashboard.notification.NotificationActivity
import com.awakenwhitelabeling.dashboard.other.DashboardHelper
import com.awakenwhitelabeling.dashboard.policy.PrivacyPolicyActivity
import com.awakenwhitelabeling.dashboard.profile.NewUserProfileBean
import com.awakenwhitelabeling.dashboard.profile.topRanking.bean.NotificationCount
import com.awakenwhitelabeling.dashboard.spin_wheel.SpinWheelActivity
import com.awakenwhitelabeling.dashboard.spin_wheel.bean.SpinHideBean
import com.awakenwhitelabeling.dashboard.training.TrainingFragment
import com.awakenwhitelabeling.membership.PurchasedMembershipInfo
import com.awakenwhitelabeling.others.*
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.userAction.UserActivity
import com.bumptech.glide.RequestBuilder
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.Task
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class DashboardActivity : BaseActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {
    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, DashboardActivity::class.java))
        }

    }

    private val mInterval = 30000 // 3 seconds by default, can be changed later
    private var mHandler: Handler? = null
    var userPoints: String? = null
    var userDeductedPoints: String? = null
    var userMinSpinPoints: String? = null
    var userSpinSOund: String? = null
    var userSpinEnable: String? = null
    private val TAG: String = "Dashboard"
    var currentPosition: Int = 0
    private val HOME_FRAGMENT_ID = R.id.nav_home
    private var currentFragmentId = HOME_FRAGMENT_ID
    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var toolbar: MaterialToolbar
    lateinit var rlNotification: RelativeLayout
    lateinit var notification: ImageView
    private lateinit var tvNotification: TextView
    lateinit var tvVersioCode: TextView
    lateinit var fragment: Fragment
    lateinit var mDrawer: DrawerLayout
    lateinit var userDetailsBean: UserDetailsBean
    lateinit var nvDrawer: NavigationView
    lateinit var mFragManager: FragmentManager
    lateinit var ivProfile: ImageView
    private var previousFragment: Fragment? = null
    lateinit var dashboard_bottomnavigationview: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_drawer_dashboard)
        //Date 04/10/2022..............
        Log.d("DashBoard", "Awaken white labeling.......Has been start  after this Push #MrDev")
        loadSpinWheelData()
        findIDs()
        setUpUserDetails()
        getUnreadNotificationCount()
        getCurrentLocationLagLong()

        dashboard_bottomnavigationview = findViewById(R.id.dashboard_bottomnavigationview)
        rlNotification = findViewById(R.id.rlNotification)
        notification = findViewById(R.id.notification)
        tvNotification = findViewById(R.id.tvNotification)
        toolbar = findViewById(R.id.toolbar)
        DashboardHelper.setToolbar(toolbar)
        tvVersioCode = findViewById(R.id.tvVersioCode)
        val manager = this.packageManager
        val info = manager.getPackageInfo(this.packageName, PackageManager.GET_ACTIVITIES)
        tvVersioCode.text = "Version code: " + info.versionName
        setSupportActionBar(toolbar)
        nvDrawer = findViewById(R.id.nav_view);
        ivProfile = findViewById(R.id.ivProfile)
        dashboard_bottomnavigationview.setOnClickListener { }
        DashboardHelper.setupSideMenu(nvDrawer.findViewById(R.id.rvSideMenu),
            object : CallBack<Int>() {
                override fun onSuccess(t: Int) {
                    transactionFragment(t)
                }
            })

        toolbar.setNavigationIcon(R.drawable.ic_burger)
        toolbar.setNavigationOnClickListener {
            setUpUserDetails()
            DashboardHelper.setupSideMenu(nvDrawer.findViewById(R.id.rvSideMenu),
                object : CallBack<Int>() {
                    override fun onSuccess(t: Int) {
                        transactionFragment(t)
                    }
                })
            toggleDrawer()

        }
        mDrawer = findViewById(R.id.drawer_layout)
        notification.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
        }

        dashboard_bottomnavigationview.clearAnimation()
        dashboard_bottomnavigationview.setOnNavigationItemSelectedListener(this)
        mFragManager = this.supportFragmentManager
        fragment = TrainingFragment.newInstance("Trainings", 0)
        loadFragment(fragment)
        // startService(Intent(this, BackgroundService::class.java))


    }


    override fun onStart() {
        setUpUserDetails()
        super.onStart()
    }


    override fun onResume() {
        super.onResume()
        checkAppNewUpdates()
    }

    private fun checkAppNewUpdates() {
        val appUpdateManager = AppUpdateManagerFactory.create(this)
        val appUpdateInfoTask: Task<AppUpdateInfo> = appUpdateManager.appUpdateInfo
        // Checks that the platform will allow the specified type of update.
        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { result ->
            if (result.updateAvailability() === UpdateAvailability.UPDATE_AVAILABLE) {
//                requestUpdate(result);

                val customDialog = Dialog(this)
                customDialog.setContentView(R.layout.awaken_new_update_available)
                customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                customDialog.window?.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )

                val btnCancelAN = customDialog.findViewById(R.id.btnCancelAN) as Button
                val btnOkAN = customDialog.findViewById(R.id.btnOkAN) as Button
                customDialog.setCancelable(false)
                btnOkAN.setOnClickListener {
                    try {
                        startActivity(
                            Intent(
                                "android.intent.action.VIEW",
                                Uri.parse("market://details?id=$packageName")
                            )
                        )

                    } catch (e: ActivityNotFoundException) {
                        startActivity(
                            Intent(
                                "android.intent.action.VIEW",
                                Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                            )
                        )
                    }

                }
                btnCancelAN.setOnClickListener {
                    customDialog.dismiss()
                }
                customDialog.show()

            } else {

            }

        }


    }


    private fun findIDs() {}

    private fun toggleDrawer() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START)
        } else {
            mDrawer.openDrawer(GravityCompat.START)
        }
    }

    private fun transactionFragment(position: Int) {
        when (position) {
            0 -> {

                mFragManager = this.supportFragmentManager
                fragment = TrainingFragment.newInstance("Trainings", 0)
                loadFragment(fragment)
                dashboard_bottomnavigationview.menu.getItem(0).isChecked = true;

            }
            1 -> {
                val bundle = Bundle()
                bundle.putString("userDeductedPoints", userDeductedPoints)
                bundle.putString("userMinPoints", userMinSpinPoints)
                bundle.putString("userPoints", userPoints)
                bundle.putString("userSpinSound", userSpinSOund)
                // bundle.putString("userSpinEnable", userSpinEnable)

                val intent: Intent = Intent(DashboardActivity@ this, SpinWheelActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
            2 -> {

                showE_Business_Info()
                showLoader()

            }

            3 -> {
                PurchasedMembershipInfo(this).show()
            }
            4 -> {
                val intent = Intent(this@DashboardActivity, PrivacyPolicyActivity::class.java)
                intent.putExtra("Name", RetrofitClient.BASE_URL + "privacy_policy")
                intent.putExtra("Title", "Privacy Policy")
                startActivity(intent)
            }
            5 -> {
                startActivity(Intent(this, ChangePassword::class.java))
            }
            6 -> {
                logoutConfirm()

            }
        }
        toggleDrawer()
    }

    private fun logoutConfirm() {
        val conformDialog = Dialog(this)
        conformDialog.setContentView(R.layout.user_logout_conformation)
        conformDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        conformDialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val logoutCancel = conformDialog.findViewById(R.id.btn_cancel_timesheet) as Button
        val userLogout = conformDialog.findViewById(R.id.btn_print_timesheet) as Button
        conformDialog.setCancelable(false)

        userLogout.setOnClickListener {

            val map = hashMapOf<String, String?>()
            map["fcm_token"] = ""
            RetrofitClient.getRequest().saveFCMToken(map).enqueue(object : Callback<JsonObject?> {
                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                }

                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                    if (response.isSuccessful && response.code() == 200) {
                        SharedPref.get().clearAll()
                        startActivity(Intent(this@DashboardActivity, UserActivity::class.java))
                        finishAffinity()
                        Log.d("fcm res", "FCM updated token on Logout!" + response.body())
                    } else {
                        Toaster.shortToast("Something went wrong, Please clear the Cache of this Application")
                    }

                }
            })
        }
        logoutCancel.setOnClickListener {
            conformDialog.dismiss()
        }
        conformDialog.show()
    }

    @Synchronized
    private fun resetFragment(): Boolean {
        /*LOAD FIRST FRAGMENT*/
        //Toaster.shortToast("Hello")
        if (previousFragId != DashboardHelper.FragNumber.TRAINING) {

            val baseFragment = TrainingFragment.newInstance("Trainings", 0)
            FragMover.addFrag(
                mFragManager, Cons.fragContainerId,
                baseFragment
            )
            previousFragId = DashboardHelper.FragNumber.TRAINING
            return true
        }
        return false
    }

    @Synchronized
    private fun clearBackStack(): Boolean {
        if (mFragManager.backStackEntryCount > 1) {
            DashboardHelper.setTitle("Trainings")


            val entry: FragmentManager.BackStackEntry =
                mFragManager.getBackStackEntryAt(1)
            mFragManager.popBackStack(entry.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            mFragManager.executePendingTransactions()
            return true
        }
        return false
    }

    fun loadFragment(fragment: Fragment?): Boolean {

        //switching fragment
        try {
            if (fragment != null) {
                val ft = supportFragmentManager.beginTransaction()

                previousFragment?.let { ft.remove(it) }

                ft.replace(Cons.fragContainerId, fragment).commit()
                previousFragment = fragment

                return true
            }
        } catch (e: Exception) {
        }
        return false
    }

    var previousFragId = -2
    private fun loadFragment(fragment: BaseFragment?, previousId: Int): Boolean {

        if (previousId == 0) {
            return true
        }

        //switching fragment
        if (fragment != null && previousId != previousFragId) {
            previousFragId = previousId
            FragMover.addFrag(mFragManager, Cons.fragContainerId, fragment!!)

            return true
        }

        return false
    }

    private var backPressTime: Long = 0
    private var toast: Toast? = null
    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        Log.e(TAG, "onBackPressed $count")
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START)
            return
        }

        if (currentFragmentId != HOME_FRAGMENT_ID) {
            super.onBackPressed()
            return
        }

        if (System.currentTimeMillis() - backPressTime <= 300) {
            toast?.cancel()
            finish()
            finishAffinity()
        } else {
            toast?.cancel()
            toast = Toast.makeText(this, "Press twice to exit", Toast.LENGTH_SHORT)
            toast?.show()
            backPressTime = System.currentTimeMillis()
        }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: BaseFragment? = null
        var fragId = -2
        when (item.itemId) {
            R.id.training -> {
                fragId = DashboardHelper.FragNumber.TRAINING
                fragment = TrainingFragment.newInstance("Trainings", 0)
                Handler().postDelayed({
                    dashboard_bottomnavigationview.isClickable = false
                }, 2000)
            }
            R.id.workflow -> {
                fragId = DashboardHelper.FragNumber.DailyWorkFlow
                fragment = DailyWorkFlowFragment.newInstance("Daily Workflow", 1)
                Handler().postDelayed({
                    dashboard_bottomnavigationview.isClickable = false
                }, 2000)

            }
            R.id.notes -> {
                fragId = DashboardHelper.FragNumber.NOTES
                fragment = NotesFragment.newInstance("Notes", 2)
                // Toaster.shortToast("Notes Clicked!!!")
                /*fragId = DashboardHelper.FragNumber.NOTES
                fragment = NewNotesFragment.NewNoteInstance("Notes", 2)
                // Toaster.shortToast("Notes Clicked!!!")
*/

                Handler().postDelayed({
                    dashboard_bottomnavigationview.isClickable = false
                }, 2000)


            }
            R.id.account -> {
                fragId = DashboardHelper.FragNumber.ACCOUNT
                fragment = UpdateProfileFragment.UpdatenewProfileInstance("Profile", 3)
                Handler().postDelayed({
                    dashboard_bottomnavigationview.isClickable = false
                }, 2000)

            }
        }
        return loadFragment(fragment)
    }

    fun setUpUserDetails() {
        var userRequest = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userRequest.getUserDetails().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Log.e(TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {

                if (response.isSuccessful) {
                    Log.d("UserInfo", "UserInfo: " + response.body())

                    //var userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                    var userData = Gson().fromJson(response.body(), NewUserProfileBean::class.java)


                    if (userData.data.activeStatus == 0) {
                        val customDialog = Dialog(this@DashboardActivity)
                        customDialog.setContentView(R.layout.dialog_session_expired)
                        customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                        customDialog.window?.setLayout(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                        val btnDismiss = customDialog.findViewById(R.id.btnDismiss) as Button
                        customDialog.setCancelable(false)
                        btnDismiss.setOnClickListener {
                            SharedPref.get().clearAll()
                            this@DashboardActivity.startActivity(
                                Intent(
                                    this@DashboardActivity,
                                    UserActivity::class.java
                                )
                            )
                            this@DashboardActivity.finishAffinity()
                        }
                        customDialog.show()
                    }

                    Log.d("UserData", "" + response.body())
                    val tvName = nvDrawer.findViewById<TextView>(R.id.tvName)

                    var strName: String =
                        userData.data.firstName + " " + userData.data.lastName.trim()

                    if (strName?.length!! <= 15) {
                        tvName.text = userData.data.firstName + " " + userData.data.lastName
                        // Toaster.shortToast(strName.length)

                    } else {
                        //   Toaster.shortToast(strName.length)

                        tvName.text = userData.data.firstName + "\n" + userData.data.lastName
                    }

                    //  tvName.text = userData.data.first_name + " " + userData.data.last_name

                    val tvPoints = nvDrawer.findViewById<TextView>(R.id.tvPoints)
                    tvPoints.text = userData.data.coinsEarned.toString() + " Coins"
                    val ivProfile = nvDrawer.findViewById<ImageView>(R.id.ivProfile)
                    try {
                        if (!Utils.isEmptyString(userData.data.imagePath)) {
                            PicassoUtil.loadProfileImage(ivProfile, userData.data.imagePath)
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }
        })


    }


    private fun loadSpinWheelData() {
        var userData = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userData.hideWheel()?.enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                //hideProgressView()
                hideLoader()

                Log.e("onFailure", t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                if (response.isSuccessful) {
                    var userData = Gson().fromJson(response.body(), SpinHideBean::class.java)

                    userPoints = userData.data.userPoints.toString()
                    userMinSpinPoints = userData.data.minSpinPoints
                    userDeductedPoints = userData.data.deductedPoints
                    userSpinSOund = userData.data.spinWheelSound
                    userSpinEnable = userData.data.enable

                    //   Toaster.shortToast(userSpinSOund.toString())
                }
            }


        })


    }


    private fun getUnreadNotificationCount() {
        RetrofitClient.getRequest().getNotificationCount().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Log.d("onFailure", "onFailure")
            }

            @SuppressLint("LongLogTag")
            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>?) {
                try {
                    if (response != null) {
                        if (response.isSuccessful && response.code() == 200) {
                            Log.d("Resp", "UnRead :" + response.body())
                            val json = Utils.convertToJSON(response.body())
                            val resCount =
                                Gson().fromJson(response.body(), NotificationCount::class.java)
                            val count = json.optInt("count", resCount.data.count)
                            tvNotification.text = resCount.data.count.toString()

                            when {
                                count <= 0 -> {
                                    Utils.viewGone(tvNotification)
                                }
                                count < 100 -> {
                                    //Utils.viewVisible(tvNotification)
                                    tvNotification?.text = count.toString()
                                }
                                else -> {
                                    // Utils.viewVisible(tvNotification)
                                    tvNotification?.text = "99+"
                                }
                            }
                        } else {
                            Utils.viewGone(tvNotification)
                        }
                    } else {
                        Log.d(
                            "GET_UNREADNOTIFICATION_COUNT",
                            "Don't have any notification for now to count."
                        )
                    }
                } catch (e: Exception) {
                    Log.d("exception:", "onExceptionOccur" + e.localizedMessage)
                }
            }
        })
    }


    private fun getCurrentLocationLagLong() {
        val handler2 = Handler()
        handler2.postDelayed({
            mHandler = Handler()
            startRepeatingTask()
        }, 20000) //5 seconds
    }

    private fun startRepeatingTask() {
        mStatusChecker.run()
    }

    var mStatusChecker: Runnable = object : Runnable {
        override fun run() {
            try {
                getUnreadNotificationCount()
            } finally {
                mHandler!!.postDelayed(this, mInterval.toLong())
            }
        }
    }


    private fun showE_Business_Info() {


        val alertView: View = LayoutInflater.from(this).inflate(R.layout.generate_qr, null)
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setView(alertView)
        var close: ImageView = alertView.findViewById(R.id.ic_close_QR)
        var ivQr: ImageView = alertView.findViewById(R.id.ivQrCode)
        var uName: TextView = alertView.findViewById(R.id.tvUName)
        var tvQrCode: TextView = alertView.findViewById(R.id.tvQrCode)
        var uEmail: TextView = alertView.findViewById(R.id.tvUEmail)
        var uPhone: TextView = alertView.findViewById(R.id.tvUPhone)


        var userRequest = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userRequest.getUserDetails().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Log.e(TAG, "onFailure: " + t.localizedMessage)
                hideLoader()
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {

                hideLoader()
                if (response.isSuccessful) {
                    Log.d("UserInfo", "UserInfo: " + response.body())

                    //var userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                    var userData = Gson().fromJson(response.body(), NewUserProfileBean::class.java)


                    if (userData.data.activeStatus == 0) {
                        val customDialog = Dialog(this@DashboardActivity)
                        customDialog.setContentView(R.layout.dialog_session_expired)
                        customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                        customDialog.window?.setLayout(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                        val btnDismiss = customDialog.findViewById(R.id.btnDismiss) as Button
                        customDialog.setCancelable(false)
                        btnDismiss.setOnClickListener {
                            SharedPref.get().clearAll()
                            this@DashboardActivity.startActivity(
                                Intent(
                                    this@DashboardActivity,
                                    UserActivity::class.java
                                )
                            )
                            this@DashboardActivity.finishAffinity()
                        }
                        customDialog.show()
                    }

                    Log.d("UserData", "" + response.body())
                    val tvName = nvDrawer.findViewById<TextView>(R.id.tvName)

                    var strName: String =
                        userData.data.firstName + " " + userData.data.lastName.trim()

                    if (strName?.length!! <= 15) {
                        uName.text = userData.data.firstName + " " + userData.data.lastName
                        // Toaster.shortToast(strName.length)

                    } else {
                        //   Toaster.shortToast(strName.length)

                        uName.text = userData.data.firstName + "\n" + userData.data.lastName
                    }

                    //  tvName.text = userData.data.first_name + " " + userData.data.last_name

                    uEmail.text = userData.data.email
                    uPhone.text = userData.data.phone

                    try {
                        var requestBuilder: RequestBuilder<PictureDrawable?>
                        if (!userData.data.qrcodeImagePath.isNullOrEmpty()) {
                            //  PicassoUtil.loadQRImage(ivQr, userData.data.qrcodeImagePath)
                            //  SVG_Utils().fetchSVG(this@DashboardActivity,RetrofitClient.BASE_URL+userData.data.qrcodeImagePath,ivQr)

/*
                            Picasso.get()
                                .load("https://media.geeksforgeeks.org/wp-content/cdn-uploads/logo-new-2.svg")
                                .into(ivQr);
*/


                            val userAvatarUrl = "https://avatars.dicebear.com/v2/female/anna.svg"
                            /*SVG_Utils().fetchSvg(
                                this@DashboardActivity,
                                RetrofitClient.BASE_URL + userData.data.qrcodeImagePath,
                                ivQr
                            )*/

                            ivQr.visibility = View.VISIBLE
                            ivQr.loadSvg(userData.data.qrcodeImagePath)

                        } else {
                            //Toaster.somethingWentWrong()
                            tvQrCode.text = "Qr code not available for this user"
                            tvQrCode.visibility = View.VISIBLE

                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }
        })















        alertDialog.setCancelable(false)
        val dialog = alertDialog.create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        close.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun ImageView.loadSvg(qrcodeImagePath: String) {
        GlideToVectorYou
            .init()
            .with(this.context)
            .load(Uri.parse(RetrofitClient.BASE_URL + qrcodeImagePath), this)

    }

}
