package com.awakenwhitelabeling.dashboard.NewNotes

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.notes.NotesBean
import com.awakenwhitelabeling.dashboard.notes.NotesHelper
import com.awakenwhitelabeling.dashboard.notes.noteDetails.NotesDetailsActivity
import com.awakenwhitelabeling.others.Cons
import java.text.SimpleDateFormat


class NewNotesAdapter(
    var dfdData: List<NotesBean.Data.Data>?
) : RecyclerView.Adapter<NewNotesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.notes_card_list, parent, false)
        return ViewHolder(v)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var notes = dfdData?.get(position)!!
        if (notes.category=="blank")
        {
            holder.tvNoteTitle.text=""
        }else{
            holder.tvNoteTitle.text = NotesHelper.returnCamelCaseWord(notes.category)
        }
        holder.tvNotesDesc.text = Html.fromHtml(notes.notes_description)

        var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var setDateFormat = SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
        holder.tvNotesDate.text =
            setDateFormat.format(getDateFormat.parse(notes.created_at))+" EST"

        holder.main.setOnClickListener {
            // Toaster.shortToast("Well, trying to fixed upcoming issues!")
            NotesDetailsActivity.noteDetails(holder.itemView.context, notes.id)
        }

       /* if (notes.post_type.isNullOrEmpty())
        {
            holder.cal.visibility = View.GONE
            holder.facebook.visibility = View.GONE
        }
        else if (notes.post_type=="facebook" && notes.status == Cons.PUBLISH_NOW) {
            holder.facebook.visibility = View.VISIBLE
            holder.cal.visibility = View.VISIBLE

        }
        else if(notes.post_type=="facebook" && notes.status == Cons.SCHEDULE){
            holder.facebook.visibility = View.GONE
            holder.cal.visibility = View.VISIBLE
        }*/

        if (notes.post_type == "facebook" && notes.status == Cons.PUBLISH_NOW) {
            // holder.facebook.visibility = View.VISIBLE
            holder.ic_Icon.setBackgroundResource(R.drawable.ic_facebook)
            // holder.cal.visibility = View.GONE
        } else if (notes.post_type == "facebook" && notes.status == Cons.SCHEDULE) {
            // holder.cal.visibility = View.VISIBLE
            holder.ic_Icon.setBackgroundResource(R.drawable.ic_calendar)
        }


    }

    override fun getItemCount(): Int {
        return dfdData?.size ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ic_Icon = itemView.findViewById<ImageView>(R.id.ic_Icon)
        var main = itemView.findViewById<CardView>(R.id.cvNotesMain)
        var tvNoteTitle = itemView.findViewById<TextView>(R.id.tvNoteTitle)
        var tvNotesDesc = itemView.findViewById<TextView>(R.id.tvNotesDesc)
        var tvNotesDate = itemView.findViewById<TextView>(R.id.tvNotesDate)

    }

    var previousGetCount = -1

    @Synchronized
    private fun downloadMore(position: Int) {
        if (position > dfdData!!.size - 2 && previousGetCount != dfdData?.size) {

            previousGetCount = dfdData?.size!!
            // listener?.onLoadMore()

        }
    }

    interface OnLoadMoreListener {
        fun onLoadMore()
    }
}