package com.awakenwhitelabeling.dashboard.NewNotes

import android.annotation.SuppressLint
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.dashboard.notes.NotesBean
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.ErrorUtils
import com.awakenwhitelabeling.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object NotesHelper : BaseFragment() {
    // private val OFFSET_LIMIT = 10
    var IS_MORE_DATA_AVAILABLE = true
    private var offset = 0

    @SuppressLint("StaticFieldLeak")
    fun getNotesList(reload: Boolean, OFFSET: Int, callBack: CallBack<List<NotesBean.Data.Data>>) {
        offset = OFFSET
        if (reload) {
            offset = 0
        }
        if (!reload && !IS_MORE_DATA_AVAILABLE) {
            callBack.onSuccess(null)
            return
        }
        showLoader()
        RetrofitClient.getRequest().getNotesList(offset)
            .enqueue(@SuppressLint("StaticFieldLeak")
            object : Callback<NotesBean.Data> {
                override fun onFailure(call: Call<NotesBean.Data>, t: Throwable) {
                    hideLoader()
                    ErrorUtils.onFailure(t)
                    callBack.onSuccess(null)

                }

                override fun onResponse(
                    call: Call<NotesBean.Data>,
                    response: Response<NotesBean.Data>
                ) {
                    NotesHelper.hideLoader()
                    val code = response?.code() ?: 500
                    if (code == 200 && response?.isSuccessful) {
                        var items = response.body()
                        //  offset+=1
                        /*offset += OFFSET_LIMIT
                        IS_MORE_DATA_AVAILABLE = OFFSET_LIMIT == items?.data?.size ?: 0*/
                        callBack.onSuccess(items?.data)

                    } else if (code in 400..499) {
                        callBack.onSuccess(null)
                    } else {
                        callBack.onSuccess(null)
                    }




                }


            })
    }
}
