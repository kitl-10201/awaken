package com.awakenwhitelabeling.dashboard.NewNotes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.others.*
import android.os.Handler
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.awakenwhitelabeling.base.PaginationScrollListener
import com.awakenwhitelabeling.dashboard.notes.NotesBean
import com.awakenwhitelabeling.dashboard.other.DashboardHelper
import com.google.android.material.appbar.MaterialToolbar


class NewNotesFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener {

    private val groups = mutableListOf<NotesBean.Data.Data>()
    private var noGroupAvailable: TextView? = null
    private var mAdapter: NewNotesAdapter? = null
    private var isInProgress: Boolean = false;
    var OFFSET = 0
    var isLastPageL = false
    lateinit var ivBackDWF: ImageView
    lateinit var progressView: ProgressBar

    lateinit var main: LinearLayout


    lateinit var dfdRcv: RecyclerView

    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var layoutManager: GridLayoutManager


    companion object {

        private const val EXTRA_TITLE = "Daily Work Flow"
        private const val EXTRA_API_ENDPOINT = "frag.endpoint"

        // U_E -> URL_ENDPOINT
        private const val U_E_TRAINING = "training_categories"
        private const val U_E_WORKFLOW = "get_tasks"
        private const val U_E_NOTES = "get_user_notes"
        private const val U_E_ACCOUNT = "get_user_data"

        fun NewNoteInstance(title: String, position: Int): NewNotesFragment {
            val args = Bundle()
            args.putString(EXTRA_TITLE, title)

            val endPoint = when (position) {
                0 -> U_E_TRAINING
                1 -> U_E_WORKFLOW
                2 -> U_E_NOTES
                else -> U_E_ACCOUNT
            }

            args.putString(EXTRA_TITLE, title)
            args.putString(EXTRA_API_ENDPOINT, endPoint.toString())

            val fragment = NewNotesFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_dailyworkflow, container, false)
        progressView = view.findViewById(R.id.progressBar)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setOnRefreshListener(this)
        return view
    }

    override fun onStart() {
        super.onStart()
        isLastPageL = false
        loadFirstPage()

        dfdRcv.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun loadMoreItems() {
                isInProgress = true

                OFFSET += 1;

                getGroups(false, OFFSET)

            }

            override fun isLastPage(): Boolean {
                return isLastPageL
            }

            override fun isLoading(): Boolean {
                return isInProgress
            }
        });
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        dfdRcv = view.findViewById(R.id.rvTasks)

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))

        // swipeRefreshLayout?.isEnabled = false
        layoutManager = GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false);
        dfdRcv.layoutManager = layoutManager
        mAdapter = NewNotesAdapter(
            groups
        )
        dfdRcv.adapter = mAdapter

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setTitleTextColor(ResourceUtils.getColor(R.color.black))

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setNavigationIconTint(ResourceUtils.getColor(R.color.black))

        parseValue()
    }

    private fun parseValue() {

        val title = arguments?.getString(NewNotesFragment.EXTRA_TITLE) ?: "Daily Work Flow"
        //  apiEndPoint = arguments?.getString(NewNotesFragment.EXTRA_API_ENDPOINT) ?: NewNotesFragment.U_E_TRAINING

        DashboardHelper.setTitle(title)


    }


    override fun onRefresh() {
        /*getGroups(true,OFFSET)
       */
        //  loadFirstPage()


        layoutManager = GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false);
        dfdRcv.layoutManager = layoutManager
        mAdapter = NewNotesAdapter(
            groups
        )
        dfdRcv.adapter = mAdapter


        if (swipeRefreshLayout.isRefreshing) {
            //Utils.viewGone(dfdRcv)
            swipeRefreshLayout.isRefreshing = false
            // Utils.viewGone(dfdRcv)
        }


    }


    private fun loadFirstPage() {
        showProgress()

        getGroups(true, OFFSET)

    }

    private fun getGroups(reload: Boolean, offsetValue: Int) {
        Handler().postDelayed({
            NotesHelper.getNotesList(
                reload,
                offsetValue,
                object : CallBack<List<NotesBean.Data.Data>>() {


                    override fun onError(error: String?) {
                        super.onError(error)
                        isInProgress = false
                        isLastPageL = true
                    }

                    override fun onSuccess(t: List<NotesBean.Data.Data>?) {
                        if (swipeRefreshLayout.isRefreshing) {
//                        Utils.viewGone(dfdRcv)

                            swipeRefreshLayout.isRefreshing = false
                        }

                        isInProgress = false
                        //  Utils.viewGone(progressView)
                        //  swlLayout.visible(false)
                        if (t.isNullOrEmpty()) {
                            Utils.viewVisible(noGroupAvailable)
                            isLastPageL = true
                            return
                        }
                        if (reload) {
                            groups.clear()

                        }
                        groups.addAll(t);
                        mAdapter?.notifyDataSetChanged()
                        hideProgress()
                    }
                })

        }, 1500)

    }

    private fun hideProgress() {
        Utils.viewGone(progressView)
        Utils.viewVisible(dfdRcv)
    }

    private fun showProgress() {
        Utils.viewVisible(progressView)
        Utils.viewGone(dfdRcv)
    }

    override fun onStop() {
        super.onStop()
        OFFSET = 0
       // TaskHelper.IS_MORE_DATA_AVAILABLE = true
    }


}