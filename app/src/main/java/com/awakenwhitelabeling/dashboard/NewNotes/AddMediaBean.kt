package com.awakenwhitelabeling.dashboard.NewNotes


import com.google.gson.annotations.SerializedName

data class AddMediaBean(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("google_drive_link")
        val googleDriveLink: String
    )
}