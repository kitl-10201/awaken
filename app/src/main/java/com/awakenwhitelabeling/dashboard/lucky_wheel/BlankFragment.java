package com.awakenwhitelabeling.dashboard.lucky_wheel;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.adefruandta.spinningwheel.SpinningWheelView;
import com.awakenwhitelabeling.R;
import com.awakenwhitelabeling.base.BaseFragment;
import com.bluehomestudio.luckywheel.LuckyWheel;
import com.bluehomestudio.luckywheel.WheelItem;

import java.util.ArrayList;
import java.util.List;

public class BlankFragment extends BaseFragment implements SpinningWheelView.OnRotationListener<String>  {

    private LuckyWheel lw;
    List<WheelItem> wheelItems ;


    private SpinningWheelView wheelView;
    private Button rotate;






    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blank, container, false);

        getWheelItems();

        wheelView =view.findViewById(R.id.wheel);

        rotate =view.findViewById(R.id.rotate);

        wheelView.setItems(R.array.dummy);

        wheelView.setOnRotationListener(this);

        rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // max angle 50
                // duration 10 second
                // every 50 ms rander rotation
                wheelView.rotate(50, 5000, 30
                );
            }
        });
        return view;

    }
    private void getWheelItems() {
        wheelItems = new ArrayList<>();
        wheelItems.add(new WheelItem(Color.parseColor("#fc6c6c"), BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_account) , "100 $"));
        wheelItems.add(new WheelItem(Color.parseColor("#00E6FF"), BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_account) , "0 $"));
        wheelItems.add(new WheelItem(Color.parseColor("#F00E6F"), BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_account), "30 $"));
        wheelItems.add(new WheelItem(Color.parseColor("#00E6FF"), BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_account), "6000 $"));
        wheelItems.add(new WheelItem(Color.parseColor("#fc6c6c"), BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_account), "9 $"));
        wheelItems.add(new WheelItem(Color.parseColor("#00E6FF"), BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_account), "20 $"));
    }

    @Override
    public void onRotation() {
    }

    @Override
    public void onStopRotation(String item) {
        Toast.makeText(requireContext(), item, Toast.LENGTH_LONG).show();
    }
}