package com.awakenwhitelabeling.dashboard.spin_wheel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.awakenwhitelabeling.R

class SpinWheelRewardsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spin_wheel_rewards)

        val fragment = SpinRewardsFragment()
        supportFragmentManager.beginTransaction()
            .add(R.id.flSpin, fragment)
            .commit()
    }
}