package com.awakenwhitelabeling.dashboard.spin_wheel.adapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.spin_wheel.NewSpinBean
import com.awakenwhitelabeling.dashboard.spin_wheel.SpinWheelRewardsActivity
import com.awakenwhitelabeling.others.PicassoUtil
import com.devs.readmoreoption.ReadMoreOption
import java.text.SimpleDateFormat

class SpinWheelAdapter(

    var rwrdData: List<NewSpinBean.Data>?,
    val activity: SpinWheelRewardsActivity?
) : RecyclerView.Adapter<SpinWheelAdapter.ViewHolder>() {
    var context: Context? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.reward_list, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var dfd = rwrdData?.get(position)!!
        downloadMore(position)
        holder.titleRew.text = dfd.rewardName
        holder.tvDesc.text = dfd.rewardDescription
        var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var setDateFormat = SimpleDateFormat("MMM dd, yyyy");
        var setTimeFormat = SimpleDateFormat("hh:mm a");

        holder.tvDate.text = setDateFormat.format(getDateFormat.parse(dfd.redeemOn))
        PicassoUtil.loadImage(holder.rewardsImg, dfd.image)
        holder.rewards.setOnClickListener {
/*            alertDialog.setTitle(dfd.rewardName)
            alertDialog.setMessage(dfd.rewardDescription)*/


            val alertView: View = LayoutInflater.from(activity)
                .inflate(R.layout.redeem_dialog_background_layout, null)
            val alertDialog = AlertDialog.Builder(activity)
            alertDialog.setView(alertView)
            //   alertDialog.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            var close: ImageView = alertView.findViewById(R.id.ivCloseRewards)

            var title: TextView = alertView.findViewById(R.id.tvRewardsTitle)

            var desc: TextView = alertView.findViewById(R.id.tvRedeemDesc)
            alertDialog.setCancelable(false)
            val dialog = alertDialog.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            close.setOnClickListener {
                dialog.dismiss()
            }

            var readMoreOption: ReadMoreOption = ReadMoreOption.Builder(activity)
                .textLength(3, ReadMoreOption.TYPE_LINE) //OR
                //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
                .moreLabel("Read More")
                .lessLabel("Read Less")
                .moreLabelColor(Color.BLUE)
                .lessLabelColor(Color.BLUE)
                .labelUnderLine(true)
                .expandAnimation(true)
                .build();
            readMoreOption.addReadMoreTo(desc, dfd.rewardDescription);

            title.setText(dfd.rewardName)
            //  desc.setText(dfd.rewardDescription)

            /* final AlertDialog dialog = alert.create();
             //this line removed app bar from dialog and make it transperent and you see the image is like floating outside dialog box.
             dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
             //finally show the dialog box in android all
             dialog.show();
             cancel.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     dialog.dismiss();
                 }


             */


            /*

          ImageView cancel;
                 //will create a view of our custom dialog layout
                 View alertCustomdialog = LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_dialog,null);
                //initialize alert builder.
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                //set our custom alert dialog to tha alertdialog builder
                alert.setView(alertCustomdialog);
                cancel = (ImageView)alertCustomdialog.findViewById(R.id.cancel_button);
                final AlertDialog dialog = alert.create();
                //this line removed app bar from dialog and make it transperent and you see the image is like floating outside dialog box.
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                //finally show the dialog box in android all
                dialog.show();
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }*/


        }
    }

    override fun getItemCount(): Int {
        return rwrdData?.size ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rewards = itemView.findViewById<ConstraintLayout>(R.id.rewardsMain)
        var rewardsImg = itemView.findViewById<ImageView>(R.id.ivRwrd)
        var titleRew = itemView.findViewById<TextView>(R.id.tvTitleRwrd)
        var tvDesc = itemView.findViewById<TextView>(R.id.tvDescRwrd)
        var tvDate = itemView.findViewById<TextView>(R.id.tvDateRwrd)
        var tvPoints = itemView.findViewById<TextView>(R.id.tvRwrdPoint)
    }
    var previousGetCount = -1

    @Synchronized
    private fun downloadMore(position: Int) {
        if (position > rwrdData!!.size - 2 && previousGetCount != rwrdData?.size) {

           previousGetCount = rwrdData?.size!!

        }
    }

    interface OnLoadMoreListener {
        fun onLoadMore()
    }

}