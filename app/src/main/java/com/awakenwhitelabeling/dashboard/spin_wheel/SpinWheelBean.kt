package com.awakenwhitelabeling.dashboard.spin_wheel
import com.google.gson.annotations.SerializedName
data class SpinWheelBean(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: List<Data>
) {
    data class Data(
        @SerializedName("id")
        val id: Int,
        @SerializedName("user_id")
        val userId: Int,
        @SerializedName("active_status")
        val activeStatus: Int,
        @SerializedName("image")
        val image: String,
        @SerializedName("reward_name")
        val rewardName: String,
        @SerializedName("reward_description")
        val rewardDescription: String,
        @SerializedName("reward_coins")
        val rewardCoins: Int,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("updated_at")
        val updatedAt: String
    )
}