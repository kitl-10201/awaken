package com.awakenwhitelabeling.dashboard.spin_wheel;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adefruandta.spinningwheel.SpinningWheelView;
import com.awakenwhitelabeling.R;
import com.awakenwhitelabeling.base.BaseActivity;
import com.awakenwhitelabeling.dashboard.DashboardActivity;
import com.awakenwhitelabeling.dashboard.spin_wheel.bean.SpinHideBean;
import com.awakenwhitelabeling.retrofit.RetrofitClient;
import com.google.gson.JsonObject;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpinWheelActivity extends BaseActivity implements SpinningWheelView.OnRotationListener<String> {
    String TAG = "SpinWheelActivity";
    TextView tvViewRewards;
    ProgressBar progre_spin;
    private SpinningWheelView wheelView;
    Button rotate;
    ImageView back;
    String str;
    String[] resWheel = new String[10];
    List<String> list = new ArrayList<String>();
    List<SpinWheelBean.Data> wheelResponseList;
    MediaPlayer mp;
    String spinAudio = "";
    int lenAudio;
    TextView tvSpinCoin, tvDeductionPoints;
    ConstraintLayout MainspinView;
    RelativeLayout rlSpinView;
    TextView tvnoInternetText;
    boolean isRotateClickable;
    String minPointsNeeded = "0";
    boolean isPaused;
    Dialog dialog;
    PrepareRewardsAudioInBackground asyncTask = new PrepareRewardsAudioInBackground();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spin_wheel);
        //Setting UI refrence in "setWheelRefrence.......
        setWheelRefrence();
    }

    private void setWheelRefrence() {
        progre_spin = findViewById(R.id.progre_spin);
        wheelResponseList = new ArrayList<>();
        wheelView = findViewById(R.id.wheel);
        back = findViewById(R.id.ivBackSpin);
        tvnoInternetText = findViewById(R.id.tvnoInternetText);
        tvSpinCoin = findViewById(R.id.tvSpinCoin);
        tvDeductionPoints = findViewById(R.id.tvDeductionPoints);
        tvViewRewards = findViewById(R.id.tvViewRewards);
        rotate = findViewById(R.id.rotate);
        rlSpinView = findViewById(R.id.rlSpinView);
        MainspinView = findViewById(R.id.MainspinView);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            spinAudio = bundle.getString("userSpinSound");
            Log.d("audioooooooLink", "link" + RetrofitClient.BASE_URL + spinAudio);
            tvSpinCoin.setText(bundle.getString("userPoints"));
            tvDeductionPoints.setText("Note: " + bundle.getString("userMinPoints") + " Coins will be deducted from your account for one spin.");
            minPointsNeeded = bundle.getString("userMinPoints");
            //  Toast.makeText(this,spinAudio, Toast.LENGTH_SHORT).show();
            /*String dataStr = bundle.getString("userSpinEnable");
            if (dataStr.equals("false")) {
                wheelView.setAlpha(.5f);
                isRotateClickable = false;
            } else {
                isRotateClickable = true;
            }*/
        }

        //...To prepare music in background Used...."asyncTask
        asyncTask.execute();
        tvViewRewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpinWheelActivity.this, SpinWheelRewardsActivity.class);
                startActivity(intent);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpinWheelActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });

        wheelView.setFilterTouchesWhenObscured(false);
        wheelView.setEnabled(false);
        wheelView.setOnRotationListener(this);
        checkInternet();
        getWheelItem();
        getHideWheel();
    }

    private class PrepareRewardsAudioInBackground extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            if (spinAudio != "" && spinAudio != null) {
                mp = new MediaPlayer();
                try {
                    mp.setDataSource(RetrofitClient.BASE_URL + spinAudio);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d("audioooooooLink", "link" + RetrofitClient.BASE_URL + spinAudio);
                try {
                    mp.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                lenAudio = mp.getDuration();
            } else {
                Toast.makeText(SpinWheelActivity.this, "Media not available", Toast.LENGTH_SHORT).show();
            }
            return spinAudio;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(SpinWheelActivity.this);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.spin_rewards_loader_dialog);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.DIM_AMOUNT_CHANGED);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.show();
            dialog.setCancelable(false);


        }

        @Override
        protected void onPostExecute(String s) {
            dialog.hide();
            rotate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isPaused = false;
                    if (isRotateClickable) {

                        mp.start();
                        wheelView.rotate(50, lenAudio - 1600, 20);

                    } else {
                        Toast.makeText(SpinWheelActivity.this, "Minimum " + minPointsNeeded + " coins required to Spin the Wheel.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public void onRotation() {
        checkInternet();
        rotate.setClickable(false);
        tvViewRewards.setClickable(false);
        back.setClickable(false);
    }

    @Override
    public void onStopRotation(String item) {
        checkInternet();
        if (!isPaused) {
            if (mp != null && mp.isPlaying()) {
                mp.pause();
                mp.seekTo(0);
            }
            str = item;
            int idToSend = -1;//To maintain the current index value of arrayList
            for (int i = 0; i < wheelResponseList.size(); i++) {
                if (wheelResponseList.get(i).getRewardName().equals(str)) {
                    idToSend = wheelResponseList.get(i).getId();
                    Log.d("dhfshjhh", "jghfsg" + idToSend);
                    redeemSpinData(idToSend);
                    openDialog();
                }
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rotate.setClickable(true);
                tvViewRewards.setClickable(true);
                back.setClickable(true);
            }
        }, 2000);
    }

    private void stopAudio() {
        try {
            if (mp.isPlaying()) {
                mp.pause();
                mp.seekTo(0);
                stopWheel();
            }
        } catch (Exception e) {

        }
    }

    private void stopWheel() {
        try {
            wheelView.rotate(50, 0, 20);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onPause() {
        checkInternet();
        super.onPause();
        isPaused = true;
        stopAudio();

    }

    @Override
    protected void onResume() {
        checkInternet();
        //startAudio();
        super.onResume();

    }

    private void getWheelItem() {
        // showSpinLoader();
        //  showLoader();
        list.clear();
        wheelResponseList.clear();
        Call<SpinWheelBean> call = RetrofitClient.getRequest().getWheelRewards();
        call.enqueue(new Callback<SpinWheelBean>() {
            @Override
            public void onResponse(Call<SpinWheelBean> call, Response<SpinWheelBean> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.d(TAG, "onResponsedfldfsdfsd: " + response.body());
                    try {
                        String str = null;
                        int i;
                        wheelResponseList.addAll(response.body().getData());
                        resWheel = new String[wheelResponseList.size()];
                        for (i = 0; i < wheelResponseList.size(); i++) {
                            resWheel[i] = wheelResponseList.get(i).getRewardName();
                            list.addAll(Collections.singleton(resWheel[i]));
                        }
                        if (list.isEmpty() || list.size() == 1) {
                            wheelView.rotate(50, 0, 20);
                            wheelView.setFilterTouchesWhenObscured(false);
                            wheelView.setEnabled(false);
                            rotate.setEnabled(false);
                            rotate.setClickable(false);
                           // rotate.setVisibility(View.GONE);
                            wheelView.setAlpha(.5f);
                            isRotateClickable = false;
                            //Toast.makeText(SpinWheelActivity.this, "You can not redeem more than 10 rewards in a day.", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "onResponse: " + " dfsdfhsjfhhkl");
                        } else {

                            wheelView.setItems(list);

                        }

                    } catch (Exception e) {

                        Log.d(TAG, "onResponse: " + e.getLocalizedMessage());
                    }

                } else {
                    JSONObject jOBJObject = null;
                    try {
                        jOBJObject = new JSONObject(response.errorBody().string());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        Toast.makeText(SpinWheelActivity.this, jOBJObject.getString("message").toString(), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SpinWheelBean> call, Throwable t) {
                //  hideSpinLoader();
                //    hideLoader();
                rlSpinView.setVisibility(View.GONE);
                tvnoInternetText.setVisibility(View.VISIBLE);
                //Toast.makeText(SpinWheelActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getHideWheel() {
        checkInternet();
        Call<SpinHideBean> call = RetrofitClient.getRequest().hideWheelInJava();
        call.enqueue(new Callback<SpinHideBean>() {
            @Override
            public void onResponse(Call<SpinHideBean> call, Response<SpinHideBean> response) {
                if (response.code() == 200) {
                    // hideLoader();
                    String dataStr = response.body().getData().getEnable();
                    tvSpinCoin.setText(String.valueOf(response.body().getData().getUserPoints()));
                    if (dataStr.equals("false")) {
                        wheelView.setAlpha(.5f);
                        isRotateClickable = false;
                    } else {
                        isRotateClickable = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<SpinHideBean> call, Throwable t) {
                // hideLoader();
                rlSpinView.setVisibility(View.GONE);
                tvnoInternetText.setVisibility(View.VISIBLE);
            }
        });
    }

    private void redeemSpinData(int idToSend) {
        checkInternet();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("reward_id", String.valueOf(idToSend));
        Log.d("reward_id", "hffshfshjsjlgl" + String.valueOf(idToSend));
        Call<JsonObject> call = RetrofitClient.getRequest().spinRedeemRewards(hashMap);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    Log.d("", "hffshfshjsjlgl" + response.body());
                    getHideWheel();
                    //openDialog();
                } else {
                    Log.d("", "hffshfshjsjlgl_wrong" + response.body());
                    Toast.makeText(SpinWheelActivity.this, "Insufficient coins", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("", "hffshfshjsjlglError" + t.getLocalizedMessage());
                rlSpinView.setVisibility(View.GONE);
                tvnoInternetText.setVisibility(View.VISIBLE);


            }
        });


    }

    private void openDialog() {
        checkInternet();
        Dialog dialog = new Dialog(this);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.spin_rewards_dialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView coins;
        Button btnDone;
        coins = dialog.findViewById(R.id.tvSpiningCoins);
        btnDone = dialog.findViewById(R.id.btnDoneSW);
        coins.setText(str);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getWheelItem();

                dialog.dismiss();
            }
        });
        dialog.show();
    }

/*
    private void getUserCoin() {

        Call<SpinRewardsBean> call = RetrofitClient.getRequest().getUserRewardsCoin();
        call.enqueue(new Callback<SpinRewardsBean>() {

            @Override
            public void onResponse(Call<SpinRewardsBean> call, Response<SpinRewardsBean> response) {
                if (response.code() == 200) {

                    int coinmm = response.body().getData().component18();
                    Toast.makeText(SpinWheelActivity.this, coinmm, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SpinRewardsBean> call, Throwable t) {

            }
        });

    }*/




   /* private void startAudio() {
        try {
            if (mp != null && mp.isLooping()) {
                mp.prepare();
                mp.start();
            } else {
                mp.stop();
                mp.release();
            }

        } catch (Exception e) {

        }
    }*/

    //Custom dialog to check internet connectivity
    private void checkInternet() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
        } else {
            //no connection

            Dialog internetDialog = new Dialog(this);

            internetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            internetDialog.setContentView(R.layout.dialog_no_internet_connection);
            internetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


            Button btnDone;
            btnDone = internetDialog.findViewById(R.id.retry);


            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    internetDialog.dismiss();
                }
            });
            internetDialog.show();
            if (internetDialog.isShowing()) {
                rlSpinView.setVisibility(View.GONE);
            } else {
                rlSpinView.setVisibility(View.VISIBLE);
            }

        }
    }

}