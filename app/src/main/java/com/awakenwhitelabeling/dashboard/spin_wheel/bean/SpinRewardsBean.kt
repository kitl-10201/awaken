package com.awakenwhitelabeling.dashboard.spin_wheel.bean


import com.google.gson.annotations.SerializedName

data class SpinRewardsBean(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: Data
) {
    data class Data(
        @SerializedName("id")
        val id: Int,
        @SerializedName("active_status")
        val activeStatus: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("first_name")
        val firstName: String,
        @SerializedName("last_name")
        val lastName: String,
        @SerializedName("register_type")
        val registerType: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("previous_email")
        val previousEmail: Any,
        @SerializedName("email_verified_at")
        val emailVerifiedAt: Any,
        @SerializedName("phone")
        val phone: String,
        @SerializedName("dob")
        val dob: String,
        @SerializedName("about_yourself")
        val aboutYourself: Any,
        @SerializedName("fcm_token")
        val fcmToken: Any,
        @SerializedName("socialLogID")
        val socialLogID: Any,
        @SerializedName("device_type")
        val deviceType: String,
        @SerializedName("ibo_number")
        val iboNumber: String,
        @SerializedName("image_path")
        val imagePath: Any,
        @SerializedName("is_deleted")
        val isDeleted: Int,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("updated_at")
        val updatedAt: String,
        @SerializedName("last_updated_by")
        val lastUpdatedBy: Int,
        @SerializedName("address_line_1")
        val addressLine1: String,
        @SerializedName("address_line_2")
        val addressLine2: String,
        @SerializedName("state")
        val state: String,
        @SerializedName("zip")
        val zip: String,
        @SerializedName("coins_earned")
        val coinsEarned: Int
    )
}