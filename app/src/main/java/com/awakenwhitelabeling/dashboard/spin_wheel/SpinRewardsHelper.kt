package com.awakenwhitelabeling.dashboard.spin_wheel

import android.annotation.SuppressLint
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.ErrorUtils
import com.awakenwhitelabeling.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("StaticFieldLeak")
object SpinRewardsHelper : BaseFragment() {
    // private val OFFSET_LIMIT = 10
    var IS_MORE_DATA_AVAILABLE = true
    private var offset=0
    fun getGroupsList(reload: Boolean, OFFSET:Int, callBack: CallBack<List<NewSpinBean.Data>>) {
        offset=OFFSET
        if (reload){
            offset=0
        }
        if (!reload && !IS_MORE_DATA_AVAILABLE) {
            callBack.onSuccess(null)
            return
        }
        showLoader()
        RetrofitClient.getRequest().getSpinRewards(offset)
            .enqueue(object : Callback<NewSpinBean> {
                override fun onFailure(call: Call<NewSpinBean>, t: Throwable) {
                    hideLoader()
                    ErrorUtils.onFailure(t)
                    callBack.onSuccess(null)
                }

                override fun onResponse(
                    call: Call<NewSpinBean>,
                    response: Response<NewSpinBean>
                ) {
                    hideLoader()
                    val code = response?.code() ?: 500
                    if (code == 200 && response?.isSuccessful) {
                        var items = response.body()

                        callBack.onSuccess(items?.data)

                    } else if (code in 400..499) {
                        callBack.onSuccess(null)
                    } else {
                        callBack.onSuccess(null)
                    }                }
            })
    }
}