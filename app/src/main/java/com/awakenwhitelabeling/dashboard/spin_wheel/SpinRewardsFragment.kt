package com.awakenwhitelabeling.dashboard.spin_wheel

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.base.PaginationScrollListener
import com.awakenwhitelabeling.dashboard.profile.UserBean
import com.awakenwhitelabeling.dashboard.spin_wheel.adapter.SpinWheelAdapter
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.userAction.login.LoginFragment
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SpinRewardsFragment : BaseFragment() {
    private val groups = mutableListOf<NewSpinBean.Data>()
    private lateinit var noGroupAvailable: TextView
    private var isInProgress: Boolean = false;

    lateinit var ivBackReward: ImageView

    lateinit var rvReward: RecyclerView
    private var dataRew = mutableListOf<NewSpinBean.Data>()
    lateinit var tvRpoint: TextView
    private var mAdapter: SpinWheelAdapter? = null

    var OFFSET = 0
    lateinit var layoutManager: GridLayoutManager
    var isLastPageL = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_rewards, container, false)


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivBackReward = view.findViewById(R.id.ivBackReward)
        rvReward = view.findViewById(R.id.rvReward)
        tvRpoint = view.findViewById(R.id.tvRpoint)
        noGroupAvailable = view.findViewById(R.id.tvNoData)

        ivBackReward.setOnClickListener {
            activity?.onBackPressed()
        }



        layoutManager = GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false);
        rvReward.layoutManager = layoutManager
        mAdapter = SpinWheelAdapter(
            groups, activity as SpinWheelRewardsActivity?
        )
        rvReward.adapter = mAdapter

        loadFirstPage()
        getRewardPoints()


        rvReward.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun loadMoreItems() {
                isInProgress = true

                OFFSET += 1;
                getGroups(false, OFFSET)

            }

            override fun isLastPage(): Boolean {
                return isLastPageL
            }

            override fun isLoading(): Boolean {
                return isInProgress
            }
        });


    }

    private fun getRewardPoints() {
        showLoader()
        var userRequest = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userRequest.getUserDetails().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()
                Log.e(LoginFragment.TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                if (response.isSuccessful) {
                    //var userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                    var userData = Gson().fromJson(response.body(), UserBean::class.java)
                    Log.d("UserData", "" + response.body())
                    tvRpoint.text = userData.data.coins_earned.toString()
                }
            }
        })

    }
    private fun loadFirstPage() {
        getGroups(true, OFFSET)

    }

    private fun getGroups(reload: Boolean, offsetValue: Int) {
        SpinRewardsHelper.getGroupsList(
            reload,
            offsetValue,
            object : CallBack<List<NewSpinBean.Data>>() {
                override fun onSuccess(beans: List<NewSpinBean.Data>?) {
                  //  Toaster.shortToast("LOL: " + beans?.size)
                    isInProgress = false

                    if (beans.isNullOrEmpty()) {
                        isLastPageL = true
                        noGroupAvailable?.visibility = View.VISIBLE
                        return
                    } else {
                        if (reload) {
                            groups.clear()
                        }
                        noGroupAvailable?.visibility = View.GONE
                        groups.addAll(beans);
                        mAdapter?.notifyDataSetChanged()

                    }
                }

                override fun onError(error: String?) {
                    super.onError(error)
                    isInProgress = false
                    isLastPageL = true
                }
            }
        )
    }

    override fun onStop() {
        super.onStop()
        OFFSET = 0
        // TaskHelper.IS_MORE_DATA_AVAILABLE = true
    }

}

