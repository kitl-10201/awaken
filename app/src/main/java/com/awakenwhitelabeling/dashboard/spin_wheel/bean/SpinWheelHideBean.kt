package com.awakenwhitelabeling.dashboard.spin_wheel.bean


import com.google.gson.annotations.SerializedName

data class SpinWheelHideBean(
    @SerializedName("status")
    val status: String,
    @SerializedName("data")
    val `data`: Data
) {
    data class Data(
        @SerializedName("enable")
        val enable: String,
        @SerializedName("user_points")
        val userPoints: Int,
        @SerializedName("min_spin_points")
        val minSpinPoints: String,
        @SerializedName("deducted_points")
        val deductedPoints: String,
        @SerializedName("spin_wheel_sound")
        val spinWheelSound: String
    )
}