package com.awakenwhitelabeling.dashboard.spin_wheel.bean


import com.google.gson.annotations.SerializedName

data class SpinRedeemBean(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: Data
) {
    data class Data(
        @SerializedName("user_id")
        val userId: Int,
        @SerializedName("reward_id")
        val rewardId: String,
        @SerializedName("redeemed_on")
        val redeemedOn: String,
        @SerializedName("updated_at")
        val updatedAt: String,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("id")
        val id: Int
    )
}