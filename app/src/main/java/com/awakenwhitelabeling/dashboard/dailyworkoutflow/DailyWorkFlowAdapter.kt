package com.awakenwhitelabeling.dashboard.dailyworkoutflow

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.ErrorEventHelper
import com.awakenwhitelabeling.dashboard.dailyworkoutflow.DailyWorkFlowAdapter.ViewHolder
import com.awakenwhitelabeling.dashboard.dailyworkoutflow.bean.NewDailyWorkFlowBean
import com.awakenwhitelabeling.dashboard.dailyworkoutflow.detail.DailyWorkFlowDetailsActivity
import com.awakenwhitelabeling.others.Cons
import com.awakenwhitelabeling.others.PicassoUtil
import java.util.ArrayList

class DailyWorkFlowAdapter(
    var dfdData: MutableList<NewDailyWorkFlowBean.Data.Task>?
) : RecyclerView.Adapter<ViewHolder>() {

    var isClickable = false
    lateinit var context: Context
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        context=parent.context
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.daily_work_flow_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var currentStatus: ArrayList<String> = ArrayList()


        var dfd = dfdData?.get(position)!!

        for (i in dfdData!!.indices) {
            currentStatus.add(dfdData!![i].currentStatus)
        }
        val hashSet = HashSet(currentStatus)
        holder.tvMessangerType.text = dfd.taskCategory
        holder.tvMessage.text = dfd.taskName
        holder.tvFacebookQuotes.text = dfd.taskDescription
        holder.tvLeftPoint.text = dfd.rewardCoins.toString() + " Coins"
        holder.tvStatus.text = dfd.currentStatus
        holder.llFM.setOnClickListener {
            //Toaster.shortToast("Clicked CardView")
            if (dfd.currentStatus == "In Progress" || dfd.currentStatus == "Completed") {
                DailyWorkFlowDetailsActivity.startDetailsActivity(
                    holder.itemView.context,
                    dfd.id
                )
            } else {
                if (!hashSet.isNullOrEmpty()) {
                    if (hashSet.contains("In Progress")) {

                        ErrorEventHelper().Error_dialog(context as Activity,Cons.INPROGRESSMSG,true)

                    } else {
                        DailyWorkFlowDetailsActivity.startDetailsActivity(
                            holder.itemView.context,
                            dfd.id
                        )
                    }
                } else {
                    DailyWorkFlowDetailsActivity.startDetailsActivity(
                        holder.itemView.context,
                        dfd.id
                    )
                }
            }

            // Log.d("AdapterData","AdapterData"+dfd)
//On Item click change the button behaviour.
            // holder.llFM.setSelected(if (holder.llFM.isSelected()) true else false)

        }

        Log.d(
            "DailyWorkFLowAdapter",
            "DWF status: ${dfd.currentStatus} DWF item position: $position"
        )


        if (dfd.currentStatus == "In Progress" || dfd.currentStatus == "In progress" || dfd.currentStatus == "in progress" ||
            dfd.currentStatus == "IN PROGRESS"
        ) {
            holder.llFM.isClickable = true
        } else if (dfd.currentStatus == "Completed" || dfd.currentStatus == "") {
            holder.llFM.isClickable = true
        }

        PicassoUtil.loadImage(holder.ivWorkoutlogo, dfd.categoryImage)
        ///downloadMore(position)
        when (dfd.currentStatus) {

            Cons.COMPLETED -> {
                holder.cvHide.visibility = View.VISIBLE
            }
            else -> {
                holder.cvHide.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return dfdData?.size ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cvHide = itemView.findViewById<CheckBox>(R.id.cbHide)
        var llFM = itemView.findViewById<LinearLayout>(R.id.llFM)
        var tvMessangerType = itemView.findViewById<TextView>(R.id.tvMessangerType)
        var tvMessage = itemView.findViewById<TextView>(R.id.tvMessage)
        var tvFacebookQuotes = itemView.findViewById<TextView>(R.id.tvFacebookQuotes)
        var tvLeftPoint = itemView.findViewById<TextView>(R.id.tvLeftPoint)
        var tvStatus = itemView.findViewById<TextView>(R.id.tvStatus)
        var ivWorkoutlogo = itemView.findViewById<ImageView>(R.id.ivWorkoutlogo)
    }

    var previousGetCount = -1

    @Synchronized
    private fun downloadMore(position: Int) {
        if (position > dfdData!!.size - 2 && previousGetCount != dfdData?.size) {

            previousGetCount = dfdData?.size!!
            // listener?.onLoadMore()

        }
    }

    interface OnLoadMoreListener {
        fun onLoadMore()
    }
}