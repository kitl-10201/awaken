package com.awakenwhitelabeling.dashboard.dailyworkoutflow.detail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.others.Cons.Companion.WEB_URL


class WebFragment : BaseFragment() {

    private lateinit var title: TextView
    private lateinit var webView: WebView
    private lateinit var ivBackWebView: ImageView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_web, container, false)
    }

    private class AppWebViewClients : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            url?.let { view?.loadUrl(it) }
            return true
        }

        override fun onPageFinished(view: WebView?, url: String?) {
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title = view.findViewById(R.id.title)
        webView = view.findViewById(R.id.web)
        ivBackWebView = view.findViewById(R.id.ivWebViewBack)
        //arguments?.containsKey(WEB_TITLE) == true &&
        if ( arguments?.containsKey(WEB_URL) == true) {
           // var inputText = arguments?.get(WEB_TITLE)
            var url = arguments?.get(WEB_URL)
           // var checkCategory = NotesHelper.returnCamelCaseWord(inputText.toString())
            //  title.text = NotesHelper.returnCamelCaseWord(inputText.toString())
//            if (checkCategory.equals("Blank")) {
//                title.text = ""
//            } else {
//                title.text = checkCategory
//            }
            Log.e("URL_TO_BE_OPENNED: ",url.toString())
            webView.loadUrl(url.toString())
            webView.webViewClient = AppWebViewClients()
            webView.settings.javaScriptEnabled = true
            webView.settings.useWideViewPort = true
            webView.isVerticalScrollBarEnabled = true
            webView.isHorizontalScrollBarEnabled = true
        }
        ivBackWebView.setOnClickListener {
            activity?.onBackPressed()

//            val fragment = DailyWorkoutFlowDetailFragment()
//            //  fragment.arguments = bundle
//            activity?.supportFragmentManager?.beginTransaction()
//                ?.replace(R.id.flDailworkout, fragment)
//
//                ?.commit()

        }


    }


}

