package com.awakenwhitelabeling.dashboard.dailyworkoutflow.popup

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseDialog
import com.awakenwhitelabeling.dashboard.dailyworkoutflow.detail.DailyWorkoutFlowDetailFragment
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.vimeoPlayable.bean.VimeoVideoDetail
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response


class AutoVideoPlay(
    val mFragmentDW: DailyWorkoutFlowDetailFragment,
    val context: Activity,
    val videoUrl: String,
    val thumbnail: (String) -> Unit
) : BaseDialog(context) {
    var listenerForHidingButton: (() -> Unit)? = null

    lateinit var videoID: String
    var videoURl: String? = null
    var url: String? = "https://vimeo.com/226053498"
    private var completeUrl: String? = null
    var player: SimpleExoPlayer? = null
    var playerView: PlayerView? = null
    private var fullscreenButton: ImageView? = null
    var fullscreen = true
    var progressBar: ProgressBar? = null
    lateinit var bgShadow: CardView
    lateinit var tvSkip: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_for_video)
        progressBar = findViewById(R.id.progressBar)
        bgShadow = findViewById(R.id.PlayVideo)
        this.setCancelable(false)
        playerView?.controllerHideOnTouch
        tvSkip = findViewById(R.id.tvYes)
        mFragmentDW.listener = {
            dismissPlayer()
        }
        setDimBlur(window)
        tvSkip.setOnClickListener {
            dismissPlayer()
        }
        window?.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        player = ExoPlayerFactory.newSimpleInstance(context)
        playerView = findViewById(R.id.player)
        fullscreenButton = playerView?.findViewById(R.id.exo_fullscreen_icon)

        videoURl = videoUrl
        if (videoURl?.contains("vimeo.com") == true) {
            //Toaster.shortToast("Yes")
            val list = videoUrl?.split("/")

            videoID = list?.get(3).toString()
            getVideoPlayableLink()
        } else {
            completeUrl = RetrofitClient.WORKOUT_VIDEO_URL + videoUrl
            Log.d("url.......", "url......." + completeUrl)
            thumbnail.invoke("FROM_SERVER")
            configureVideoPlayer()
        }
    }


    fun dismissPlayer() {

        listenerForHidingButton?.invoke()

            if (this@AutoVideoPlay != null&&  this@AutoVideoPlay.isShowing()) {
                player?.stop()
                this@AutoVideoPlay.dismiss()
            }

        /*} catch (e: IllegalArgumentException) {
            // Handle or log or ignore
            e.printStackTrace()
        } catch (e: Exception) {
            // Handle or log or ignore
            e.printStackTrace()

        } finally {
            // this.Auto = null

        }*/
    }

    private fun getVideoPlayableLink() {
        Utils.viewVisible(progressBar)
        RetrofitClient.getRequest().getVimeoVideoDetails(videoID)
            .enqueue(object : retrofit2.Callback<JsonObject?> {
                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                    Utils.viewGone(progressBar)

                    if (response.isSuccessful && response.code() == 200) {
                        val vimeoDetails =
                            Gson().fromJson(response.body(), VimeoVideoDetail::class.java)
                        val progressiveSize = vimeoDetails.request.files.progressive.size
                        var maxWidth = -1
                        for (i in vimeoDetails.request.files.progressive) {
                            if (maxWidth < i.width) {
                                maxWidth = i.width
                            }
                        }
                        completeUrl =
                            vimeoDetails.request.files.progressive.maxByOrNull { it.width }?.url

                        Log.d("url.......Error", "url......." + completeUrl)
                        thumbnail.invoke(vimeoDetails.video.thumbs.`960`)
                        configureVideoPlayer()
                    } else {
                        Utils.viewGone(progressBar)
                        val jObjError = JSONObject(response.errorBody()?.string())
                        Toaster.shortToast(jObjError.getString("message"))
                    }
                }
                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    Toaster.shortToast(t.localizedMessage.toString())
                    Log.d("getErrotType", "ErrorURL" + t.localizedMessage)
                }
            })
    }








    private fun configureVideoPlayer() {
        fullscreenButton?.setOnClickListener {
            if (fullscreen) {
                setPortraitView()
            }
        }
        playerView?.player = player
        val dataSourceFactory: DataSource.Factory =
            DefaultDataSourceFactory(
                context,
                Util.getUserAgent(
                    context,
                    context.getString(R.string.app_name)
                )
            )
        val videoSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(completeUrl))
        player?.prepare(videoSource)
        player?.playWhenReady = true
        player?.addListener(object : Player.EventListener {

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                if (playbackState == Player.STATE_BUFFERING) {
                    Utils.viewVisible(progressBar)
                } else if (playbackState == Player.STATE_IDLE) {
                    Utils.viewVisible(progressBar)
                } else if (playbackState == Player.STATE_ENDED) {
                    dismiss()
                } else if (!playWhenReady) {
                    Utils.viewVisible(progressBar)
                } else {
                    Utils.viewGone(progressBar)
                }
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                super.onLoadingChanged(isLoading)
                if (isLoading) {
                    tvSkip.isClickable = true
                    Utils.viewVisible(progressBar)
                } else {
                    Utils.viewGone(progressBar)
                }
            }
            //bna kuch   nhi sir main aapko ek baar dikhata hu kya issues hai

            override fun onPlayerError(error: ExoPlaybackException?) {
                super.onPlayerError(error)
                Toaster.shortToast(error.toString())
                player?.stop()

            }
        })
    }

    fun setPortraitView() {
        fullscreenButton?.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_fullscreen_open
            )
        )
        window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        val params = playerView?.layoutParams as RelativeLayout.LayoutParams
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = (200 * context.resources.displayMetrics.density).toInt()
        playerView?.layoutParams = params
        fullscreen = false
    }



/*
    class SomeTask(val context:Context) : AsyncTask<Void, Void, String>() {


        override fun doInBackground(vararg params: Void?): String? {

            val context: Context=context

            val dataSourceFactory: DataSource.Factory =

                DefaultDataSourceFactory(
                    context,
                    Util.getUserAgent(
                        context,
                        context.getString(R.string.app_name)
                    )
                )


            val videoSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(completeUrl))

       }

        override fun onPreExecute() {
            super.onPreExecute()
            // ...
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            // ...
        }
    }
*/



}
