package com.awakenwhitelabeling.dashboard.dailyworkoutflow.bean

data class TestDailyWorkout(
    val `data`: List<Data>,
    val message: String,
    val status: String
) {
    data class Data(
        val category_image: String,
        val created_at: String,
        val current_status: String,
        val id: Int,
        val reward_coins: Int,
        val task_category: String,
        val task_description: String,
        val task_name: String,
        val task_remote_video: String,
        val task_video_platform: Int,
        val task_video_url: String,
        val updated_at: String,
        val user_id: Int
    )
}