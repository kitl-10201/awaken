package com.awakenwhitelabeling.dashboard.dailyworkoutflow.detail

import android.app.Dialog

import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import androidx.core.os.bundleOf
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.App
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.base.MembershipRequest
import com.awakenwhitelabeling.base.UserMembershipKBean
import com.awakenwhitelabeling.dashboard.dailyworkoutflow.bean.DailyTasksByIdBean
import com.awakenwhitelabeling.dashboard.notes.NotesFragment
import com.awakenwhitelabeling.membership.MembershipActivity

import com.awakenwhitelabeling.others.*
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.vimeoPlayable.bean.VimeoBean
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class DailyWorkoutFlowDetailFragment : BaseFragment() {

    private var taskID: Int = -1
    var listener: (() -> Unit)? = null
    var rlTaskDetails: RelativeLayout? = null
    private lateinit var tvStartTask: Button
    private lateinit var tvStartTaskAgain: Button
    private lateinit var tvDesc: TextView
    private lateinit var tvBanner: TextView
    private lateinit var tvpoints: TextView
    private lateinit var ivReplay: ImageView
    private lateinit var ivShapableImageLogo: ImageView
    private lateinit var ivBackDFD: ImageView
    private var videoUrl: String? = null
    private var category_Url: String? = null
    private var category_slug: String? = null
    private var status: String? = null
    private var setCurrentStatus: String? = null
    private var main: RelativeLayout? = null

    lateinit var videoID: String
    var videoURl: String? = null
    var url: String? = "https://vimeo.com/226053498"
    private var completeUrl: String? = null
    var player: SimpleExoPlayer? = null
    var playerView: PlayerView? = null
    private var fullscreenButton: ImageView? = null
    private var audioPath: String? = null
    private var mDailyWorkoutFlowDetailFragment: DailyWorkoutFlowDetailFragment? = null
    private lateinit var vDialog: Dialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_daily_workout_flow_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDailyWorkoutFlowDetailRefremce(view)
        parseBundle()
        checkMembershipStatus()
        getWorkoutDetails()

    }

    private fun mDailyWorkoutFlowDetailRefremce(view: View) {

        mDailyWorkoutFlowDetailFragment = this
        tvStartTask = view.findViewById(R.id.tvStartTest)
        tvStartTaskAgain = view.findViewById(R.id.tvStartAgain)
        tvDesc = view.findViewById(R.id.tvDescDFD)
        ivReplay = view.findViewById(R.id.ivReplay)
        ivShapableImageLogo = view.findViewById(R.id.ivShapableImageLogo)
        tvpoints = view.findViewById(R.id.tvpoints)
        tvBanner = view.findViewById(R.id.tvBanner)
        main = view.findViewById(R.id.relativeLayout)
        ivBackDFD = view.findViewById(R.id.ivBackDetails)
        ivBackDFD.setOnClickListener {
            activity?.onBackPressed()
        }
        tvStartTask.setOnClickListener {
            if (tvStartTask.isPressed) {
                tvStartTask.isClickable = false
                ivBackDFD.isClickable = false
                tvStartTask.isClickable = false

                if (setCurrentStatus.equals("Completed")) {
                    playCoinSound()
                }
                CheckStatus()
            } else {
                tvStartTask.isClickable = true
            }
        }
        tvStartTaskAgain.setOnClickListener {

            if (tvStartTaskAgain.isPressed) {
                tvStartTaskAgain.isClickable = false
                CheckStatusAgin()
            } else {
                tvStartTaskAgain.isClickable = true
            }

        }

        /*if (tvStartTask.text == "Mark as Complete") {
            tvStartTask.setOnClickListener {
                Toaster.shortToast("Completed!!!!")
            }
        }*/

    }

    private fun CheckStatus() {

        if (category_slug == "profile") {
            if (status.isNullOrEmpty() || status == "New") {
                setCurrentStatus = "In Progress"
                tasksStatus()
                val bundle = bundleOf(
                    Pair(Cons.WEB_TITLE, tvBanner.text.toString()),
                    // Pair(Cons.WEB_URL, WebUrlHelper.getUrlFromTitle(postType.text.toString()))
                    Pair(Cons.WEB_URL, category_Url)
                )
                val fragment = WebFragment()
                fragment.arguments = bundle
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.flDailworkout, fragment)
                    ?.addToBackStack(null)
                    ?.detach(DailyWorkoutFlowDetailFragment())
                    ?.attach(fragment)
                    ?.commit()
            } else if (status == "In Progress") {
                setCurrentStatus = "Completed"
                tasksStatus()

                activity?.onBackPressed()
            }
        } else if (category_slug == "messenger") {

            if (status.isNullOrEmpty() || status == "New") {
                setCurrentStatus = "In Progress"
                tasksStatus()
                val bundle = bundleOf(
                    Pair(Cons.WEB_TITLE, tvBanner.text.toString()),
                    Pair(Cons.WEB_URL, category_Url)
                )
                val fragment = WebFragment()
                fragment.arguments = bundle
                activity?.supportFragmentManager?.beginTransaction()
                    //            .add(R.id.flDailworkout,DailyWorkFlowFragment())
                    ?.replace(R.id.flDailworkout, fragment)
                    ?.addToBackStack(null)
                    ?.commit()

            } else if (status == "In Progress") {
                showLoader()
                val comp = "Completed"
                // tasksStatus()
                tasksStatusComplete(comp)
            }

        } else if (category_slug == "group") {

            if (status.isNullOrEmpty() || status == "New") {
                setCurrentStatus = "In Progress"
                tasksStatus()
                val bundle = bundleOf(
                    Pair(Cons.WEB_TITLE, tvBanner.text.toString()),
                    Pair(Cons.WEB_URL, category_Url)
                )

                val fragment = WebFragment()
                fragment.arguments = bundle
                activity?.supportFragmentManager?.beginTransaction()
                    //            .add(R.id.flDailworkout,DailyWorkFlowFragment())
                    ?.replace(R.id.flDailworkout, fragment)
                    ?.addToBackStack(null)
                    ?.commit()

            } else if (status == "In Progress") {

                setCurrentStatus = "Completed"
                tasksStatus()
            }
        } else if (category_slug == "notes") {
            if (status.isNullOrEmpty() || status == "New") {
                setCurrentStatus = "In Progress"
                tasksStatus()
                val fragment = NotesFragment()
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.flDailworkout, fragment)
                    ?.addToBackStack(null)
                    ?.commit()

            } else if (status == "In Progress") {
                activity?.onBackPressed()
                setCurrentStatus = "Completed"
                tasksStatus()

            }
        }
    }


    private fun CheckStatusAgin() {
        Log.d("dfsfffsdf", "No. of times button get clicked by user!")


        if (category_slug == "profile") {
            val bundle = bundleOf(
                Pair(Cons.WEB_TITLE, tvBanner.text.toString()),
                // Pair(Cons.WEB_URL, WebUrlHelper.getUrlFromTitle(postType.text.toString()))
                Pair(Cons.WEB_URL, category_Url)
            )
            val fragment = WebFragment()
            fragment.arguments = bundle
            tvStartTaskAgain.isClickable = true
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.flDailworkout, fragment)
                ?.addToBackStack(null)
                ?.detach(DailyWorkoutFlowDetailFragment())
                ?.attach(fragment)
                ?.commit()

        } else if (category_slug == "messenger") {
            val bundle = bundleOf(
                Pair(Cons.WEB_TITLE, tvBanner.text.toString()),
                Pair(Cons.WEB_URL, category_Url)
            )
            val fragment = WebFragment()
            fragment.arguments = bundle
            tvStartTaskAgain.isClickable = true
            activity?.supportFragmentManager?.beginTransaction()
                //            .add(R.id.flDailworkout,DailyWorkFlowFragment())
                ?.replace(R.id.flDailworkout, fragment)
                ?.addToBackStack(null)
                ?.commit()


        } else if (category_slug == "group") {

            val bundle = bundleOf(
                Pair(Cons.WEB_TITLE, tvBanner.text.toString()),
                Pair(Cons.WEB_URL, category_Url)
            )
            val fragment = WebFragment()
            fragment.arguments = bundle
            tvStartTaskAgain.isClickable = true
            activity?.supportFragmentManager?.beginTransaction()
                //            .add(R.id.flDailworkout,DailyWorkFlowFragment())
                ?.replace(R.id.flDailworkout, fragment)
                ?.addToBackStack(null)
                ?.commit()

        } else if (category_slug == "notes") {
            val fragment = NotesFragment()
            tvStartTaskAgain.isClickable = true
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.flDailworkout, fragment)
                ?.addToBackStack(null)
                ?.commit()

        }
    }


    override fun onPause() {

        super.onPause()
        player?.stop()
        player == null
        vDialog.dismiss()
    }

    fun getWorkoutDetails() {
        showLoader()
        if (!App.get().isConnected()) {
            hideLoader()
            //  activity?.let { InternetConnectionDialog(it, null).show() }
            popUpInternetConnectionDialog()
            return
        }
        getWorkoutDetail(taskID.toString(), object : CallBack<DailyTasksByIdBean.Data>() {
            override fun onSuccess(t: DailyTasksByIdBean.Data) {
                //  hideLoader()
                Handler().postDelayed(
                    {
                        hideLoader()
                    }, 5000
                )

                tvDesc.text = t.task_description
                tvBanner.text = t.task_name
                tvpoints.text = t.reward_coins.toString() + " Coins to complete this tasks"
                // postType.text = t.task_category
                PicassoUtil.loadImage(ivShapableImageLogo, t?.category_image)
                if (t.task_video_platform.equals(3)) {
                    videoUrl = t.task_remote_video
                    //  Toaster.shortToast(videoUrl.toString())
                } else if (t.task_video_platform.equals(1)) {
                    // videoUrl?.get(3)
                    videoUrl = t.task_video_url
                    //  Toaster.shortToast(videoUrl.toString())
                }
                audioPath = t.task_completed_sound
                audioPath = RetrofitClient.BASE_URL + audioPath
                category_Url = t.category_url
                category_slug = t.category_slug
                status = t.current_status
                setStatus()
                videoUrl?.let { it1 -> VimeoPlayer(it1) }
                //  videoUrl?.let { setupThumbnail(it) }
                Log.d("btn", "btn" + status)
                ivReplay.setOnClickListener {
                    videoUrl?.let { it1 -> VimeoPlayer(it1) }
                    // videoUrl?.let { setupThumbnail(it) }
                }
                Utils.viewVisible(main)
            }

            override fun onError(error: String?) {
                super.onError(error)
                hideLoader()
            }
        })
    }

    private fun parseBundle() {
        if (arguments != null) {
            if (arguments?.containsKey(DailyWorkFlowDetailsActivity.EXTRA_WORKOUT) == true) {
                taskID =
                    arguments?.getInt(DailyWorkFlowDetailsActivity.EXTRA_WORKOUT, -1)!!

            }
        }
    }

/*
    private fun videoIntoDialog() {
        if (videoUrl == null || videoUrl.equals("")) {
            Toaster.shortToast("Video not available")
        } else {

            AutoVideoPlay(mDailyWorkoutFlowDetailFragment!!, requireActivity(), videoUrl!!) {
                setupThumbnail(it)
                hideLoader()
            }.show()
        }
    }
*/


    fun getWorkoutDetail(id: String, callBack: CallBack<DailyTasksByIdBean.Data>) {
        if (!App.get().isConnected()) {
            Toaster.shortToast("Please connect to internet to see more details...")
            callBack.onError(null);
            return
        }
        RetrofitClient.getRequest().getDailyWorkById(id)
            .enqueue(object : Callback<JsonObject?> {
                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    ErrorUtils.onFailure(t)
                    callBack.onSuccess(null)
                }

                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>?) {
                    try {
                        val code = response?.code() ?: 500
                        if (code == 200 && response?.isSuccessful == true) {
                            // hideLoader()
                            rlTaskDetails?.visibility = View.VISIBLE

                            callBack.onSuccess(
                                Gson().fromJson(
                                    Utils.convertToJSON(response.body())?.optJSONObject("data")
                                        ?.toString() ?: "",
                                    DailyTasksByIdBean.Data::class.java
                                )
                            )

                            Log.d("TaskDetailsActivity", "You data: " + response.body())

                        } else {
                            val jObjError = JSONObject(response?.errorBody()?.string())
                            Toaster.shortToast(
                                jObjError.getString("message")
                            )
                        }

                        /* else if (code == 400 || code == 401) {
                                ErrorUtils.parseError(response?.errorBody()?.string())
                                callBack.onSuccess(null)
                            } else {
                                callBack.onSuccess(null)
                                Toaster.somethingWentWrong()
                            }*/
                    } catch (e: Exception) {
                        //  callBack.onSuccess(null)
                        Toaster.shortToast(e.toString())
                    }
                }
            })
    }

    private fun setStatus() {

        if (status.isNullOrEmpty() || status == "New") {
            tvStartTask.text = "Start Task"
            setCurrentStatus = "In Progress"

        } else if (status == "In Progress") {

            tvStartTask.text = "Mark as Completed"
            setCurrentStatus = "Completed"
            tvStartTaskAgain.visibility = View.VISIBLE

        } else if (status == "Completed") {
            tvStartTask.text = "Completed"
            tvStartTask.isClickable = false

        }
    }

    private fun playCoinSound() {
        try {
            var mp: MediaPlayer?
            // mp = MediaPlayer.create(context, R.raw.awaken_sound)
            mp = MediaPlayer()
            if (!audioPath.equals("") && audioPath != null) {
                mp.setDataSource(audioPath)

                if (mp != null) {
                    mp.prepare()
                    mp?.start()
                } else {
                    mp?.stop()
                    mp?.release()
                }
            } else {

            }
        } catch (e: Exception) {
        }
    }

    private fun tasksStatus() {
        if (!App.get().isConnected()) {
            //   activity?.let { InternetConnectionDialog(it, null).show() }
            Toaster.shortToast("Check you Internet connection")
            return
        }

        val hashMap = HashMap<String, Any>()
        hashMap["task_id"] = taskID
        hashMap["task_status"] = setCurrentStatus.toString()
        val loginRequest = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        loginRequest.updateTaskStatus(hashMap).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Log.e("TAG", t.localizedMessage)
                hideLoader()
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                try {

                    tvStartTask.isClickable = true
                    ivBackDFD.isClickable = true
                    tvStartTask.isClickable = true
                    hideLoader()
                    Log.d("TAG", " Response")
                    if (response.isSuccessful) {
                        if (response.body() == null) {
                            Toaster.somethingWentWrong()
                            return
                        } else {
                            Log.d("Response111", "Response111" + response.body())
                        }
                    } else {
                        val jObjError = JSONObject(response.errorBody()?.string())
                        Toaster.shortToast(jObjError.getString("message"))
                    }
                } catch (e: Exception) {
                    Log.d("onException", e.localizedMessage)
                    // Toaster.shortToast("Handle")

                }
            }
        })

    }

    private fun tasksStatusComplete(comp: String) {


        if (!App.get().isConnected()) {
            //   activity?.let { InternetConnectionDialog(it, null).show() }
            Toaster.shortToast("Check you Internet connection")
            return
        }
        val hashMap = HashMap<String, Any>()
        hashMap["task_id"] = taskID
        hashMap["task_status"] = comp
        val loginRequest = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        loginRequest.updateTaskStatus(hashMap).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Log.e("TAG", t.localizedMessage)
                hideLoader()
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                try {
                    hideLoader()
                    tvStartTask.isClickable = true
                    ivBackDFD.isClickable = true
                    tvStartTask.isClickable = true
                    Log.d("TAG", " Response")
                    if (response.isSuccessful) {
                        activity?.onBackPressed()

                        Log.d("Response111", "Response111" + response.body())


                    } else {
                        val jObjError = JSONObject(response.errorBody()?.string())
                        Toaster.shortToast(jObjError.getString("message"))

                    }
                } catch (e: Exception) {
                    Log.d("onException", e.localizedMessage)
                    // Toaster.shortToast("Handle")
                }
            }
        })


    }


    private fun checkMembershipStatus() {
        MembershipRequest.checkMembership(object : CallBack<UserMembershipKBean.Data>() {
            override fun onSuccess(t: UserMembershipKBean.Data?) {
                if (t == null) {
                    startActivity(Intent(context, MembershipActivity::class.java))
                    activity?.finish()
                    return
                }
                if (t?.isValid == "false") {

                    val customDialog = Dialog(requireActivity())
                    customDialog.setContentView(R.layout.dialog_membership_expired)
                    customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                    customDialog.window?.setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    val btnOkAN = customDialog.findViewById(R.id.tvUpdatePlan) as Button
                    var dateValidTil = customDialog.findViewById(R.id.validDate) as TextView

                    var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    var setDateFormat = SimpleDateFormat("MMM d, yy");
                    //  var setTimeFormat = SimpleDateFormat("hh:mm a");
                    val text = setDateFormat.format(getDateFormat.parse(t?.validTo))
                    dateValidTil.text = text + " (EST)"
                    customDialog.setCancelable(false)
                    btnOkAN.setOnClickListener {
                        val intent = Intent(context, MembershipActivity::class.java)
                        startActivity(intent)
                        requireActivity().finish()
                    }
                    customDialog.show()

                    //  validityExpire()

                } else {
/*                    MembershipRequest.saveMembershipValidInToLocal()
                    DashboardActivity.startActivity(requireContext())*/
                }
            }
        })
    }

/*
    override fun onStart() {
        super.onStart()
        videoUrl?.let { VimeoPlayer(it) }
    }
*/

    private fun VimeoPlayer(videoUrl: String) {
        // setupThumbnail(videoUrl)
        vDialog = Dialog(requireContext())
        vDialog.setContentView(R.layout.dialog_for_video)
        vDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        vDialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        vDialog.setCancelable(false)
        val tvSkip = vDialog.findViewById<TextView>(R.id.tvYes)
        player = ExoPlayerFactory.newSimpleInstance(context)
        playerView = vDialog.findViewById(R.id.player)
        fullscreenButton = playerView?.findViewById(R.id.exo_fullscreen_icon)
        videoURl = videoUrl
        if (videoURl?.contains("vimeo.com") == true) {
            val list = videoUrl?.split("/")
            videoID = list?.get(3).toString()
            getVideoPlayableLink()
        } else {
            completeUrl = RetrofitClient.WORKOUT_VIDEO_URL + videoUrl
            Log.d("url.......", "url......." + completeUrl)

            configureVideoPlayer()
        }
        if (vDialog.isShowing) {
            Utils.viewGone(ivReplay)
        } else {
            Utils.viewVisible(ivReplay)
        }


        tvSkip.setOnClickListener {
            if (player != null) {
                player?.stop()
                player = null
                vDialog.dismiss()
                hideLoader()
            }
        }

        if (!vDialog.isShowing) {
            player?.stop()
            vDialog.dismiss()
        }
        vDialog.show()

    }

    private fun getVideoPlayableLink() {
        //  showLoader()
        RetrofitClient.getVimeoAccess("7c6c7d0ddab13693fb4b72684c11a337")
            .create(RetrofitApi::class.java).getVimeoVideoDetails(videoID)
            .enqueue(object : retrofit2.Callback<JsonObject?> {
                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                    try {
                        if (response.isSuccessful && response.code() == 200) {
                            val vimeoThumbnail = Gson().fromJson<VimeoBean>(
                                response.body(),
                                VimeoBean::class.java
                            )
                            var maxWidth = 0
                            var maxImagePosition = 0
                            for (i in 0 until vimeoThumbnail.files.size) {
                                if (i == 0) {
                                    if (vimeoThumbnail.files[i].width != null) {
                                        maxWidth = vimeoThumbnail.files[i].width
                                        maxImagePosition = i
                                    }
                                }

                                if (vimeoThumbnail.files[i].width != null) {
                                    if (maxWidth < vimeoThumbnail.files[i].width) {
                                        maxWidth = vimeoThumbnail.files[i].width
                                        maxImagePosition = i
                                    }
                                }
                            }

                            Log.e(
                                "fdssdf",
                                "getVideoPlayableLink: invoke " + vimeoThumbnail.files[maxImagePosition].link
                            )
                            completeUrl = vimeoThumbnail.files[maxImagePosition].link
                            configureVideoPlayer()
                        }
                    } catch (e: Exception) {
                        Log.e("fdssdf", "onException: ${e.localizedMessage}")
                    }
                }

                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    Toaster.shortToast(t.localizedMessage.toString())
                    Log.d("getErrotType", "ErrorURL" + t.localizedMessage)
                }
            })
    }

    private fun configureVideoPlayer() {
        playerView?.player = player
        val dataSourceFactory: DataSource.Factory =
            DefaultDataSourceFactory(
                context,
                Util.getUserAgent(
                    context,
                    context?.getString(R.string.app_name)
                )
            )
        val videoSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(completeUrl))
        completeUrl?.let { setupThumbnail(it) }
        player?.prepare(videoSource)
        player?.playWhenReady = true
        player?.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                if (playbackState == Player.STATE_BUFFERING) {
                    showLoader()
                } else if (playbackState == Player.STATE_IDLE) {
                    hideLoader()
                } else if (playbackState == Player.STATE_ENDED) {
                    vDialog.dismiss()
                    player?.stop()
                    hideLoader()

                } else if (!playWhenReady) {
                    // showLoader()
                } else {
                    hideLoader()
                }
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                super.onLoadingChanged(isLoading)
                if (isLoading) {
                    hideLoader()
//                    showLoader()
                } else {
                    hideLoader()
                }
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                super.onPlayerError(error)
                Toaster.shortToast(error.toString())
                player?.stop()
            }
        })
    }

    private fun setupThumbnail(it: String) {
        if (it.isNotEmpty()) {
            if (it == "FROM_SERVER") {
                val retriever = MediaMetadataRetriever()
                retriever.setDataSource(
                    RetrofitClient.WORKOUT_VIDEO_URL + videoUrl,
                    HashMap<String, String>()
                )
                val bitmap: Bitmap? = retriever.getFrameAtTime(
                    2000000,
                    MediaMetadataRetriever.OPTION_CLOSEST_SYNC
                )
                ivReplay.setImageBitmap(bitmap)
            } else {
                PicassoUtil.loadFromWeb(ivReplay, it)
            }
        }
    }
}


