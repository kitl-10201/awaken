package com.awakenwhitelabeling.dashboard.dailyworkoutflow.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity


class DailyWorkFlowDetailsActivity : BaseActivity() {


    companion object {
        const val EXTRA_WORKOUT = "extra.workout.data"
        const val EXTRA_FULL_DETAIL = "extra.workout.full.detail"

        // method to start activity to show workout detail
        fun startDetailsActivity(context: Context, data: Int) {
            context.startActivity(
                Intent(context, DailyWorkFlowDetailsActivity::class.java)
                    .putExtra(EXTRA_WORKOUT, data)
                    .putExtra(EXTRA_FULL_DETAIL, true)
            )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dailyworkoutflow)
        val bundle= intent.extras
        val fragment=DailyWorkoutFlowDetailFragment()
        fragment.arguments=bundle
        supportFragmentManager.beginTransaction()
//            .add(R.id.flDailworkout,DailyWorkFlowFragment())
            .add(R.id.flDailworkout, fragment)

            .commit()
    }

}


