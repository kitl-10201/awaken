package com.awakenwhitelabeling.dashboard.dailyworkoutflow

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.base.MembershipRequest
import com.awakenwhitelabeling.base.UserMembershipKBean
import com.awakenwhitelabeling.dashboard.dailyworkoutflow.bean.NewDailyWorkFlowBean
import com.awakenwhitelabeling.dashboard.other.DashboardHelper
import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.ResourceUtils
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.android.material.appbar.MaterialToolbar
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class NewDailWorkFlowFragment: BaseFragment() {




    private val groups = mutableListOf<NewDailyWorkFlowBean.Data.Task>()
    private var noGroupAvailable: TextView? = null
    private var mAdapter: DailyWorkFlowAdapter? = null
    private var isInProgress: Boolean = false;
    var OFFSET = 0
    var isLastPageL = false
    private lateinit var ivBackDWF: ImageView
    private lateinit var main: LinearLayout
    private lateinit var dfdRcv: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var layoutManager: GridLayoutManager



    private val list = ArrayList<NewDailyWorkFlowBean.Data.Task>()
    private var adapter = DailyWorkFlowAdapter(list)
    private var offSet = 0
    private var isLoading = false


    companion object {
        private const val EXTRA_TITLE = "Daily Work Flow"
        private const val EXTRA_API_ENDPOINT = "frag.endpoint"

        // U_E -> URL_ENDPOINT
        private const val U_E_TRAINING = "training_categories"
        private const val U_E_WORKFLOW = "get_tasks"
        private const val U_E_NOTES = "get_user_notes"
        private const val U_E_ACCOUNT = "get_user_data"

        fun newInstance(title: String, position: Int): DailyWorkFlowFragment {
            val args = Bundle()
            args.putString(EXTRA_TITLE, title)

            val endPoint = when (position) {
                0 -> U_E_TRAINING
                1 -> U_E_WORKFLOW
                2 -> U_E_NOTES
                else -> U_E_ACCOUNT
            }

            args.putString(EXTRA_TITLE, title)
            args.putString(EXTRA_API_ENDPOINT, endPoint.toString())

            val fragment = DailyWorkFlowFragment()
            fragment.arguments = args
            return fragment
        }
    }
    private fun setDailWorkFlowRefrence(view: View) {
        dfdRcv = view.findViewById(R.id.rvTasks)
        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))
        checkMembershipStatus()
        layoutManager = GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false);
        dfdRcv.layoutManager = layoutManager
        mAdapter = DailyWorkFlowAdapter(
            groups
        )
        dfdRcv.adapter = mAdapter

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setTitleTextColor(ResourceUtils.getColor(R.color.black))
        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setNavigationIconTint(ResourceUtils.getColor(R.color.black))






        dfdRcv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    val visibleItemCount = layoutManager.childCount
                    val pastVisibleItem = layoutManager.findFirstVisibleItemPosition()
                    val total = adapter.itemCount

                    if (!isLoading) {
                        if ((visibleItemCount + pastVisibleItem) >= total) {
                            offSet += 1
                            getMessagesList()
                        }
                    }
                }
            }
        })










    }

    private fun parseValue() {

        val title = arguments?.getString(DailyWorkFlowFragment.EXTRA_TITLE) ?: "Daily Work Flow"
        DashboardHelper.setTitle(title)
    }

    private fun checkMembershipStatus() {


        MembershipRequest.checkMembership(object : CallBack<UserMembershipKBean.Data>() {
            override fun onSuccess(t: UserMembershipKBean.Data?) {
                if (t == null) {
                    startActivity(Intent(context, MembershipActivity::class.java))
                    activity?.finish()
                    return
                }
                if (t?.isValid == "false") {

                    val customDialog = Dialog(requireActivity())
                    customDialog.setContentView(R.layout.dialog_membership_expired)
                    customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                    customDialog.window?.setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )

                    val btnOkAN = customDialog.findViewById(R.id.tvUpdatePlan) as Button
                    var dateValidTil = customDialog.findViewById(R.id.validDate) as TextView

                    var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    var setDateFormat = SimpleDateFormat("MMM d, yy");
                    //  var setTimeFormat = SimpleDateFormat("hh:mm a");

                    val text = setDateFormat.format(getDateFormat.parse(t?.validTo))
                    dateValidTil.text = text + " (EST)"
                    customDialog.setCancelable(false)
                    btnOkAN.setOnClickListener {
                        val intent = Intent(context, MembershipActivity::class.java)
                        startActivity(intent)
                        requireActivity().finish()

                    }

                    customDialog.show()


                    //  validityExpire()

                } else {
/*                    MembershipRequest.saveMembershipValidInToLocal()
                    DashboardActivity.startActivity(requireContext())*/
                }
            }
        })

        /*MembershipRequest.checkMembership(object :
            CallBack<UserMembershipKBean.Data>()
        {
            override fun onSuccess(t: UserMembershipKBean.Data?) {
                hideLoader()
                if (t == null) {
                    startActivity(Intent(context, MembershipActivity::class.java))
                    activity?.finish()
                    return
                }
                if (t?.isValid == "true") {
                Toaster.shortToast("Already member")

                *//*startActivity(Intent(context, MembershipActivity::class.java))
                    activity?.finish()*//*
                } else {
                    (activity as DashboardActivity).setUpUserDetails()
                }

                *//*else{
                        DashboardActivity.startActivity(requireContext())
                    }*//*
            }

            override fun onError(error: String?) {
                super.onError(error)
                hideLoader()
                DashboardActivity.startActivity(requireContext())
            }
        })*/
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dailyworkflow, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parseValue()
        setDailWorkFlowRefrence(view)

    }



    private fun getMessagesList() {
        val communicationData = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        communicationData.getGroupsList(offSet).enqueue(object : Callback<JsonObject?> {
            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()

                                try {
                    if (response.isSuccessful && response.code() == 200) {
                        val communicationBean =
                            Gson().fromJson(response.body(), NewDailyWorkFlowBean::class.java)
                        val arrList: List<NewDailyWorkFlowBean.Data.Task>? =
                            communicationBean.data.tasks
                        if (arrList?.size != 0) {
                            dfdRcv.visibility = View.VISIBLE
                          //  tvNoData.visibility = View.GONE
                            if (offSet == 0) {
                                list.clear()
                                list.addAll(arrList!!)
                                adapter.notifyDataSetChanged()
                            } else {
                                list.addAll(arrList!!)
                                adapter.notifyDataSetChanged()
                            }
                        } else if (list.isNullOrEmpty()) {
                            dfdRcv.visibility = View.GONE
                            //tvNoData.visibility = View.VISIBLE
                        }
                    }
                } catch (e: Exception) {
                    Log.d("exception:", "onExceptionOccur" + e.localizedMessage)
                }
            }

            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()

            }

        })
    }

    private fun setAdapter() {
        dfdRcv.layoutManager = layoutManager
        adapter = DailyWorkFlowAdapter(list)
        dfdRcv.adapter = adapter
    }








}