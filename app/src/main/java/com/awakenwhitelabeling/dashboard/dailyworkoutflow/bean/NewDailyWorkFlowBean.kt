package com.awakenwhitelabeling.dashboard.dailyworkoutflow.bean


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class NewDailyWorkFlowBean(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("is_enabled")
        val isEnabled: Int,
        @SerializedName("tasks")
        val tasks: List<Task>
    ) {
        data class Task(
            @SerializedName("category_image")
            val categoryImage: String,
            @SerializedName("created_at")
            val createdAt: String,
            @SerializedName("current_status")
            val currentStatus: String,
            @SerializedName("id")
            val id: Int,
            @SerializedName("reward_coins")
            val rewardCoins: Int,
            @SerializedName("task_category")
            val taskCategory: String,
            @SerializedName("task_description")
            val taskDescription: String,
            @SerializedName("task_name")
            val taskName: String,
            @SerializedName("task_remote_video")
            val taskRemoteVideo: String,
            @SerializedName("task_video_platform")
            val taskVideoPlatform: Int,
            @SerializedName("task_video_url")
            val taskVideoUrl: String,
            @SerializedName("updated_at")
            val updatedAt: String,
            @SerializedName("user_id")
            val userId: Int
        ):Serializable
    }
}