package com.awakenwhitelabeling.dashboard.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.RingtoneManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.App


class SendNotification {
    companion object {
        @RequiresApi(Build.VERSION_CODES.M)
        fun sendNotification(
            context: Context,
            intent: Intent,
            title: String,
            messageBody: String,
            bitmap: Bitmap?,
        ) {
            val REQUEST_CODE = System.currentTimeMillis().toInt()
            val pendingIntent = TaskStackBuilder.create(context).run {
                // Add the intent, which inflates the back stack
                addNextIntentWithParentStack(intent)
                // Get the PendingIntent containing the entire back stack
                getPendingIntent(
                    REQUEST_CODE,
                    PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                )
            }

            val channelId = App.get().getString(R.string.app_name)
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_app_icon)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
            if (bitmap != null) {
                notificationBuilder.setLargeIcon(bitmap).setStyle(
                    NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap)
                        .bigLargeIcon(null)
                )
            }

            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    channelId,
                    App.get().getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH
                )
                notificationManager.createNotificationChannel(channel)
            }
            // val id= Random(System.currentTimeMillis()).nextInt(1000)
            val id = System.currentTimeMillis()
            notificationManager.notify(
                REQUEST_CODE /* ID of notification */,
                notificationBuilder.build()
            )
        }
    }
}