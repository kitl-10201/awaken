package com.awakenwhitelabeling.dashboard.notification.bean


import com.google.gson.annotations.SerializedName

data class NotificationBean(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("current_page")
        val currentPage: Int,
        @SerializedName("data")
        val `data`: List<Data>,
        @SerializedName("first_page_url")
        val firstPageUrl: String,
        @SerializedName("from")
        val from: Int,
        @SerializedName("last_page")
        val lastPage: Int,
        @SerializedName("last_page_url")
        val lastPageUrl: String,
        @SerializedName("next_page_url")
        val nextPageUrlDOkHttp: String,
        @SerializedName("path")
        val path: String,
        @SerializedName("per_page")
        val perPage: Int,
        @SerializedName("prev_page_url")
        val prevPageUrl: Any,
        @SerializedName("to")
        val to: Int,
        @SerializedName("total")
        val total: Int
    ) {
        data class Data(
            @SerializedName("notification_description")
            val notificationDescription: String,
            @SerializedName("notification_id")
            val notificationId: Int,
            @SerializedName("notification_published_time")
            val notificationPublishedTime: String,
            @SerializedName("notification_title")
            val notificationTitle: String,
            @SerializedName("read_status")
            val readStatus: Int,
            @SerializedName("user_id")
            val userId: Int
        )
    }
}