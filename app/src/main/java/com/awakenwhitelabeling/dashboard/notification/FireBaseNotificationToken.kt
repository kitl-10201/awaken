package com.awakenwhitelabeling.dashboard.notification

import android.annotation.SuppressLint
import android.util.Log
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.android.gms.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object FireBaseNotificationToken {
    private val TAG = "Refresh Notification Token"

    @SuppressLint("LongLogTag")
    fun getToken() {
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener { task: Task<String> ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@addOnCompleteListener
                }
                val token = task.result
                Log.e(TAG, "FCM_TOKEN: $token")
                //saveToken(token);
                saveFCMTokenNew(token)
            }
    }


    fun saveFCMTokenNew(fcm_token: String) {
        val hashMap = HashMap<String, String>()
        hashMap["fcm_token"] = fcm_token
        val call: Call<JsonObject> = RetrofitClient.getRequest().saveFCMTokenNew(hashMap)
        call.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.code() == 200) {
                    Log.d("", "FCM token saved:" + response.body())
                } else {
                    Log.d("", "Unable to save FCM token:" + response.body())
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.d("", "Something went wrong while saving fcm token: " + t.localizedMessage)
            }
        })
    }
}