package com.awakenwhitelabeling.dashboard.notification

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity
import com.awakenwhitelabeling.dashboard.DashboardActivity
import com.awakenwhitelabeling.dashboard.notification.bean.NotificationBean
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.Cons
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class NotificationActivity : BaseActivity() {

    lateinit var ivBackNotification: ImageView
    lateinit var rcvNotification: RecyclerView
    private lateinit var mAdapter: NotificationAdapter
    var newWorkoutData = ArrayList<NotificationBean.Data.Data>()

    private lateinit var layoutManager: GridLayoutManager
    private val list = ArrayList<NotificationBean.Data.Data>()
    private lateinit var tvNoData: TextView
    private var ACTIVITY_TYPE = ""
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    var current_page = 1
    private var isLoading = false
    var totalPage = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        ACTIVITY_TYPE = intent.getStringExtra(Cons.Notification).toString()

        tvNoData = findViewById(R.id.tvNoData)
        rcvNotification = findViewById(R.id.rcvNotification)
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)
        ivBackNotification = findViewById(R.id.iv_back_notification)
        rcvNotification.layoutManager = LinearLayoutManager(this)
        ivBackNotification.setOnClickListener {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
            this.finish()
        }

        swipeRefreshLayout.setOnRefreshListener {
            current_page = 1
            getNotificationList()
            swipeRefreshLayout.isRefreshing =
                false   // reset the SwipeRefreshLayout (stop the loading spinner)
        }
    }
    override fun onResume() {
        super.onResume()
        setAdapterToList()
        getNotificationList()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (ACTIVITY_TYPE == Cons.Notification) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
            this.finish()
        } else {
            this.finish()
        }
    }
    override fun onStart() {
        super.onStart()
        layoutManager = GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false);
        rcvNotification.layoutManager = layoutManager
        mAdapter = NotificationAdapter(
            list, onItemClick
        )
        rcvNotification.adapter = mAdapter
        rcvNotification.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    val visibleItemCount = layoutManager.childCount
                    val pastVisibleItem = layoutManager.findFirstVisibleItemPosition()
                    val total = mAdapter.itemCount
                    if (!isLoading) {
                        if ((visibleItemCount + pastVisibleItem) >= total) {
                            while (current_page <= total) {
                                current_page += 1
                                getNotificationList()
                            }
                        }
                    }
                }
            }
        })
    }

    private fun getNotificationList() {
        val newWorkoutCall = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        newWorkoutCall.getNotification(current_page).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                Log.d("dfsdfsfd", "dfsdfsfds     " + response.body())
                hideLoader()
                try {
                    if (response.isSuccessful && response.code() == 200) {
                        Log.d("dfsdfsfd", "dfsdfsfds     " + response.body())
                        if (response.body() == null) {
                            Toaster.shortToast("Error in API" + response.body())
                            return
                        }
                        try {
                            val newNotificationInfo =
                                Gson().fromJson(response.body(), NotificationBean::class.java)
                            totalPage = newNotificationInfo.data.total
                            newWorkoutData.clear()
                            newWorkoutData.addAll(newNotificationInfo.data.data)
                            list.addAll(newNotificationInfo.data.data)
                            Log.d("dfsdfs", "dfsdjksd" + newNotificationInfo.data.data.size)
                            if (mAdapter!!.itemCount != 0) {
                                rcvNotification.adapter = mAdapter
                                mAdapter.notifyDataSetChanged()
                                Utils.viewGone(tvNoData)
                            } else {
                                Utils.viewVisible(tvNoData)
                                rcvNotification.adapter = mAdapter
                                mAdapter.notifyDataSetChanged()
                                Log.d("dfsdfs", "dfsdfsdgs " + mAdapter)
                            }
                            var mAdapter = NotificationAdapter(list, onItemClick)
                            rcvNotification.adapter = mAdapter

                        } catch (e: Exception) {
                            Log.e("NewWorkouts ", e.localizedMessage);
                        }
                    }
                    else if (response.code()==404 || response.code()==401)
                    {

                        Utils.viewVisible(tvNoData)
                    }

                    else {

                        val jObjError = JSONObject(response.errorBody()?.string())
                        Toaster.shortToast(jObjError.getString("message"))
                    }
                } catch (e: Exception) {
                    Log.d("exception:", "onExceptionOccur" + e.localizedMessage)
                }
            }
        })
    }

    private fun setAdapterToList() {
        mAdapter = NotificationAdapter(newWorkoutData, onItemClick)
        rcvNotification.adapter = mAdapter
    }

    val onItemClick = object : CallBack<NotificationBean.Data.Data>() {
        override fun onSuccess(t: NotificationBean.Data.Data?) {

            if (t != null) {
                t?.notificationId?.let { readNotification(it) }
            }

        }

    }

    private fun readNotification(notificationId: Int) {
        showLoader()
        val hashMap = HashMap<String, Any>()
        hashMap["notification_id"] = notificationId
        val loginRequest = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        loginRequest.updateReadStatus(hashMap).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Log.e("TAG", t.localizedMessage)
                hideLoader()
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                try {
                    hideLoader()
                    Log.d("TAG", " Response")
                    if (response.isSuccessful && response.code() == 200) {
                        if (response.body() == null) {
                            Toaster.somethingWentWrong()
                            return
                        } else {
                            Log.d("Response111", "Response111" + response.body())
                        }
                    } else {
                        val jObjError = JSONObject(response.errorBody()?.string())
                        Toaster.shortToast(jObjError.getString("message"))
                    }
                } catch (e: Exception) {
                    Log.d("onException", e.localizedMessage)
                }
            }
        })

    }

}