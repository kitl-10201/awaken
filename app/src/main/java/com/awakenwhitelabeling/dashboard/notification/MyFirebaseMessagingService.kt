package com.awakenwhitelabeling.dashboard.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.SharedPref
import com.awakenwhitelabeling.others.Cons
import com.awakenwhitelabeling.others.Utils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var FireBaseNotificationIntent: Intent? = null
    var flagNotification: String = ""
    var count: Int? = null
    var boolean: Boolean = false
    private val TAG = "MyFirebaseMessagingServ"
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e(TAG, "onMessageReceived: ${remoteMessage?.toString()}")
        Log.e(TAG, "onMessageReceived: ${remoteMessage?.notification}")

        var number = 0
        if (!remoteMessage?.data["message"].isNullOrEmpty()) {
            boolean = true
            if (boolean) {
                val preCount = SharedPref.get().get<Int>(Cons.MSGCount).toInt()
                SharedPref.get().save(Cons.MSGCount, preCount + number + 1)

            }
            flagNotification = Cons.Message
            FireBaseNotificationIntent = Intent(this, NotificationActivity::class.java)
            FireBaseNotificationIntent!!.putExtra(Cons.Notification, Cons.Notification)
            remoteMessage.notification?.title?.let {
                remoteMessage.notification!!.body?.let { it1 ->
                    SendNotification.sendNotification(
                        this, FireBaseNotificationIntent!!,
                        it, it1, null
                    )
                }
            }
        } else {
            remoteMessage.notification?.let {
                val mGroupId = remoteMessage.data["conversationId"]
                Log.d("fsfsfsfsff", "COnversation ID: " + mGroupId)

                if (!Utils.isEmptyString(it.imageUrl?.toString())) {
                    sendNotification(
                        mGroupId,
                        Html.fromHtml(it.title).toString(),
                        Html.fromHtml(it.body).toString(),
                        null
                    )
                } else {
                    Handler(Looper.getMainLooper()).post {
                        Glide.with(this)
                            .asBitmap()
                            .load(it.imageUrl)
                            .into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(
                                    resource: Bitmap,
                                    transition: Transition<in Bitmap>?
                                ) {
                                    sendNotification(
                                        "",
                                        it.title ?: "Title",
                                        Html.fromHtml(it.body).toString(),
                                        null
                                    )
                                }

                                override fun onLoadFailed(errorDrawable: Drawable?) {
                                    super.onLoadFailed(errorDrawable)
                                    sendNotification(
                                        "", it.title ?: "Title",
                                        it.body ?: "Notification Body",
                                        null
                                    )
                                }
                            });
                    }
                }
            }
        }
    }

    override fun onNewToken(token: String) {
        Log.d("MyFireBaseMessaging", "NewFCM_Token" + token)

    }

    private fun sendNotification(
        mGroupID: String?,
        title: String,
        messageBody: String,
        bitmap: Bitmap?
    ) {


        val intent: Intent = Intent(this, NotificationActivity::class.java)
        intent.putExtra(Cons.Notification, Cons.Notification)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent: PendingIntent? = TaskStackBuilder.create(this).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(intent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val channelId = "com.meevoonlinebooking.notifications"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_app_icon)
            .setContentTitle(title)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setCategory(NotificationCompat.CATEGORY_CALL)
            .setContentIntent(pendingIntent)
        if (bitmap != null) {
            notificationBuilder.setLargeIcon(bitmap).setStyle(
                NotificationCompat.BigPictureStyle()
                    .bigPicture(bitmap)
                    .bigLargeIcon(null)
            )
        }

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Workout Notifications",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.description = "To show latest updates from Meevo"
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    fun interface HandlerMsg<C> {
        fun call(context: C)
    }

}