package com.awakenwhitelabeling.dashboard.notification

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.notification.bean.NotificationBean
import com.awakenwhitelabeling.others.CallBack
import java.text.SimpleDateFormat


class NotificationAdapter(
    val data: ArrayList<NotificationBean.Data.Data>,
    val callBack: CallBack<NotificationBean.Data.Data>
) :
    RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    var selectedPosition = -1
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.notification_list, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var notification = data?.get(position)!!
        downloadMore(position)
        holder.tvTitle.text = notification.notificationTitle
        holder.tvDesc.text = Html.fromHtml(notification.notificationDescription)
        var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var setDateFormat = SimpleDateFormat("dd MMM yyyy hh:mm a");
        holder.tvDate.text =
            setDateFormat.format(getDateFormat.parse(notification.notificationPublishedTime))
        if (notification.readStatus == 0) {

            holder.ivRead_Unread_msg.visibility = View.VISIBLE
//            holder.ll_Notification_Main.setBackgroundColor(context.getColor(R.color.unReadNotification))
            Log.d("Status", "Read/Unread status" + notification.readStatus)
        } else {
            holder.ivRead_Unread_msg.visibility = View.GONE
            //holder.ll_Notification_Main.setBackgroundColor(context.getColor(R.color.white))
            holder.ll_Notification_Main.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )
        }

        holder.ll_Notification_Main.setOnClickListener {
            selectedPosition = position;
            callBack.onSuccess(notification)
            holder.ivRead_Unread_msg.visibility = View.GONE
            holder.ll_Notification_Main.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )
            val alertView: View =
                LayoutInflater.from(context).inflate(R.layout.read_notification_popup, null)
            val alertDialog = AlertDialog.Builder(context)
            alertDialog.setView(alertView)
            var close: ImageView = alertView.findViewById(R.id.ic_close_Notification_Popup)
            var title: TextView = alertView.findViewById(R.id.tvTitlePopup)
            var date: TextView = alertView.findViewById(R.id.tvDatePopup)
            var desc: TextView = alertView.findViewById(R.id.tvDescriptionPopup)
            title.text = notification.notificationTitle
            var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            var setDateFormat = SimpleDateFormat("hh:mm a dd MMM yyyy ");
            date.text =
                setDateFormat.format(getDateFormat.parse(notification.notificationPublishedTime))
            desc.text = Html.fromHtml(notification.notificationDescription)
            alertDialog.setCancelable(false)
            val dialog = alertDialog.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            close.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    interface LoadMoreListener {
        fun onLoadMore()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ll_Notification_Main = itemView.findViewById<LinearLayout>(R.id.ll_Notification_Main)
        var tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
        var tvDesc = itemView.findViewById<TextView>(R.id.tvDescription)
        var tvDate = itemView.findViewById<TextView>(R.id.tvDate)
        var ivRead_Unread_msg = itemView.findViewById<ImageView>(R.id.ivRead_Unread_msg)
    }

    var previousGetCount = 0

    @Synchronized
    private fun downloadMore(position: Int) {
        if (position > data!!.size - 3 && previousGetCount != data.size) {
            // listener?.onLoadMore()
            previousGetCount = data.size
        }
    }
}
