package com.awakenwhitelabeling.dashboard.policy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import com.awakenwhitelabeling.R

class PrivacyPolicyActivity : AppCompatActivity() {

    private lateinit var webView: WebView
    private lateinit var title: TextView
    lateinit var ivBackPraviry: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        webView = findViewById(R.id.privacyPolicy)
        ivBackPraviry = findViewById(R.id.ivPolicyBack)
        title = findViewById(R.id.title)
        val intent = intent
        val name = intent.getStringExtra("Name")
        var tit = intent.getStringExtra("Title")
        title.text = tit.toString()
        webView.settings.setJavaScriptEnabled(true)
        ivBackPraviry.setOnClickListener {
            this.finish()
        }
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null) {
                    view?.loadUrl(url)
                }
                return true
            }
        }
        webView.loadUrl(name.toString())

    }

}