package com.awakenwhitelabeling.dashboard.account

import java.io.Serializable

data class UserDetailsBean(
    val `data`: Data,
    val message: String,
    val status: String
) {
    data class Data(
        val about_yourself: String,
        val active_status: Int,
        val address_line_1: String,
        val address_line_2: String,
        val created_at: String,
        val device_type: String,
        val dob: String,
        val email: String,
        val email_verified_at: String,
        val facebook_link: String,
        val instagram_link: String,
        val website_link: String,
        val fcm_token: String,
        val first_name: String,
        val ibo_number: String,
        val id: Int,
        val image_path: String,
        val last_name: String,
        val last_updated_by: Int,
        val name: String,
        val phone: String,
        val register_type: String,
        val socialLogID: String,
        val state: String,
        val city: String,
        val updated_at: String,
        val zip: String
    ):Serializable
}