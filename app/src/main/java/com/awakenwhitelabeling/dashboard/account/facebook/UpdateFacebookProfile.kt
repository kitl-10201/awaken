package com.awakenwhitelabeling.dashboard.account.facebook

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.*
import com.awakenwhitelabeling.dashboard.DashboardActivity
import com.awakenwhitelabeling.dashboard.other.DashboardHelper
import com.awakenwhitelabeling.others.*
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.android.material.appbar.MaterialToolbar
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.socreates.base.PermissionFragment
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException

import android.app.DatePickerDialog
import android.os.Build
import android.text.InputType
import android.widget.*
import androidx.annotation.RequiresApi
import com.awakenwhitelabeling.dashboard.account.UserDetailsBean

import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.userAction.UserActivity
import java.util.*
import com.google.android.material.textfield.TextInputLayout
import org.json.JSONObject
import java.io.FileNotFoundException


class UpdateFacebookProfile : PermissionFragment(), View.OnClickListener {
    val TAG = "UpdateProfile"

    lateinit var btnProfUpdate: Button

    //  lateinit var progressView: ProgressBar
    lateinit var rlEditProfile: RelativeLayout
    lateinit var tokenServerApi: RetrofitApi
    lateinit var sharedPref: SharedPref
    private val gallery = 1
    private val camera = 2
    lateinit var call: Call<JsonObject>
    lateinit var profileimage: ImageView
    lateinit var profileimageCam: ImageView
    var fil: File? = null
    var email: String? = null
    lateinit var tilFName: TextInputLayout
    lateinit var etFName: EditText
    lateinit var tilLName: TextInputLayout
    lateinit var etLName: EditText

    lateinit var etUPEmail: EditText
    lateinit var tilMobile: TextInputLayout
    lateinit var mobile: EditText
    lateinit var etDOB: EditText
    lateinit var tilIbo: TextInputLayout
    lateinit var etIbo: EditText
    lateinit var tilAddress: TextInputLayout

    lateinit var etAddress1: EditText
    lateinit var etAddress2: EditText
    lateinit var tilState: TextInputLayout

    lateinit var etState: EditText
    lateinit var tilCity: TextInputLayout
    lateinit var etCity: EditText

    lateinit var tilZip: TextInputLayout

    lateinit var etZip: EditText


    companion object {
        private const val IMAGE_DIRECTORY = "/Awaken"
        private const val EXTRA_TITLE = "frag.title"
        private const val EXTRA_API_ENDPOINT = "frag.endpoint"

        // U_E -> URL_ENDPOINT
        private const val U_E_TRAINING = "training_categories"
        private const val U_E_WORKFLOW = "get_tasks"
        private const val U_E_NOTES = "get_user_notes"
        private const val U_E_ACCOUNT = "get_user_data"

        fun UpdatenewProfileInstance(title: String, position: Int): UpdateFacebookProfile {

            return UpdateFacebookProfile()

        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_update_facebook_profile, container, false)
        sharedPref = SharedPref(context)

        return view
    }

    /*override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.elevation = 0f
    }
    */
    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.btnColor))


        activity?.findViewById<MaterialToolbar>(R.id.toolbar)?.setOnClickListener {
            // etFName.focusable=false
        }


        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.elevation = 0F

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.let { DashboardHelper.setToolbar(it) }







        btnProfUpdate = view.findViewById(R.id.btnProfUpdate)
        tilFName = view.findViewById(R.id.tilFName)
        etFName = view.findViewById(R.id.etFName)
        tilLName = view.findViewById(R.id.tilLName)
        tilFName = view.findViewById(R.id.tilFName)
        etLName = view.findViewById(R.id.etLName)
        etUPEmail = view.findViewById(R.id.etFacebookEmail)
        tilMobile = view.findViewById(R.id.tilMobile)
        mobile = view.findViewById(R.id.mobile)
        etDOB = view.findViewById(R.id.etDOB)
        etIbo = view.findViewById(R.id.etIbo)
        tilIbo = view.findViewById(R.id.tilIbo)
        tilAddress = view.findViewById(R.id.tilAddress1)
        etAddress1 = view.findViewById(R.id.etAddress1)
        etAddress2 = view.findViewById(R.id.etAddress2)
        tilState = view.findViewById(R.id.tilState)
        etState = view.findViewById(R.id.etState)
        profileimage = view.findViewById(R.id.profileimage)
        profileimageCam = view.findViewById(R.id.profileimageCam)
        tilZip = view.findViewById(R.id.tilZip)
        etZip = view.findViewById(R.id.etZip)
        tilCity = view.findViewById(R.id.tilCity)
        etCity = view.findViewById(R.id.etCity)
        // progressView = view.findViewById(R.id.progressBar)
        rlEditProfile = view.findViewById(R.id.rlEditProfile)
        getUserInformation()
        profileimage.setOnClickListener(this)
        profileimageCam.setOnClickListener(this)

        parseValue()

        etDOB.setOnClickListener(View.OnClickListener {
            val calendar: Calendar = Calendar.getInstance()
            val yy: Int = calendar.get(Calendar.YEAR)
            val mm: Int = calendar.get(Calendar.MONTH)
            val dd: Int = calendar.get(Calendar.DAY_OF_MONTH)
            val datePicker = DatePickerDialog(
                requireActivity(),
                { view, year, monthOfYear, dayOfMonth ->
                    val date =
                        year.toString() + "-" + (monthOfYear + 1).toString() + "-" + dayOfMonth.toString()

                    etDOB.setText(date)
                }, yy, mm, dd
            )
            datePicker.datePicker.maxDate = System.currentTimeMillis()
            datePicker.show()
        })

        btnProfUpdate.setOnClickListener {
            if (checkValidation()) {
                if (mobile.length() == 0 || mobile.length() == 10) {
                    updateUserProfile()
                } else {
                    tilMobile.error = "Invalid mobile number"
                }
            }
        }
        warnings()

    }


    private fun parseValue() {
        val title = arguments?.getString(UpdateFacebookProfile.EXTRA_TITLE) ?: "Profile"
        DashboardHelper.setTitle(title)
        if (title == "Profile") {
            activity?.findViewById<MaterialToolbar>(R.id.toolbar)
                ?.setTitleTextColor(ResourceUtils.getColor(R.color.white))

            activity?.findViewById<MaterialToolbar>(R.id.toolbar)
                ?.setNavigationIconTint(ResourceUtils.getColor(R.color.white))


        }

    }

    /*  private fun showProgressView() {
          Utils.viewVisible(progressView)
          // Utils.viewGone(rlEditProfile)
      }

      private fun hideProgressView() {
          Utils.viewGone(progressView)
          //.  Utils.viewVisible(rlEditProfile)
      }
  */
    override fun onClick(v: View?) {
        when (v) {

            /* btnProfUpdate -> {
                 updateUserProfile()
             }*/
            profileimage -> {
                if (hasCameraAndStoragePermission()) {
                    showPictureAndCameraPopup()

                }
            }
            profileimageCam -> {
                if (hasCameraAndStoragePermission()) {
                    showPictureAndCameraPopup()
                }
            }
        }
    }

    private fun showPictureAndCameraPopup() {
        showPicturePickerDialog(object : CallBack<Int>() {
            override fun onSuccess(t: Int?) {
                when (t) {
                    1 -> {
                        picturePickerDialog?.hide()
                        takePhotoFromCamera()
                    }
                    2 -> {
                        picturePickerDialog?.hide()

                        choosePhotoFromGallary()

                    }
                }
            }
        })
    }


    private fun getUserInformation() {
        if (!App.get().isConnected()) {
            //  activity?.let { InternetConnectionDialog(it, null).show() }
            popUpInternetConnectionDialog()
            return
        }
        showLoader()
        var userData = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userData.getUserDetails().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()
                Log.e("onFailure", t.localizedMessage)
            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                //  hideProgressView()
                if (response.code() == 200) {
                    var userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                    DataCache.get().userData = userData.data
                    Log.d("DataUserP", "DataUserP\n" + response.body())
                    etFName.setText(userData.data.first_name)
                    etLName.setText(userData.data.last_name)
                    etUPEmail.setText(userData.data.email)
                    mobile.setText(userData.data.phone)
                    etDOB.setText(userData.data.dob)
                    etIbo.setText(userData.data.ibo_number)
                    etAddress1.setText(userData.data.address_line_1)
                    etAddress2.setText(userData.data.address_line_2)
                    etState.setText(userData.data.state)
                    etZip.setText(userData.data.zip)

                    if (!userData.data.email.isNullOrEmpty()) {
                        // Toaster.shortToast("hi")
                        Log.d("dfsdfsdfsdf", "dfsfsddgsdgsdg")
                        etUPEmail.isClickable = false
                        etUPEmail.isFocusable = false
                        etUPEmail.isFocusableInTouchMode = false

                    } else if (userData.data.active_status == 0) {
                        SharedPref.get().clearAll()
                        requireActivity().startActivity(
                            Intent(
                                requireActivity(),
                                UserActivity::class.java
                            )
                        )
                        requireActivity().finishAffinity()
                    } else {
                        etUPEmail.isClickable = true
                        etUPEmail.isFocusable = true
                        etUPEmail.isFocusableInTouchMode = true
                        etUPEmail.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                    }

                    // PicassoUtil.UPImage(profileimage,userData.data.image_path)
                    try {
                        if (userData.data.image_path != null) {
                            PicassoUtil.loadProfileImage(profileimage, userData.data.image_path)
                        }

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }


                } else {
                    val jObjError = JSONObject(response.errorBody()?.string())
                    Toaster.shortToast(
                        jObjError.getString("message")
                    )
                }
            }
        })
    }


    private fun updateUserProfile() {

        if (!App.get().isConnected()) {
            //  activity?.let { InternetConnectionDialog(it, null).show() }
            popUpInternetConnectionDialog()
            return
        }
        // Toaster.shortToast("Data saved")
        showLoader()
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("first_name", etFName.text.toString())
        builder.addFormDataPart("last_name", etLName.text.toString())
        builder.addFormDataPart("email", etUPEmail.text.toString())
        builder.addFormDataPart("phone", mobile.text.toString())
        builder.addFormDataPart("ibo_number", etIbo.text.toString().trim())
        builder.addFormDataPart("device_type", "Android")
        builder.addFormDataPart("dob", etDOB.text.toString())
        builder.addFormDataPart("address_line_1", etAddress1.text.toString().trim())
        builder.addFormDataPart("address_line_2", etAddress2.text.toString().trim())
        builder.addFormDataPart("state", etState.text.toString())
        builder.addFormDataPart("city", etCity.text.toString())
        builder.addFormDataPart("zip", etZip.text.toString())
        builder.addFormDataPart(Cons.LOGIN_TYPE, Cons.NATIVE)
        if (fil != null) {
            builder.addFormDataPart(
                "image",
                fil!!.name,
                RequestBody.create(MediaType.parse("multipart/form-data"), fil)
            )
        }
        val requestBody = builder.build()
        tokenServerApi = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        call = tokenServerApi.updateProfile(requestBody)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                hideLoader()
                ErrorUtils.onFailure(t)
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                Log.e(tag, "response code ${response.code()}")
                Log.e(tag, "on response")
                hideLoader()
                if (response.isSuccessful && response.code() == 200) {
                    etFName.clearFocus()
                    etLName.clearFocus()
                    etIbo.clearFocus()
                    mobile.clearFocus()
                    etDOB.clearFocus()
                    etAddress1.clearFocus()
                    etAddress2.clearFocus()
                    etState.clearFocus()
                    etZip.clearFocus()
                    val userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                    Log.d(TAG, "Profile updated data: " + response.body())
                    Log.e(TAG, "Saved_FCM_TOKEN: : "+userData.data.fcm_token)
                    Toaster.shortToast("Profile updated Successfully")

                    sharedPref.save(Cons.username, userData.data.name)
                    sharedPref.save(Cons.user_email, userData.data.email)
                    sharedPref.save(Cons.logintype, userData.data.socialLogID)
                    sharedPref.save(Cons.id, userData.data.id.toString())
                    sharedPref.save(Cons.image_path, userData.data.image_path)
                    MembershipRequest.checkMembership(object :
                        CallBack<UserMembershipKBean.Data>() {
                        override fun onSuccess(t: UserMembershipKBean.Data?) {
                            hideLoader()
                            if (t == null) {
                                startActivity(Intent(context, MembershipActivity::class.java))
                                activity?.finish()
                                return
                            }
                            if (t?.isValid == "false") {
                                startActivity(Intent(context, MembershipActivity::class.java))
                                activity?.finish()
                            } else {
                                (activity as DashboardActivity).setUpUserDetails()
                            }
                        }

                        override fun onError(error: String?) {
                            super.onError(error)
                            hideLoader()
                            DashboardActivity.startActivity(requireContext())
                        }
                    })

                } else {
                    hideLoader()
                    Toaster.somethingWentWrong()
                }
            }
        })
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, camera)
    }

    private fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, gallery)
    }

    override fun onPermissionGranted(REQUESTED_FOR: Int) {
        showPictureAndCameraPopup()
    }

    override fun onPermissionDisabled(REQUESTED_FOR: Int) {
        Toaster.shortToast("Please enable permission from app settings")
    }

    override fun onPermissionDenied(REQUESTED_FOR: Int) {
        Toaster.shortToast("Please allow STORAGE | CAMERA permission to update profile pic.")
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /*   if (resultCode == this.RESULT_CANCELED) {
               return
           }*/

        if (requestCode == gallery) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(
                        requireActivity().contentResolver,
                        contentURI
                    )
                    fil = File(saveImage(bitmap))
                    profileimage.setImageBitmap(bitmap)

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

        }/* else if (requestCode == camera) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            profileimage.setImageBitmap(thumbnail)
            fil = File(saveImage(thumbnail))
        }*/
        else if (requestCode == camera) {
            if (data != null) {
                try {
                    val thumbnail = data!!.extras!!.get("data") as Bitmap
                    profileimage.setImageBitmap(thumbnail)
                    fil = File(saveImage(thumbnail))
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace();
                } catch (e: IOException) {
                    e.printStackTrace();
                }
            } else {
                Toaster.shortToast("Selection cancel")
            }
        } else {
        }
    }

    private fun warnings() {
        etFName.addTextChangedListener(
            CustomWatcher(
                etFName,
                tilFName,

                "First name Required",
                CustomWatcher.EditTextType.FirstName
            )

        )
        etLName.addTextChangedListener(
            CustomWatcher(
                etLName,
                tilLName,

                "Last name Required",
                CustomWatcher.EditTextType.FirstName
            )

        )

        mobile.addTextChangedListener(
            CustomWatcher(
                mobile,
                tilMobile,

                "",
                CustomWatcher.EditTextType.FirstName
            )
        )

       /* etIbo.addTextChangedListener(
            CustomWatcher(
                etIbo,
                tilIbo,

                "IBO number Required",
                CustomWatcher.EditTextType.FirstName
            )

        )*/

        etAddress1.addTextChangedListener(
            CustomWatcher(
                etAddress1,
                tilAddress,

                "Address Required",
                CustomWatcher.EditTextType.FirstName
            )

        )


        etZip.addTextChangedListener(
            CustomWatcher(
                etZip,
                tilZip,

                "Zip Code Required",
                CustomWatcher.EditTextType.FirstName
            )

        )


    }

    private fun checkValidation(): Boolean {
        var isValid = false
        when {

            Utils.isEmptyString(etFName.text.toString().trim()) -> {
                tilFName.error = "First Name Required"
            }

            Utils.isEmptyString(etLName.text.toString().trim()) -> {
                tilLName.error = "Last Name Required"
            }
            Utils.isEmptyString(etIbo.text.toString()) -> {
                tilIbo.error = "IBO number is required"
            }

            Utils.isEmptyString(etAddress1.text.toString()) -> {
                tilAddress.error = "Address required"
            }

            Utils.isEmptyString(etState.text.toString()) -> {
                tilState.error = "State required"
            }

            Utils.isEmptyString(etCity.text.toString()) -> {
                tilCity.error = "City required"
            }

            Utils.isEmptyString(etZip.text.toString()) -> {
                tilZip.error = "Zip number required"
            }


            else -> {
                isValid = true
            }
        }



        return isValid

    }


}
