package com.awakenwhitelabeling.dashboard.account

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.*
import com.awakenwhitelabeling.dashboard.DashboardActivity
import com.awakenwhitelabeling.dashboard.other.DashboardHelper
import com.awakenwhitelabeling.dashboard.profile.topRanking.UserRankingActivity
import com.awakenwhitelabeling.dashboard.profile.topRanking.UsersRankingAdapter
import com.awakenwhitelabeling.dashboard.profile.topRanking.bean.TopUsersRankinBean
import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.others.*

import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.awakenwhitelabeling.userAction.UserActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.socreates.base.PermissionFragment
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.util.*
import java.util.regex.Pattern


class UpdateProfileFragment : PermissionFragment(), View.OnClickListener {
    val TAG = "UpdateProfile"

    private lateinit var btnProfUpdate: Button

    private lateinit var rlEditProfile: RelativeLayout
    private lateinit var tokenServerApi: RetrofitApi
    private lateinit var sharedPref: SharedPref


    lateinit var call: Call<JsonObject>
    private lateinit var profileimage: ImageView
    private lateinit var profileimageCam: ImageView
    var fil: File? = null

    private val gallery = 1
    private val camera = 2
    private val SELECT_PICTURE = 27
    private val REQUEST_IMAGE_CAPTURE = 250
    private var imageSize = 0.0
    private var currentPhotoPath = ""
    private val imageHelper = ImageHelper()


    var email: String? = null
    private lateinit var tilFName: TextInputLayout
    private lateinit var etFName: EditText
    private lateinit var tilLName: TextInputLayout
    private lateinit var etLName: EditText
    private lateinit var etUPEmail: EditText

    private lateinit var tilFacebook: TextInputLayout
    private lateinit var etUPFacebook: EditText

    private lateinit var tilInstagram: TextInputLayout
    private lateinit var etUPInstagram: EditText

    private lateinit var tilWebsite: TextInputLayout
    private lateinit var etUPWebsite: EditText


    private lateinit var tilMobile: TextInputLayout
    private lateinit var mobile: EditText
    private lateinit var etDOB: EditText
    private lateinit var tilIbo: TextInputLayout
    private lateinit var etIbo: EditText
    private lateinit var tilAddress: TextInputLayout
    private lateinit var etAddress1: EditText
    private lateinit var etAddress2: EditText
    private lateinit var tilState: TextInputLayout
    private lateinit var etState: EditText
    private lateinit var tilCity: TextInputLayout
    private lateinit var etCity: EditText
    private lateinit var tilZip: TextInputLayout
    private lateinit var etZip: EditText
    private lateinit var seeMoreUserRanking: TextView

    lateinit var rcvTopRanking: RecyclerView
    lateinit var layoutManager: GridLayoutManager
    private val list = ArrayList<TopUsersRankinBean.Data>()
    private var mAdapter = UsersRankingAdapter(list)

    companion object {
        private const val EXTRA_TITLE = "frag.title"


        fun UpdatenewProfileInstance(title: String, position: Int): UpdateProfileFragment {

            return UpdateProfileFragment()

        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_updateprofile, container, false)
        sharedPref = SharedPref(context)

        return view
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpdateProfRefrence(view)
        parseValue()
        getUserInformation()
        validateInputType()
        getuserRanking()


    }

    private fun setUpdateProfRefrence(view: View) {

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.btnColor))

        activity?.findViewById<RelativeLayout>(R.id.rlNotification)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.btnColor))

        activity?.findViewById<ImageView>(R.id.notification)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.btnColor))

        activity?.findViewById<ImageView>(R.id.notification)?.visibility = View.GONE
        activity?.findViewById<TextView>(R.id.tvNotification)?.visibility = View.GONE





        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.elevation = 0F

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.let { DashboardHelper.setToolbar(it) }

        rcvTopRanking = view.findViewById(R.id.rcvTopRanking)
        btnProfUpdate = view.findViewById(R.id.btnProfUpdate)
        tilFName = view.findViewById(R.id.tilFName)
        etFName = view.findViewById(R.id.etFName)
        tilLName = view.findViewById(R.id.tilLName)
        tilFName = view.findViewById(R.id.tilFName)
        etLName = view.findViewById(R.id.etLName)
        etUPEmail = view.findViewById(R.id.etUPEmail)

        tilFacebook = view.findViewById(R.id.tilFacebook)
        etUPFacebook = view.findViewById(R.id.etUPFacebook)

        tilInstagram = view.findViewById(R.id.tilInstagram)
        etUPInstagram = view.findViewById(R.id.etUPInstagram)

        tilWebsite = view.findViewById(R.id.tilWebsite)
        etUPWebsite = view.findViewById(R.id.etUPWebsite)



        tilMobile = view.findViewById(R.id.tilMobile)
        mobile = view.findViewById(R.id.mobile)
        etDOB = view.findViewById(R.id.etDOB)
        etIbo = view.findViewById(R.id.etIbo)
        tilIbo = view.findViewById(R.id.tilIbo)
        tilAddress = view.findViewById(R.id.tilAddress1)
        etAddress1 = view.findViewById(R.id.etAddress1)
        etAddress2 = view.findViewById(R.id.etAddress2)
        tilState = view.findViewById(R.id.tilState)
        etState = view.findViewById(R.id.etState)
        profileimage = view.findViewById(R.id.profileimage)
        profileimageCam = view.findViewById(R.id.profileimageCam)
        tilZip = view.findViewById(R.id.tilZip)
        etZip = view.findViewById(R.id.etZip)
        tilCity = view.findViewById(R.id.tilCity)
        etCity = view.findViewById(R.id.etCity)
        seeMoreUserRanking = view.findViewById(R.id.seeMoreUserRanking)
        // progressView = view.findViewById(R.id.progressBar)
        rlEditProfile = view.findViewById(R.id.rlEditProfile)
        // getUserInformation()

        seeMoreUserRanking.setOnClickListener {
            startActivity(Intent(requireActivity(), UserRankingActivity::class.java))

        }

        btnProfUpdate.setOnClickListener {
            if (checkValidation()) {
                if (mobile.length() >= 4 || mobile.length() == 15) {
                    updateUserProfile()
                } else {
                    tilMobile.error = "Invalid mobile number"
                }
            }
        }

        profileimage.setOnClickListener(this)
        profileimageCam.setOnClickListener(this)


        etDOB.setOnClickListener(View.OnClickListener {
            val calendar: Calendar = Calendar.getInstance()
            val yy: Int = calendar.get(Calendar.YEAR)
            val mm: Int = calendar.get(Calendar.MONTH)
            val dd: Int = calendar.get(Calendar.DAY_OF_MONTH)
            val datePicker = DatePickerDialog(
                requireActivity(),
                { view, year, monthOfYear, dayOfMonth ->
                    val date =
                        year.toString() + "-" + (monthOfYear + 1).toString() + "-" + dayOfMonth.toString()

                    etDOB.setText(date)
                }, yy, mm, dd
            )
            datePicker.datePicker.maxDate = System.currentTimeMillis()
            datePicker.show()
        })




        layoutManager = GridLayoutManager(activity, 1, LinearLayoutManager.HORIZONTAL, false);
        rcvTopRanking.layoutManager = layoutManager
        mAdapter = UsersRankingAdapter(list)
        rcvTopRanking.adapter = mAdapter

    }

    override fun onResume() {
        super.onResume()
        getuserRanking()
        // setAdapter()
    }

    private fun setAdapter() {

        rcvTopRanking.layoutManager = layoutManager
        mAdapter = UsersRankingAdapter(list)
        rcvTopRanking.adapter = mAdapter

    }


    private fun parseValue() {
        val title = arguments?.getString(UpdateProfileFragment.EXTRA_TITLE) ?: "Profile"
        DashboardHelper.setTitle(title)
        if (title == "Profile") {
            activity?.findViewById<MaterialToolbar>(R.id.toolbar)
                ?.setTitleTextColor(ResourceUtils.getColor(R.color.white))
            activity?.findViewById<MaterialToolbar>(R.id.toolbar)
                ?.setNavigationIconTint(ResourceUtils.getColor(R.color.white))

        }

    }

    override fun onClick(v: View?) {
        when (v) {

            /* btnProfUpdate -> {
                 updateUserProfile()
             }*/
            profileimage -> {
                if (hasCameraAndStoragePermission()) {
                    showPictureAndCameraPopup()
                }
            }
            profileimageCam -> {
                if (hasCameraAndStoragePermission()) {
                    showPictureAndCameraPopup()
                }
            }
        }
    }

    private fun showPictureAndCameraPopup() {
        showPicturePickerDialog(object : CallBack<Int>() {
            override fun onSuccess(t: Int?) {
                when (t) {
                    1 -> {
                        picturePickerDialog?.hide()
                        takePhotoFromCamera()
                    }
                    2 -> {
                        picturePickerDialog?.hide()

                        takePhotoFromGallery()

                    }
                }
            }
        })
    }

    //Setting existing user details.........
    private fun getUserInformation() {

        showLoader()
        if (!App.get().isConnected()) {
            popUpInternetConnectionDialog()
            hideLoader()
            return
        }
        var userData = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userData.getUserDetails().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()

                Log.e("onFailure", t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                if (response.code() == 200) {
                    var userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                    DataCache.get().userData = userData.data

                    Log.d("DataUserP", "DataUserP\n" + response.body())
                    Log.e("Saved_User_FCM_Token", "\n" + userData.data.fcm_token)


                    setUserDataIntoForm(userData)
                    // PicassoUtil.UPImage(profileimage,userData.data.image_path)
                    try {
                        if (userData.data.image_path != null) {
                            PicassoUtil.loadProfileImage(profileimage, userData.data.image_path)
                        }

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                } else {
                    val jObjError = JSONObject(response.errorBody()?.string())
                    Toaster.shortToast(jObjError.getString("message"))
                }
            }
        })

    }


    // Updating existing user details........
    private fun updateUserProfile() {
        showLoader()
        if (!App.get().isConnected()) {
            //  activity?.let { InternetConnectionDialog(it, null).show() }
            popUpInternetConnectionDialog()
            hideLoader()
            return
        }

        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("first_name", etFName.text.toString())
        builder.addFormDataPart("last_name", etLName.text.toString())
        builder.addFormDataPart("email", etUPEmail.text.toString())
        builder.addFormDataPart("facebook_link", etUPFacebook.text.toString())
        builder.addFormDataPart("instagram_link", etUPInstagram.text.toString())
        builder.addFormDataPart("website_link", etUPWebsite.text.toString())

        builder.addFormDataPart("phone", mobile.text.toString())
        builder.addFormDataPart("ibo_number", etIbo.text.toString().trim())
        builder.addFormDataPart("device_type", "Android")
        builder.addFormDataPart("dob", etDOB.text.toString())
        builder.addFormDataPart("address_line_1", etAddress1.text.toString().trim())
        builder.addFormDataPart("address_line_2", etAddress2.text.toString().trim())
        builder.addFormDataPart("state", etState.text.toString())
        builder.addFormDataPart("city", etCity.text.toString())
        builder.addFormDataPart("zip", etZip.text.toString())
        builder.addFormDataPart(Cons.LOGIN_TYPE, Cons.NATIVE)
        if (fil != null) {
            builder.addFormDataPart(
                "image",
                fil!!.name,
                RequestBody.create(MediaType.parse("multipart/form-data"), fil)
            )
        }
        val requestBody = builder.build()
        tokenServerApi = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        call = tokenServerApi.updateProfile(requestBody)

        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                hideLoader()
                ErrorUtils.onFailure(t)
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                hideLoader()
                Log.e(tag, "response code ${response.code()}")
                Log.e(tag, "on response")
                hideLoader()
                if (response.isSuccessful && response.code() == 200) {
                    val userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                    Toaster.shortToast("Profile updated Successfully")
                    Log.d(TAG, "Data" + response.body())

                    setUpdatedDataintoSharedPref(userData)

                    MembershipRequest.checkMembership(object :
                        CallBack<UserMembershipKBean.Data>() {
                        override fun onSuccess(t: UserMembershipKBean.Data?) {
                            hideLoader()
                            /*if (t == null) {
                                startActivity(Intent(context, MembershipActivity::class.java))
                                activity?.finish()
                                return
                            }*/
                            if (t?.isValid == "false") {
                                startActivity(Intent(context, MembershipActivity::class.java))
                                activity?.finish()
                            } else {
                                /*try {
                                    (activity as DashboardActivity).setUpUserDetails()
                                } catch (e: Exception) {
                                    (activity as DashboardActivity).setUpUserDetails()
                                    Log.d("DashBoardActivity", "exception" + e.localizedMessage)
                                }*/
                            }

                        }

                        override fun onError(error: String?) {
                            super.onError(error)
                            hideLoader()
                            DashboardActivity.startActivity(requireContext())
                        }
                    })

                } else {
                    hideLoader()
                    val jObjError = JSONObject(response.errorBody()?.string())
                    Toaster.shortToast(jObjError.getString("message"))

                }


            }
        })
    }

    private fun getuserRanking() {
        if (!App.get().isConnected()) {
            popUpInternetConnectionDialog()
            hideLoader()
            return
        }

        var userData = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userData.topUserRanking().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()

                Log.e("onFailure", t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                if (response.code() == 200) {
                    Log.e("TopUserRanking", response.body()?.size().toString())
                    val userData = Gson().fromJson(response.body(), TopUsersRankinBean::class.java)
                    list.clear()
                    list.addAll(userData.data)

                    rcvTopRanking.layoutManager = layoutManager
                    mAdapter = UsersRankingAdapter(list)
                    rcvTopRanking.adapter = mAdapter


                } else if (response.code() == 401 || response.code() == 404) {
                    Log.e("Page not found", "Page not found exception occuer in top ranking users.")
                } else {


                    Toaster.somethingWentWrong()
                }
            }
        })
    }

    // Fetching user data into Edittext(Form)
    private fun setUserDataIntoForm(userData: UserDetailsBean?) {
        etFName.setText(userData?.data?.first_name)
        etLName.setText(userData?.data?.last_name)
        etUPEmail.setText(userData?.data?.email)
        etUPFacebook.setText(userData?.data?.facebook_link)
        etUPInstagram.setText(userData?.data?.instagram_link)
        etUPWebsite.setText(userData?.data?.website_link)
        mobile.setText(userData?.data?.phone)
        etDOB.setText(userData?.data?.dob)
        etIbo.setText(userData?.data?.ibo_number)
        etAddress1.setText(userData?.data?.address_line_1)
        etAddress2.setText(userData?.data?.address_line_2)
        etCity.setText(userData?.data?.city)
        etState.setText(userData?.data?.state)
        etZip.setText(userData?.data?.zip)
        if (userData?.data?.email?.length != 0) {

            etUPEmail.isClickable = false
            etUPEmail.isFocusable = false
            etUPEmail.isFocusableInTouchMode = false

        } else if (userData.data.active_status == 0) {
            SharedPref.get().clearAll()
            requireActivity().startActivity(
                Intent(
                    requireActivity(),
                    UserActivity::class.java
                )
            )
            requireActivity().finishAffinity()
        } else {
            etUPEmail.isClickable = true
            etUPEmail.isFocusable = true
            etUPEmail.isFocusableInTouchMode = true
            etUPEmail.inputType = InputType.TYPE_NULL
        }


    }

    //   focusable false and storing data into sharedPref........ 
    private fun setUpdatedDataintoSharedPref(userData: UserDetailsBean?) {

        etFName.clearFocus()
        etLName.clearFocus()
        etIbo.clearFocus()
        mobile.clearFocus()
        etDOB.clearFocus()
        etAddress1.clearFocus()
        etAddress2.clearFocus()
        etState.clearFocus()
        etZip.clearFocus()
        etCity.clearFocus()
        sharedPref.save(Cons.username, userData?.data?.name)
        sharedPref.save(Cons.user_email, userData?.data?.email)
        sharedPref.save(Cons.logintype, userData?.data?.socialLogID)
        sharedPref.save(Cons.id, userData?.data?.id.toString())
        sharedPref.save(Cons.image_path, userData?.data?.image_path)


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK) {
            val f = File(currentPhotoPath)
            imageSize = imageHelper.compressImage(f.absolutePath, 2.0)
            setImage(profileimage, f.absolutePath)
        } else if (requestCode == SELECT_PICTURE && resultCode == AppCompatActivity.RESULT_OK) {
            val selectedImageUri: Uri? = data?.data
            if (selectedImageUri != null) {
                imageHelper.uriToFile(requireContext(), selectedImageUri).let { file ->
                    imageSize = imageHelper.compressImage(file!!.absolutePath, 2.0)
                    setImage(profileimage, file.absolutePath)
                }
            }
        } else {
            Toaster.shortToast("Selection cancel.")
        }
    }


    private fun takePhotoFromGallery() {
/*
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, SELECT_PICTURE)

*/

        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, SELECT_PICTURE)

    }

    private fun takePhotoFromCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = imageHelper.createImageFile(requireActivity())
                currentPhotoPath = photoFile.absolutePath
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(
                    requireContext(),
                    "com.awaken.android.fileprovider",
                    photoFile
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }

        }
    }


    private fun setImage(imageView: ImageView, filePath: String) {
        fil = File(filePath)
        Glide.with(imageView.context).asBitmap().load(filePath).skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView)
    }


    // Requesting permission .......
    override fun onPermissionGranted(REQUESTED_FOR: Int) {
        showPictureAndCameraPopup()
    }

    override fun onPermissionDisabled(REQUESTED_FOR: Int) {
        Toaster.shortToast("Please enable permission from app settings")
    }

    override fun onPermissionDenied(REQUESTED_FOR: Int) {
        Toaster.shortToast("Please allow STORAGE | CAMERA permission to update profile pic.")
    }

    private fun validateInputType() {
        etFName.addTextChangedListener(
            CustomWatcher(
                etFName,
                tilFName,

                "First name Required",
                CustomWatcher.EditTextType.FirstName
            )
        )
        etLName.addTextChangedListener(
            CustomWatcher(
                etLName,
                tilLName,
                "Last name Required",
                CustomWatcher.EditTextType.FirstName
            )
        )
        etUPFacebook.addTextChangedListener(
            CustomWatcher(
                etUPFacebook,
                tilFacebook,
                "Please enter valid URL",
                CustomWatcher.EditTextType.FaceBook
            )
        )


        etUPInstagram.addTextChangedListener(
            CustomWatcher(
                etUPInstagram,
                tilInstagram,
                "Please enter valid URL",
                CustomWatcher.EditTextType.FaceBook
            )
        )




        etUPWebsite.addTextChangedListener(
            CustomWatcher(
                etUPWebsite,
                tilWebsite,
                "Please enter valid URL",
                CustomWatcher.EditTextType.FaceBook
            )
        )











        mobile.addTextChangedListener(
            CustomWatcher(
                mobile,
                tilMobile,
                "",
                CustomWatcher.EditTextType.FirstName
            )
        )


        /*  etIbo.addTextChangedListener(
              CustomWatcher(
                  etIbo,
                  tilIbo,

                  "IBO number Required",
                  CustomWatcher.EditTextType.FirstName
              )

          )*/

        etAddress1.addTextChangedListener(
            CustomWatcher(
                etAddress1,
                tilAddress,

                "Address Required",
                CustomWatcher.EditTextType.FirstName
            )
        )



        etCity.addTextChangedListener(
            CustomWatcher(
                etCity,
                tilCity,
                "City Required",
                CustomWatcher.EditTextType.FirstName
            )
        )





        etZip.addTextChangedListener(
            CustomWatcher(
                etZip,
                tilZip,
                "Zip Code Required",
                CustomWatcher.EditTextType.FirstName
            )

        )


        etState.addTextChangedListener(
            CustomWatcher(
                etState,
                tilState,
                "State Required",
                CustomWatcher.EditTextType.FirstName
            )

        )


    }


    private fun isValidUrl(url: String): Boolean {
        val urlPattern = Pattern
            .compile("^((https?:\\/\\/(www)?)|(www)).+(\\..{2,4})(\\/)?.+$")
        val urlMatcher = urlPattern.matcher(url)
        return urlMatcher.matches()
    }


    private fun checkValidation(): Boolean {
        var isValid = false
        when {

            Utils.isEmptyString(etFName.text.toString().trim()) -> {
                tilFName.error = "First Name Required"
            }

            Utils.isEmptyString(etLName.text.toString().trim()) -> {
                tilLName.error = "Last Name Required"
            }


/*
            URLUtil.isHttpsUrl(etUPFacebook.text.toString().trim()) || URLUtil.isHttpUrl(
                etUPFacebook.text.toString().trim()
            ) -> {
                tilFacebook.error = "Please enter valid facebook URL"
            }

            URLUtil.isHttpsUrl(etUPInstagram.text.toString().trim()) || URLUtil.isHttpUrl(
                etUPInstagram.text.toString().trim()
            ) -> {
                tilInstagram.error = "Please enter valid instagram URL"
            }

            URLUtil.isHttpsUrl(etUPWebsite.text.toString().trim()) || URLUtil.isHttpUrl(
                etUPWebsite.text.toString().trim()
            ) -> {
                tilWebsite.error = "Please enter valid website URL"
            }*/





            Utils.isEmptyString(etState.text.toString()) -> {
                tilState.error = "State required"
            }

            Utils.isEmptyString(etCity.text.toString()) -> {
                tilCity.error = "City required"
            }

            Utils.isEmptyString(etZip.text.toString()) -> {
                tilZip.error = "Zip number required"
            }

            else -> {
                isValid = true
            }
        }



        return isValid

    }


}


