package com.awakenwhitelabeling.dashboard.account

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity
import com.awakenwhitelabeling.base.SharedPref
import com.awakenwhitelabeling.dashboard.account.facebook.UpdateFacebookProfile
import com.awakenwhitelabeling.dashboard.other.DashboardHelper
import com.awakenwhitelabeling.userAction.UserActivity
import com.google.android.material.appbar.MaterialToolbar

class UpdateProfileActivity:BaseActivity() {
    lateinit var toolbar: MaterialToolbar

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, UpdateProfileActivity::class.java))
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_updateprofile)

        toolbar = findViewById(R.id.toolbar)
        /*toolbar.setNavigationOnClickListener {
            Toaster.shortToast("Profile clicked")
            Log.d("fddfkhsjdfksh","dfsdfsdf")
        }*/
        DashboardHelper.setToolbar(toolbar)
//        toolbar.setTitleTextAppearance(this,)
        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener {
            SharedPref.get().clearAll()
            this.startActivity(Intent(this, UserActivity::class.java))
            this.finishAffinity()
        }
        supportFragmentManager.beginTransaction()
            .add(R.id.flUpdate, UpdateFacebookProfile())
            .commit()
    }
    override fun onBackPressed() {
        super.onBackPressed()
        SharedPref.get().clearAll()
        this.startActivity(Intent(this, UserActivity::class.java))
        this.finishAffinity()    }
}