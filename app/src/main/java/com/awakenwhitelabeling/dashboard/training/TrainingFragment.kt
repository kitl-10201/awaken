package com.awakenwhitelabeling.dashboard.training

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.*
import com.awakenwhitelabeling.dashboard.other.DashboardHelper
import com.awakenwhitelabeling.dashboard.training.bean.TrainingListBean
import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.ResourceUtils
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.android.material.appbar.MaterialToolbar
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class TrainingFragment : BaseFragment(), WorkoutAdapter.LoadMoreListener,
    SwipeRefreshLayout.OnRefreshListener {

    // lateinit var progressView: ProgressBar
    private lateinit var main: LinearLayout
    private var newWorkoutData = ArrayList<TrainingListBean.Data.Data>()
    private lateinit var newWorkouts: RecyclerView
    private lateinit var apiEndPoint: String
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var pageNumber = 1
    private lateinit var workoutAdapter: WorkoutAdapter
    private lateinit var tvNoData: TextView

    companion object {
        private const val EXTRA_TITLE = "frag.title"
        private const val EXTRA_API_ENDPOINT = "frag.endpoint"

        // U_E -> URL_ENDPOINT
        private const val U_E_TRAINING = "training_categories"
        private const val U_E_WORKFLOW = "get_tasks"
        private const val U_E_NOTES = "get_user_notes"
        private const val U_E_ACCOUNT = "get_user_data"

        fun newInstance(title: String, position: Int): TrainingFragment {
            val args = Bundle()
            args.putString(EXTRA_TITLE, title)
            val endPoint = when (position) {
                0 -> U_E_TRAINING
                1 -> U_E_WORKFLOW
                2 -> U_E_NOTES
                else -> U_E_ACCOUNT
            }
            args.putString(EXTRA_TITLE, title)
            args.putString(EXTRA_API_ENDPOINT, endPoint.toString())

            val fragment = TrainingFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onResume() {
        super.onResume()

        checkMembershipStatus()

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_training, container, false)
        //  progressView = view.findViewById(R.id.progressBar)


        setReferences(view)



        return view
    }

    private fun setReferences(view: View?) {


        newWorkouts = view?.findViewById(R.id.rvWorkouts)!!
        tvNoData = view.findViewById(R.id.tvNoData)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setOnRefreshListener(this)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))

        activity?.findViewById<RelativeLayout>(R.id.rlNotification)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))


        activity?.findViewById<ImageView>(R.id.notification)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))



        activity?.findViewById<ImageView>(R.id.notification)?.visibility = View.VISIBLE
        activity?.findViewById<TextView>(R.id.tvNotification)?.visibility = View.VISIBLE


        activity?.findViewById<RelativeLayout>(R.id.rlNotification)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))





      //

        parseValue()
        setAdapterToList()
        getNewWorkouts()

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setTitleTextColor(ResourceUtils.getColor(R.color.black))

        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setNavigationIconTint(ResourceUtils.getColor(R.color.black))

    }
    private fun checkMembershipStatus() {
        MembershipRequest.checkMembership(object : CallBack<UserMembershipKBean.Data>() {
            override fun onSuccess(t: UserMembershipKBean.Data?) {
                if (t == null) {
                    startActivity(Intent(context, MembershipActivity::class.java))
                    activity?.finish()
                    return
                }
                if (t?.isValid == "false") {

                    val customDialog = Dialog(requireActivity())
                    customDialog.setContentView(R.layout.dialog_membership_expired)
                    customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                    customDialog.window?.setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    val btnOkAN = customDialog.findViewById(R.id.tvUpdatePlan) as Button
                    var dateValidTil = customDialog.findViewById(R.id.validDate) as TextView
                    var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    var setDateFormat = SimpleDateFormat("MMM d, yy");
                    //  var setTimeFormat = SimpleDateFormat("hh:mm a");

                    val text = setDateFormat.format(getDateFormat.parse(t?.validTo))
                    dateValidTil.text = text + " (EST)"
                    customDialog.setCancelable(false)
                    btnOkAN.setOnClickListener {
                        val intent = Intent(context, MembershipActivity::class.java)
                        startActivity(intent)

                    }
                    customDialog.show()
                     // validityExpire()

                } else {
/*                    MembershipRequest.saveMembershipValidInToLocal()
                    DashboardActivity.startActivity(requireContext())*/
                }
            }
        })
    }

    private fun parseValue() {
        val title = arguments?.getString(EXTRA_TITLE) ?: "Trainings"
        apiEndPoint = arguments?.getString(EXTRA_API_ENDPOINT) ?: U_E_TRAINING
        DashboardHelper.setTitle(title)
    }

    private fun getNewWorkouts() {
        showLoader()

        if (!App.get().isConnected()) {
            popUpInternetConnectionDialog()
            hideLoader()
            return
        }
        val newWorkoutCall = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        newWorkoutCall.getWorkout(pageNumber).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                Toaster.shortToast("Server not responding")
                //   hideProgressView()
                hideLoader()

                if (swipeRefreshLayout.isRefreshing) {

                    swipeRefreshLayout.isRefreshing = false
                }
                Log.e("onFailure", t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                // hideProgressView()
                if (swipeRefreshLayout.isRefreshing) {
                    swipeRefreshLayout.isRefreshing = false
                }
                if (response.isSuccessful) {
                    if (response.body() == null) {
                        Toaster.shortToast("No item available")
                        Log.d("Training", "TotalResponse " + response.body())
                    }

                    try {
                        val newWorkoutsInfo =
                            Gson().fromJson(response.body(), TrainingListBean::class.java)
                        newWorkoutData.clear()
                        newWorkoutData.addAll(newWorkoutsInfo.data.data)
                        workoutAdapter.notifyDataSetChanged()
                    } catch (e: Exception) {
                        Log.e("NewWorkouts ", e.localizedMessage);
                    }
                } else {
                    hideLoader()
                    val jObjError = JSONObject(response.errorBody()?.string())
                    Toaster.shortToast(
                        jObjError.getString("message")
                    )
                }

            }
        })
    }

    private fun setAdapterToList() {
        workoutAdapter = WorkoutAdapter(newWorkoutData, this)
        newWorkouts.adapter = workoutAdapter
    }

    override fun onLoadMore() {
        pageNumber += 1
        val newWorkoutCall = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)

        newWorkoutCall.getWorkout(pageNumber).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                pageNumber -= 1
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                try {
                    if (response.isSuccessful && response.code() == 200) {
                        val data =
                            Gson().fromJson(response.body(), TrainingListBean::class.java)
                        if (data?.data?.data?.size != 0) {

                            newWorkoutData.addAll(data.data.data)
                            workoutAdapter.notifyDataSetChanged()
                        } else {
                            pageNumber -= 1
                        }

                    } else {
                        pageNumber -= 1
                    }

                } catch (e: Exception) {
                }
            }
        })

    }

    override fun onRefresh() {
        pageNumber = 1
        getNewWorkouts()
    }


    override fun onDestroy() {
        super.onDestroy()
        hideLoader()
        hideDialogForFragment()
    }

}

