package com.awakenwhitelabeling.dashboard.training

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.training.bean.TrainingListBean
import com.awakenwhitelabeling.dashboard.workoutdetails.workoutType.TrainingDetailsActivity
import com.awakenwhitelabeling.others.PicassoUtil

class WorkoutAdapter(
    val data: ArrayList<TrainingListBean.Data.Data>,
    trainingFragment: TrainingFragment
) :
    RecyclerView.Adapter<WorkoutAdapter.ViewHolder>() {
    var listener: LoadMoreListener = trainingFragment

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_training_list, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var workout = data?.get(position)!!
        downloadMore(position)
        holder.workoutName.text = workout.category_name
        holder.countVideos.text = workout.count.toString() + " Videos"
        /* if (workout.workoutSessionsCount <= 0) {
             holder.workoutDesc.text = "--";
         } else {
             holder.workoutDesc.text = String.format("%s sessions", workout.workoutDescription)
         }*/
        PicassoUtil.loadImage(holder.workoutImage, workout.category_featured_image)

        holder.main.setOnClickListener {
            //Toaster.shortToast("Well, trying to fixed upcoming issues!")
            TrainingDetailsActivity.startForWorkout(holder.itemView.context, workout)
        }
    }

    interface LoadMoreListener {
        fun onLoadMore()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var main = itemView.findViewById<CardView>(R.id.cvWorkoutMain)
        var workoutImage = itemView.findViewById<ImageView>(R.id.ivNewWorkouts)
        var workoutName = itemView.findViewById<TextView>(R.id.tvWorkoutName)
        var countVideos = itemView.findViewById<TextView>(R.id.tvCount)

    }

    var previousGetCount = 0

    @Synchronized
    private fun downloadMore(position: Int) {
        if (position > data!!.size - 3 && previousGetCount != data.size) {
            listener?.onLoadMore()
            previousGetCount = data.size

        }
    }

}