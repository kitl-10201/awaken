package com.awakenwhitelabeling.dashboard.training.bean

import java.io.Serializable

data class TrainingListBean(
    val `data`: Data,
    val message: String,
    val status: String
) {
    data class Data(
        val current_page: Int,
        val `data`: List<Data>,
        val first_page_url: String,
        val from: Int,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String,
        val path: String,
        val per_page: Int,
        val prev_page_url: Any,
        val to: Int,
        val total: Int
    ) {
        data class Data(
            val active_status: Int,
            val category_description: String,
            val category_featured_image: String,
            val category_name: String,
            val category_slug: String,
            val count: Int,
            val created_at: String,
            val id: Int,
            val updated_at: String
        ):Serializable
    }
}