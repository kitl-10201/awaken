package com.awakenwhitelabeling.dashboard.notes.noteDetails.notesUpdate.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R

class UpdateCustomAdapter(private val userList: List<UpdateCustomBean>) : RecyclerView.Adapter<UpdateCustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpdateCustomAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.selecte_multiple_image_from_gallery, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: UpdateCustomAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }
    override fun getItemCount(): Int {
        return userList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(user: UpdateCustomBean) {
            val ivSelectedImage  = itemView.findViewById(R.id.ivSelectedImage) as ImageView
            ivSelectedImage.setImageResource(user.img)
        }
    }
}
