package com.awakenwhitelabeling.dashboard.notes

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.*
import com.awakenwhitelabeling.dashboard.notes.adapter.NotesAdapter
import com.awakenwhitelabeling.dashboard.other.DashboardHelper
import com.awakenwhitelabeling.membership.MembershipActivity
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.ResourceUtils
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class NotesFragment : BaseFragment(), NotesAdapter.LoadMoreListener,
    SwipeRefreshLayout.OnRefreshListener {
    private lateinit var fbAdd: FloatingActionButton
    private lateinit var progressView: ProgressBar
    private lateinit var main: LinearLayout
    private lateinit var mAdapter: NotesAdapter
    private lateinit var rvNotes: RecyclerView
    var newWorkoutData = ArrayList<NotesBean.Data.Data>()
    private lateinit var tvNoData: TextView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var layoutManager: GridLayoutManager
    var pageNumber = 1

    companion object {
        private const val EXTRA_TITLE = "frag.title"
        private const val EXTRA_API_ENDPOINT = "frag.endpoint"

        // U_E -> URL_ENDPOINT
        private const val U_E_TRAINING = "training_categories"
        private const val U_E_WORKFLOW = "get_tasks"
        private const val U_E_NOTES = "get_user_notes"
        private const val U_E_ACCOUNT = "get_user_data"

        fun newInstance(title: String, position: Int): NotesFragment {
            val args = Bundle()
            args.putString(EXTRA_TITLE, title)

            val endPoint = when (position) {
                0 -> U_E_TRAINING
                1 -> U_E_WORKFLOW
                2 -> U_E_NOTES
                else -> U_E_ACCOUNT
            }
            args.putString(EXTRA_TITLE, title)
            args.putString(EXTRA_API_ENDPOINT, endPoint.toString())

            val fragment = NotesFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notes, container, false)
    }

    @SuppressLint("WrongConstant")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setNotesRefrence(view)
        setAdapterToList()
        showCreatedNotesList()
        checkMembershipStatus()
    }


    private fun setNotesRefrence(view: View) {


        activity?.findViewById<ImageView>(R.id.notification)?.visibility = View.GONE
        activity?.findViewById<TextView>(R.id.tvNotification)?.visibility = View.GONE


        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setTitleTextColor(ResourceUtils.getColor(R.color.black))
        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setNavigationIconTint(ResourceUtils.getColor(R.color.black))
        activity?.findViewById<ImageView>(R.id.notification)?.visibility = View.GONE
        tvNoData = view.findViewById(R.id.tvNoData)

        //progressView = view.findViewById(R.id.progressBar_Notes)
        rvNotes = view.findViewById(R.id.rvNotes)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setOnRefreshListener(this)
        rvNotes.layoutManager = GridLayoutManager(requireContext(), 2, LinearLayout.VERTICAL, false)
        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))
        fbAdd = view.findViewById(R.id.fbAdd)
        parseValue()
        fbAdd.setOnClickListener {
            context?.startActivity(
                Intent(context, NotesActivity::class.java)
            )
        }
    }

    override fun onResume() {
        super.onResume()
        showLoader()
        showCreatedNotesList()
        activity?.findViewById<MaterialToolbar>(R.id.toolbar)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))

        activity?.findViewById<RelativeLayout>(R.id.rlNotification)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))

        activity?.findViewById<ImageView>(R.id.notification)
            ?.setBackgroundColor(ResourceUtils.getColor(R.color.white))


        activity?.findViewById<ImageView>(R.id.notification)?.visibility = View.GONE
        activity?.findViewById<TextView>(R.id.tvNotification)?.visibility = View.GONE

    }

    private fun parseValue() {
        val title = arguments?.getString(NotesFragment.EXTRA_TITLE) ?: "Notes"
        //  apiEndPoint = arguments?.getString(DailyWorkFlowFragment.EXTRA_API_ENDPOINT) ?: DailyWorkFlowFragment.U_E_TRAINING
        DashboardHelper.setTitle(title)
    }

/*
    private fun showProgressView() {
        Utils.viewVisible(progressView)
        Utils.viewGone(rvNotes)
    }

    private fun hideProgressView() {
        Utils.viewGone(progressView)
        Utils.viewVisible(rvNotes)

    }
*/

    private fun showCreatedNotesList() {
        if (!App.get().isConnected()) {
            //  activity?.let { InternetConnectionDialog(it, null).show() }
            popUpInternetConnectionDialog()
            // hideProgressView()
            return
        }

        val newWorkoutCall = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        newWorkoutCall.getNotes(pageNumber).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()
                if (swipeRefreshLayout.isRefreshing) {
                    swipeRefreshLayout.isRefreshing = false
                }
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                // hideProgressView()
                if (swipeRefreshLayout.isRefreshing) {
                    swipeRefreshLayout.isRefreshing = false
                }
                if (response.isSuccessful && response.code() == 200) {
                    if (response.body() == null) {
                        Toaster.shortToast("Error in API" + response.body())
                        return
                    }
                    try {
                        val newNotesInfo = Gson().fromJson(response.body(), NotesBean::class.java)

                        newWorkoutData.clear()
                        newWorkoutData.addAll(newNotesInfo.data.data)
                        if (newWorkoutData.isEmpty())
                            mAdapter.notifyDataSetChanged()
                        if (mAdapter!!.itemCount != 0) {
                            rvNotes.adapter = mAdapter
                            Utils.viewGone(tvNoData)
                        } else {
                            Utils.viewVisible(tvNoData)
                            tvNoData.text="Notes not created yet. Please create your first note to start."
                            rvNotes.adapter = mAdapter
                        }
                    } catch (e: Exception) {
                        Log.e("NewWorkouts ", e.localizedMessage);
                    }
                } else {
                    //  hideProgressView()
                    hideLoader()
                    val jObjError = JSONObject(response.errorBody()?.string())
                    Toaster.shortToast(jObjError.getString("message"))
                }
            }
        })
    }

    private fun setAdapterToList() {
        mAdapter = NotesAdapter(newWorkoutData, this)
        rvNotes.adapter = mAdapter
    }

    override fun onLoadMore() {
        pageNumber += 1
        val newWorkoutCall = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        newWorkoutCall.getNotes(pageNumber).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                pageNumber -= 1
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                try {
                    if (response.isSuccessful && response.code() == 200) {
                        val newNotesInfo = Gson().fromJson(response.body(), NotesBean::class.java)
                        if (newNotesInfo?.data?.data?.size != 0) {
                            newWorkoutData.addAll(newNotesInfo.data.data)
                            mAdapter.notifyDataSetChanged()
                        } else {
                            pageNumber -= 1
                        }
                    } else {
                        pageNumber -= 1
                    }
                } catch (e: Exception) {
                }
            }
        })
    }

    override fun onRefresh() {
        pageNumber = 1
        showCreatedNotesList()
    }

    private fun checkMembershipStatus() {
        MembershipRequest.checkMembership(object : CallBack<UserMembershipKBean.Data>() {
            override fun onSuccess(t: UserMembershipKBean.Data?) {
                if (t == null) {
                    startActivity(Intent(context, MembershipActivity::class.java))
                    activity?.finish()
                    return
                }
                if (t?.isValid == "false") {
                    val customDialog = Dialog(requireActivity())
                    customDialog.setContentView(R.layout.dialog_membership_expired)
                    customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                    customDialog.window?.setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )

                    val btnOkAN = customDialog.findViewById(R.id.tvUpdatePlan) as Button
                    var dateValidTil = customDialog.findViewById(R.id.validDate) as TextView
                    var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    var setDateFormat = SimpleDateFormat("MMM d, yy");
                    val text = setDateFormat.format(getDateFormat.parse(t?.validTo))
                    dateValidTil.text = text + " (EST)"
                    customDialog.setCancelable(false)
                    btnOkAN.setOnClickListener {
                        val intent = Intent(context, MembershipActivity::class.java)
                        startActivity(intent)
                        requireActivity().finish()
                    }
                    customDialog.show()
                } else {

                }
            }
        })

    }
}