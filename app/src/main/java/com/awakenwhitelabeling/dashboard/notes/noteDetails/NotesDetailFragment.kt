package com.awakenwhitelabeling.dashboard.notes.noteDetails

import android.app.Dialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.dashboard.notes.noteDetails.notesUpdate.NotesUpdateFragment
import com.awakenwhitelabeling.others.*
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.base.App
import com.awakenwhitelabeling.dashboard.dailyworkoutflow.detail.WebFragment
import com.awakenwhitelabeling.dashboard.notes.NotesHelper
import com.awakenwhitelabeling.dashboard.notes.noteDetails.adapter.ShowServerImagesAdapter
import com.awakenwhitelabeling.dashboard.notes.noteDetails.notesById.NotesDetailsById
import com.facebook.share.model.ShareHashtag
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.text.SimpleDateFormat
import java.util.*


class NotesDetailFragment : BaseFragment() {
    private lateinit var datetime: String
    lateinit var ivDeleteND: ImageView
    lateinit var ivBackND: ImageView
    lateinit var category: TextView
    lateinit var tvSchduleDT: TextView
    lateinit var ivCalImg: ImageView
    lateinit var details: TextView
    lateinit var fbEdit: FloatingActionButton
    lateinit var publishNote: LinearLayout
    lateinit var bottom_sheet_header: LinearLayout
    lateinit var bottomsheet_top_header: ConstraintLayout
    lateinit var btnCancel2: Button
    lateinit var btnSchedule: Button
    private var notesID: Int = -1
    private var type: String? = null
    private var NotesCategory: String? = null
    private var NotesDetails: String? = null

    /* private var NotesImageSource: String? = ""
     private var NotesDetId: Int? = null*/
    private var notesDataToPass: NotesDetailsById.Data? = null
    private lateinit var recyclerview: RecyclerView
    lateinit var noteDettailsResponse: NotesDetailsById.Data


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notes_detail, container, false)
    }

    private lateinit var mCtx: NotesDetailsActivity
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mCtx = context as NotesDetailsActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivDeleteND = view.findViewById(R.id.ivDeleteND)
        ivBackND = view.findViewById(R.id.ivBackND)
        category = view.findViewById(R.id.tvCategory)
        details = view.findViewById(R.id.tvDetailsND)
        tvSchduleDT = view.findViewById(R.id.tvSchduleDT)
        btnCancel2 = view.findViewById(R.id.btnCancel2)
        btnSchedule = view.findViewById(R.id.btnSchedule)
        ivCalImg = view.findViewById(R.id.ivCalImg)
        fbEdit = view.findViewById(R.id.fbEdit)
        recyclerview = view.findViewById(R.id.notesDetailsRCV)
        bottom_sheet_header = view.findViewById(R.id.bottom_sheet_header)
        Utils.viewGone(bottom_sheet_header)
        bottomsheet_top_header = view.findViewById(R.id.bottomsheet_top_header)
        btnCancel2.setOnClickListener {
            com.awakenwhitelabeling.others.Utils.viewGone(publishNote)
            Utils.viewVisible(fbEdit)
        }


        ivBackND.setOnClickListener {
            requireActivity().onBackPressed()
        }
        ivDeleteND.setOnClickListener {

            //delete()
            deleteConformation()
        }
        fbEdit.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable(Cons.Notes_Data, notesDataToPass)
            val fragment = NotesUpdateFragment()
            fragment.arguments = bundle
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.flNotesDetails, fragment)
                ?.commit()


        }
        val c = Calendar.getInstance()
        val dateformat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        datetime = dateformat.format(c.time)
        parseBundle()
        getUpdateData()
        getWorkoutDetails()
        setLayOutManagerForRecyclerView()
    }

    private fun setLayOutManagerForRecyclerView() {
        recyclerview.layoutManager =
            GridLayoutManager(requireContext(), 4, LinearLayoutManager.VERTICAL, false);
    }

    private fun getUpdateData() {
        if (arguments?.containsKey(Cons.NOTES_DESCRIPTION) == true) {

            var desc11: String? = null
            desc11 = arguments?.get(Cons.NOTES_DESCRIPTION) as String?
            setTextViewHTML(details, desc11)
            Toaster.shortToast(desc11.toString())
        }
    }

    private fun deleteConformation() {
        val conformDialog = Dialog(requireActivity())
        conformDialog.setContentView(R.layout.note_delete_conformation)
        conformDialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val btnCancelNote = conformDialog.findViewById(R.id.btnCancelNote) as Button
        val btnDeleteNote = conformDialog.findViewById(R.id.btnDeleteNote) as Button
        conformDialog.setCancelable(false)

        btnDeleteNote.setOnClickListener {
            //Toaster.shortToast("Wait.......")
            delete()
            activity?.onBackPressed()
        }
        btnCancelNote.setOnClickListener {
            conformDialog.dismiss()
        }
        conformDialog.show()
    }

    protected fun makeLinkClickable(strBuilder: SpannableStringBuilder, span: URLSpan?) {

        val start: Int = strBuilder.getSpanStart(span)
        val end: Int = strBuilder.getSpanEnd(span)
        val flags: Int = strBuilder.getSpanFlags(span)
        val clickable: ClickableSpan = object : ClickableSpan() {

            override fun onClick(widget: View) {
                val fragment = WebFragment()
                fragment.arguments = bundleOf(
                    // Pair(Cons.WEB_TITLE, NotesCategory),
                    // Pair(Cons.WEB_URL, WebUrlHelper.getUrlFromTitle(postType.text.toString()))
                    Pair(Cons.WEB_URL, span?.url)
                )
                activity?.supportFragmentManager?.beginTransaction()
                    //            .add(R.id.flDailworkout,DailyWorkFlowFragment())
                    ?.replace(R.id.flNotesDetails, fragment)
                    ?.addToBackStack(null)
                    ?.commit()
            }
        }
        strBuilder.setSpan(clickable, start, end, flags)
        strBuilder.removeSpan(span)
    }

    protected fun setTextViewHTML(text: TextView, html: String?) {
        val sequence: CharSequence = Html.fromHtml(html)
        val strBuilder = SpannableStringBuilder(sequence)
        val urls: Array<URLSpan> = strBuilder.getSpans(0, sequence.length, URLSpan::class.java)
        for (span in urls) {
            makeLinkClickable(strBuilder, span)
        }
        text.text = strBuilder
        text.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun delete() {
        RetrofitClient.getRequest().deleteNotes(notesID)

            .enqueue(object : Callback<JsonObject?> {
                override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                    hideLoader()
                    ErrorUtils.onFailure(t)
                    // callBack.onSuccess(null)
                }

                override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>?) {
                    try {
                        val code = response?.code() ?: 500
                        if (code == 200 && response?.isSuccessful == true) {
                            hideLoader()
                            Toaster.shortToast("Note deleted successfully")
                            activity?.onBackPressed()

                            Log.d("Noted Deleted", "You data: " + response.body())


                        } else if (code == 400 || code == 401) {
                            ErrorUtils.parseError(response?.errorBody()?.string())
                            //callBack.onSuccess(null)
                        } else {
                            //  callBack.onSuccess(null)
                            Toaster.somethingWentWrong()
                        }
                    } catch (e: Exception) {
                        // callBack.onSuccess(null)
                        Toaster.somethingWentWrong()
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        parseBundle()
    }

    fun getWorkoutDetails() {
        showLoader()
        if (!App.get().isConnected()) {
            hideLoader()
            popUpInternetConnectionDialog()
            return
        }
        getWorkoutDetail(notesID.toString(), object : CallBack<NotesDetailsById.Data>() {
            override fun onSuccess(t: NotesDetailsById.Data) {
                notesDataToPass = t
                Log.d("sdadaa", "dfgdgdgd: " + t.toString())
                hideLoader()
                NotesDetails = t.notesDescription
                NotesCategory = t.category
/*
                NotesDetId = t.id

*/

//                for(i in 0 until t.notesImage.size){
//                    NotesImageSource += t.notesImage.get(i) +","
//                }
                try {
                    if (t.notesImage != null) {
                        //NotesImageSource = t.notesImage.toString()
                        recyclerview.adapter = ShowServerImagesAdapter(t.notesImage)
                    } else {
                        Utils.viewGone(recyclerview)
                    }
                } catch (e: java.lang.Exception) {
                    Log.d("Exception: ", "Exception" + e.localizedMessage)
                }
                setTextViewHTML(details, t.notesDescription)
                if (t.category == "blank") {
                    category.text = " "
                } else {
                    category.text = NotesHelper.returnCamelCaseWord(t.category)
                }
                var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                var setDateFormat = SimpleDateFormat("MMM dd, yyyy");
                var publishDate = t.publishDate
                var createdDate = t.createdAt
                if (t.status.isNullOrEmpty()) {
                    tvSchduleDT.text =
                        setDateFormat.format(getDateFormat.parse(createdDate)) + " EST"
                    ivCalImg.setBackgroundResource(R.drawable.ic_cal_details_new)
                } else {
                    tvSchduleDT.text =
                        setDateFormat.format(getDateFormat.parse(publishDate)) + " EST"
                    ivCalImg.setBackgroundResource(R.drawable.ic_cal_details_new)
                }

            }

            override fun onError(error: String?) {
                super.onError(error)
                hideLoader()
            }
        })
    }

    private fun parseBundle() {
        if (arguments != null) {
            if (arguments?.containsKey(NotesDetailsActivity.EXTRA_Notes) == true) {
                notesID =
                    arguments?.getInt(NotesDetailsActivity.EXTRA_Notes, -1)!!
            }
        }
    }

    private fun notePublishNow(bsDialog: BottomSheetDialog) {

        val hashTag =
            ShareHashtag.Builder()
                .setHashtag(
                    NotesCategory + "\n" + setTextViewHTML(
                        details,
                        NotesDetails
                    )
                )
                .build()
        val content =
            ShareLinkContent.Builder().setQuote(details.toString())
                .setShareHashtag(hashTag)
                .setContentUrl(Uri.parse("")).build()

        ShareDialog.show(requireActivity(), content)


    }

    fun getWorkoutDetail(id: String, callBack: CallBack<NotesDetailsById.Data>) {
        if (!App.get().isConnected()) {
            Toaster.shortToast("Please connect to internet to see more details...")
            callBack.onError(null);
            return
        }
        RetrofitClient.getRequest().getNotesById(id)
            .enqueue(object : Callback<NotesDetailsById?> {
                override fun onFailure(call: Call<NotesDetailsById?>, t: Throwable) {
                    ErrorUtils.onFailure(t)
                    callBack.onSuccess(null)
                }

                override fun onResponse(
                    call: Call<NotesDetailsById?>,
                    response: Response<NotesDetailsById?>?
                ) {
/*

                    noteDettailsResponse =
                        Gson().fromJson(response?.body(), NotesDetailsById.Data::class.java)
                    if (noteDettailsResponse.notesImage.isNullOrEmpty()) {
                        Toaster.shortToast("Image not available")
                    } else {
*/

                    try {
                        val code = response?.code() ?: 500
                        if (code == 200 && response?.isSuccessful == true) {
                            callBack.onSuccess(response.body()?.data)

                            Log.d("TaskDetailsActivity", "You data: " + response.body())
                        }/* else {
                            val jObjError = JSONObject(response?.errorBody()?.string())
                            Toaster.shortToast(
                                jObjError.getString("message")
                            )
                        }*/
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.d("error=============", "Exception error: " + e.toString())

                        Toaster.shortToast(e.toString())
                    }
                }
                /*}*/
            })
    }


}



