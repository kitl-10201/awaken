package com.awakenwhitelabeling.dashboard.notes.noteDetails.notesUpdate.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.bumptech.glide.Glide

class NoteUpdateAdapter(
    private val mList: ArrayList<String>
) :
    RecyclerView.Adapter<NoteUpdateAdapter.ViewHolder>() {
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.selecte_multiple_image_from_gallery, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ItemsViewModel = mList[position]
        Glide.with(context)
            .load(RetrofitClient.WORKOUT_IMAGE_URL + ItemsViewModel)
            .into(holder.imageView)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imageView: ImageView = itemView.findViewById(R.id.ivSelectedImage)
        val ivDelete: ImageView = itemView.findViewById(R.id.ivDelete)
    }
}
