package com.awakenwhitelabeling.dashboard.notes.adapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.notes.noteDetails.notesUpdate.adapter.NewUpdateBean
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.bumptech.glide.Glide
import java.lang.Exception


class UpdateNotesImageAdapter(
    val callBack: CallBack<NewUpdateBean.CombinedList>,
    private val mList: ArrayList<NewUpdateBean.CombinedList>
) : RecyclerView.Adapter<UpdateNotesImageAdapter.ViewHolder>() {
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.selecte_multiple_image_from_gallery, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ItemsViewModel = mList[position]
        if (!ItemsViewModel.isGalleryImage) {
            var replacedPath = ""
            if (ItemsViewModel.serverImage.contains("[")) {
                replacedPath = ItemsViewModel.serverImage.replace("[", "")

            } else if (ItemsViewModel.serverImage.contains("]")) {
                replacedPath = ItemsViewModel.serverImage.replace("]", "")
            } else
                replacedPath = ItemsViewModel.serverImage
            Glide.with(context)
                .load(RetrofitClient.WORKOUT_IMAGE_URL + replacedPath)
                .into(holder.imageView)
        } else {
            holder.imageView.setImageBitmap(ItemsViewModel?.imageGallery)
        }
        holder.ivDelete.setOnClickListener {
            try {
                mList.removeAt(position)
                notifyItemRemoved(position)
                notifyDataSetChanged()
                callBack.onSuccess(ItemsViewModel)
            } catch (e: Exception) {
                Log.d("RemovedItem", "RemovedItemException: " + e.localizedMessage)
            }
        }
        holder.ivShowImage.setOnClickListener {
            val alertView: View =
                LayoutInflater.from(context).inflate(R.layout.show_selected_image_from_view, null)
            val alertDialog = AlertDialog.Builder(context)

            alertDialog.setView(alertView)
            val ivShowSelectedImage: ImageView = alertView.findViewById(R.id.ivShowSelectedImage)
            val ivClosePreview: ImageView = alertView.findViewById(R.id.ivClosePreview)


            /* if (ItemsViewModel.isGalleryImage) {
                 ivShowSelectedImage.setImageBitmap(ItemsViewModel.imageGallery)

             } else {
                PicassoUtil.loadServerimage(ivShowSelectedImage, ItemsViewModel.serverImage)
                 val path = RetrofitClient.WORKOUT_IMAGE_URL + ItemsViewModel.serverImage
 *//*
                Glide.with(context)
                    .load(RetrofitClient.WORKOUT_IMAGE_URL + ItemsViewModel.serverImage)
                    .into(ivShowSelectedImage)*//*


          *//*      Glide.with(context)
                    .load(RetrofitClient.WORKOUT_IMAGE_URL + ItemsViewModel.serverImage)
                    .into(ivShowSelectedImage);*//*
            }*/
            if (!ItemsViewModel.isGalleryImage) {
                var replacedPath = ""
                if (ItemsViewModel.serverImage.contains("[")) {
                    replacedPath = ItemsViewModel.serverImage.replace("[", "")

                } else if (ItemsViewModel.serverImage.contains("]")) {
                    replacedPath = ItemsViewModel.serverImage.replace("]", "")
                } else
                    replacedPath = ItemsViewModel.serverImage
                Glide.with(context)
                    .load(RetrofitClient.WORKOUT_IMAGE_URL + replacedPath)
                    .into(ivShowSelectedImage)
            } else {
                ivShowSelectedImage.setImageBitmap(ItemsViewModel?.imageGallery)
            }
            alertDialog.setCancelable(false)
            val dialog = alertDialog.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            ivClosePreview.setOnClickListener {
                dialog.dismiss()
            }

        }

    }


    override fun getItemCount(): Int {
        return mList.size
    }


    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val ivShowImage: CardView = itemView.findViewById(R.id.mlShowImage)
        val imageView: ImageView = itemView.findViewById(R.id.ivSelectedImage)
        val ivDelete: ImageView = itemView.findViewById(R.id.ivDelete)
    }
}