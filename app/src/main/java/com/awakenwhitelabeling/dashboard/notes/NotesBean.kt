package com.awakenwhitelabeling.dashboard.notes

import java.io.Serializable

data class NotesBean(
    val `data`: Data,
    val message: String,
    val status: String
):Serializable {
    data class Data(
        val current_page: Int,
        val `data`: List<Data>,
        val first_page_url: String,
        val from: Int,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String,
        val path: String,
        val per_page: Int,
        val prev_page_url: String,
        val to: Int,
        val total: Int
    ):Serializable
    {
        data class Data(
            val category: String,
            val created_at: String,
            val id: Int,
            val notes_description: String,
            val post_type: String,
            val publish_date: String,
            val status: String,
            val updated_at: String,
            val user_id: Int
        ):Serializable
    }
}