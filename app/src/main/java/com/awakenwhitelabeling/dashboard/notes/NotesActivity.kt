package com.awakenwhitelabeling.dashboard.notes

import android.os.Bundle
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity

class NotesActivity:BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)
        supportFragmentManager.beginTransaction()
            .add(R.id.flNotes, AddNotesFragment())
            .commit()
    }
}