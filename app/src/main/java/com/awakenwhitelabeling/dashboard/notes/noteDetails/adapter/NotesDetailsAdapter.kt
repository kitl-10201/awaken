package com.awakenwhitelabeling.dashboard.notes.noteDetails.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.notes.noteDetails.NotesDetailFragment
import com.awakenwhitelabeling.dashboard.notes.noteDetails.notesById.NotesDetailsById
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.bumptech.glide.Glide

class NotesDetailsAdapter(
    private val mList: List<NotesDetailsById.Data>,
    context:NotesDetailFragment
    ) :
    RecyclerView.Adapter<NotesDetailsAdapter.ViewHolder>() {
    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.image_from_server, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ItemsViewModel = mList[position]
       // holder.imageView.
        /*holder.ivDelete.setOnClickListener {

            try {
                mList.removeAt(position)
                notifyItemRemoved(position)
            }catch (e: Exception)
            {
                Log.d("RemovedItem","RemovedItemException: "+e.localizedMessage)
            }

        }*/



        Glide.with(context)
            .load(RetrofitClient.WORKOUT_IMAGE_URL +ItemsViewModel.notesImage)
            .into(holder.imageView)

    }

    override fun getItemCount(): Int {
        return mList.size
    }


    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imageView: ImageView = itemView.findViewById(R.id.ivSelectedImage)
    }
}