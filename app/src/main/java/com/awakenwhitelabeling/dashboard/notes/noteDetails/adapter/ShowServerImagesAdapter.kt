package com.awakenwhitelabeling.dashboard.notes.noteDetails.adapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.others.PicassoUtil
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.bumptech.glide.Glide

class ShowServerImagesAdapter(
    private val mList: List<String>
) :
    RecyclerView.Adapter<ShowServerImagesAdapter.ViewHolder>() {
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.show_server_image, parent, false)
        return ViewHolder(view)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ItemsViewModel = mList[position]
        var replacedPath = ""
        if (ItemsViewModel.contains("[")) {
            replacedPath = ItemsViewModel.replace("[", "")

        } else if (ItemsViewModel.contains("]")) {
            replacedPath = ItemsViewModel.replace("]", "")

        } else
            replacedPath = ItemsViewModel
        Glide.with(context)
            .load(RetrofitClient.WORKOUT_IMAGE_URL + replacedPath)
            .into(holder.imageView)
        holder.mlShowImage.setOnClickListener {
            val alertView: View =
                LayoutInflater.from(context).inflate(R.layout.show_selected_image_from_view, null)
            val alertDialog = AlertDialog.Builder(context)
            alertDialog.setView(alertView)
            //alertDialog.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            val ivShowSelectedImage: ImageView = alertView.findViewById(R.id.ivShowSelectedImage)
            val ivClosePreview: ImageView = alertView.findViewById(R.id.ivClosePreview)
            alertDialog.setCancelable(false)
            val dialog = alertDialog.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            ivClosePreview.setOnClickListener {
                dialog.dismiss()
            }

            PicassoUtil.loadServerimage(ivShowSelectedImage, ItemsViewModel)
            // PicassoUtil.loadPicassoImage(ivShowSelectedImage,ItemsViewModel)
        }
        // PicassoUtil.loadProfileImage(holder.imageView, RetrofitClient.WORKOUT_IMAGE_URL + ItemsViewModel)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val mlShowImage: CardView = itemView.findViewById(R.id.mlShowImage)
        val imageView: ImageView = itemView.findViewById(R.id.ivSelectedImage)
    }
}