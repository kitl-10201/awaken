package com.awakenwhitelabeling.dashboard.notes.dialog

import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.widget.Button
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseDialog
import com.awakenwhitelabeling.others.CallBack

class TextDialog(context: Activity, private val callBack: CallBack<Int>) :
    BaseDialog(context) {


    private val mCtx = context
    lateinit var logout_diag: Button
    lateinit var stay_diag: Button
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setCancelable(false)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.text_dialog)
        setDimBlur(window)



    }

}