package com.awakenwhitelabeling.dashboard.notes.adapter.selected_image_bean

import android.graphics.Bitmap
import com.google.gson.annotations.SerializedName

data class AddSelectedImageInNotesSection(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: String

){
    data class SelectedImageList( var imageGallery: Bitmap)

}


