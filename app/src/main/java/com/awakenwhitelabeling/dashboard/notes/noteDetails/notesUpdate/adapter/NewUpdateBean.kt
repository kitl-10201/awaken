package com.awakenwhitelabeling.dashboard.notes.noteDetails.notesUpdate.adapter


import android.graphics.Bitmap
import com.google.gson.annotations.SerializedName

data class NewUpdateBean(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: String

) {
    data class CombinedList(var serverImage: String, var imageGallery: Bitmap?, var isGalleryImage:Boolean =false)
}