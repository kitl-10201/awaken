package com.awakenwhitelabeling.dashboard.notes.noteDetails.notesUpdate


import com.awakenwhitelabeling.dashboard.notes.NotesBean
import com.google.gson.annotations.SerializedName

data class NotesUpdatedResponseBean(
    @SerializedName("data")
    val `data`: NotesBean.Data.Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)