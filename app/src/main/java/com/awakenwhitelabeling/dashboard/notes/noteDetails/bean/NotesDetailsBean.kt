package com.awakenwhitelabeling.dashboard.notes.noteDetails.bean

import com.google.gson.annotations.SerializedName

data class NotesDetailsBean(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: Data
) {
    data class Data(
        @SerializedName("id")
        val id: Int,
        @SerializedName("user_id")
        val userId: Int,
        @SerializedName("notes_description")
        val notesDescription: String,
        @SerializedName("post_type")
        val postType: Any,
        @SerializedName("status")
        val status: Any,
        @SerializedName("category")
        val category: String,
        @SerializedName("notes_image")
        val notesImage: List<String>,
        @SerializedName("publish_date")
        val publishDate: String,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("updated_at")
        val updatedAt: String
    )
}
