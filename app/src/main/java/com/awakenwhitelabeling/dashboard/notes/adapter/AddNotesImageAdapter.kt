package com.awakenwhitelabeling.dashboard.notes.adapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.others.CallBack
import java.lang.Exception

class AddNotesImageAdapter(
    val callBack: CallBack<Int>,
    private val mList: MutableList<Bitmap>
) : RecyclerView.Adapter<AddNotesImageAdapter.ViewHolder>() {
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.selecte_multiple_image_from_gallery, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ItemsViewModel = mList[position]
        //TODO set bitmap
        holder.imageView.setImageBitmap(ItemsViewModel)
        holder.ivDelete.setOnClickListener {
            try {
              //  mList.removeAt(position)
                notifyItemRemoved(position)
                callBack.onSuccess(position)
            } catch (e: Exception) {
                Log.d("RemovedItem", "RemovedItemException: " + e.localizedMessage)
            }
        }
        holder.mlShowImage.setOnClickListener {
            val alertView: View =
                LayoutInflater.from(context).inflate(R.layout.show_selected_image_from_view, null)
            val alertDialog = AlertDialog.Builder(context)
            alertDialog.setView(alertView)
            val ivShowSelectedImage: ImageView = alertView.findViewById(R.id.ivShowSelectedImage)
            val ivClosePreview: ImageView = alertView.findViewById(R.id.ivClosePreview)
            ivShowSelectedImage.setImageBitmap(ItemsViewModel)
            alertDialog.setCancelable(false)
            val dialog = alertDialog.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            ivClosePreview.setOnClickListener {
                dialog.dismiss()
            }


            //   PicassoUtil.loadProfileImage(ivShowSelectedImage, ItemsViewModel)
            //  ivShowSelectedImage.setImageResource(position)

        }

    }


    override fun getItemCount(): Int {
        return mList.size
    }


    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val mlShowImage: CardView = itemView.findViewById(R.id.mlShowImage)
        val imageView: ImageView = itemView.findViewById(R.id.ivSelectedImage)
        val ivDelete: ImageView = itemView.findViewById(R.id.ivDelete)
    }
}








