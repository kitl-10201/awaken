package com.awakenwhitelabeling.dashboard.notes

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Html
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.App
import com.awakenwhitelabeling.base.ErrorEventHelper
import com.awakenwhitelabeling.dashboard.NewNotes.AddMediaBean
import com.awakenwhitelabeling.dashboard.account.UserDetailsBean
import com.awakenwhitelabeling.dashboard.notes.adapter.AddNotesImageAdapter
import com.awakenwhitelabeling.dashboard.policy.PrivacyPolicyActivity
import com.awakenwhitelabeling.dashboard.profile.SaveMultipleImage
import com.awakenwhitelabeling.others.*
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.facebook.share.model.ShareHashtag
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.github.onecode369.wysiwyg.WYSIWYG
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

@SuppressLint("RestrictedApi")
class AddNotesFragment : SaveMultipleImage(),
    AdapterView.OnItemSelectedListener {
    private var mBottomSheetLayout: LinearLayout? = null
    private lateinit var tokenServerApi: RetrofitApi
    lateinit var call: Call<JsonObject>
    private lateinit var rlTextClick: LinearLayout
    private lateinit var rlImageClick: LinearLayout
    private lateinit var llPublich: LinearLayout
    private lateinit var recyclerview: RecyclerView
    private var mediaLink: String = ""
    private lateinit var bottom_sheet_header: LinearLayout
    private lateinit var bottomsheet_top_header: ConstraintLayout
    private lateinit var ivShare: ImageView
    private lateinit var postRedirect: ImageView
    private lateinit var editor: WYSIWYG
    private lateinit var bold: ImageView
    private lateinit var italic: ImageView
    private lateinit var underline: ImageView
    private lateinit var align_indent: ImageView
    private lateinit var ivTextIncrease: ImageView
    private lateinit var align_center: ImageView
    private lateinit var backAN: ImageView
    private lateinit var spinner: Spinner
    private val gallery = 1001
    private val camera = 1002
    var dataForImage = ArrayList<Bitmap>(4)
    var fil = ArrayList<File>()
    private lateinit var adapterR: AddNotesImageAdapter
    private var selectedCategory: String? = null
    private var type: String? = null
    private var dSchedule: String? = null
    private lateinit var mStartDateTime: String
    private lateinit var datetime: String
    private lateinit var hyperLink: RelativeLayout
    private lateinit var selImage: RelativeLayout
    var isRight: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showLoader()
    }

    private lateinit var mCtx: NotesActivity
    override fun onPermissionGranted(REQUESTED_FOR: Int) {
        showPictureAndCameraPopup()
    }

    override fun onPermissionDisabled(REQUESTED_FOR: Int) {
        TODO("Not yet implemented")
    }

    override fun onPermissionDenied(REQUESTED_FOR: Int) {
        TODO("Not yet implemented")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mCtx = context as NotesActivity
    }

    override fun onResume() {
        super.onResume()
        getDriveLink()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add_notes, container, false)
        backAN = view.findViewById(R.id.ivBackAN)
        mBottomSheetLayout = view.findViewById<LinearLayout>(R.id.bottom_sheet_layout)
        rlTextClick = view.findViewById(R.id.rlTextClick)
        rlImageClick = view.findViewById(R.id.rlImageClick)
        hyperLink = view.findViewById(R.id.rlIHyper)
        selImage = view.findViewById(R.id.rlIImage)
        bottomsheet_top_header = view.findViewById(R.id.bottomsheet_top_header)
        llPublich = view.findViewById(R.id.llPublich)
        bottom_sheet_header = view.findViewById(R.id.bottom_sheet_header)
        editor = view.findViewById(R.id.editorDesc)
        bold = view.findViewById(R.id.ivBoldNotes)
        italic = view.findViewById(R.id.ivItalicNotes)
        underline = view.findViewById(R.id.ivUnderlineNotes)
        align_indent = view.findViewById(R.id.ivAllignRight)
        align_center = view.findViewById(R.id.ivAllignCenter)
        ivTextIncrease = view.findViewById(R.id.ivTextIncrease)
        spinner = view.findViewById(R.id.spinner)
        postRedirect = view.findViewById(R.id.img1)
        ivShare = view.findViewById(R.id.img2)
        recyclerview = view.findViewById(R.id.recSelecetedImage)
        val wysiwygEditor = editor
        wysiwygEditor.setEditorHeight(200)
        wysiwygEditor.setEditorFontSize(16)
        wysiwygEditor.setPlaceholder("Insert your notes here...")
        editor.focusEditor()
        editor.getSettings().setLightTouchEnabled(true);
        bold.setOnClickListener {
            wysiwygEditor.setBold()
        }
        italic.setOnClickListener {
            wysiwygEditor.setItalic()
        }
        underline.setOnClickListener {
            wysiwygEditor.setUnderline()
        }
        align_indent.setOnClickListener {
            if (isRight) {
                wysiwygEditor.setAlignLeft()
                align_indent.setImageResource(R.drawable.ic_text_align_right)
                isRight = false
            } else {
                wysiwygEditor.setAlignRight()
                align_indent.setImageResource(R.drawable.ic_text_left_align)
                isRight = true
            }
        }
        ivTextIncrease.setOnClickListener {
            if (isRight) {
                // wysiwygEditor.setAlignLeft()
                wysiwygEditor.setHeading(2)
                ivTextIncrease.setImageResource(R.drawable.ic_text_increase)
                isRight = false
            } else {
                wysiwygEditor.setHeading(1)
                isRight = true
            }
        }
        align_center.setOnClickListener {
            if (isRight) {
                wysiwygEditor.setAlignCenter()
                align_center.setImageResource(R.drawable.ic_text_left_align)
                isRight = false

            } else {
                wysiwygEditor.setAlignLeft()
                align_center.setImageResource(R.drawable.ic_text_align)
                isRight = true
            }
        }
        postRedirect.setOnClickListener {
            postData()
        }
        var rlIText = view.findViewById<RelativeLayout>(R.id.rlIText)
        var rlDrive = view.findViewById<RelativeLayout>(R.id.rlDrive)

        var rlIHyper = view.findViewById<RelativeLayout>(R.id.rlIHyper)
        var rlImage = view.findViewById<RelativeLayout>(R.id.rlIImage)
        val bottomSheet: View = view.findViewById(R.id.bottom_sheet_layout)
        var behavior: BottomSheetBehavior<*>? = null
        behavior = BottomSheetBehavior.from(bottomSheet)
        if (dataForImage.size != 4) {
            rlImage.isClickable = true
        } else {
            Toaster.shortToast("You can not select more than 4 Image")
            rlImage.isClickable = false
        }
        rlIText.setOnClickListener {

            if (isRight) {
                com.awakenwhitelabeling.others.Utils.viewGone(rlTextClick)
                isRight = false
            } else {
                com.awakenwhitelabeling.others.Utils.viewVisible(rlTextClick)
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                isRight = true

            }
        }

        try {
            rlDrive.setOnClickListener {
                //Toaster.shortToast("Add Media clicked!")
                if (!mediaLink.isNullOrEmpty() && isValidUrl(mediaLink.toString())) {

                    try {
                        val intent = Intent(requireActivity(), PrivacyPolicyActivity::class.java)
                        intent.putExtra("Name", mediaLink)
                        intent.putExtra("Title", "Add Media")
                        startActivity(intent)

                    } catch (e: Exception) {
                        Log.d("DriveLink", "onError in DriveLink : " + e.localizedMessage)
                    }
                } else {
                    // Toaster.somethingWentWrong()

                    ErrorEventHelper().Error_dialog(
                        context as Activity,
                        Cons.InvalideURL,
                        true
                    )
                }

            }
        } catch (e: Exception) {
            Log.d("DriveLink", "onError in DriveLink : " + e.localizedMessage)
        }


        ivShare.setOnClickListener {
            if (editor.html.isNullOrEmpty() || editor.html.toString().replace("&nbsp;", "")
                    .trim().length == 0
            ) {
                Toaster.shortToast(Cons.NOTE_EMPTY_MSG)
            } else {
                lateinit var startDate: Calendar
                var bsDialog = BottomSheetDialog(requireContext())
                bsDialog.setContentView(R.layout.bottom_sheet_add_notes)
                var rgMainAddNote = bsDialog.findViewById<RadioGroup>(R.id.rgMainAddNote)
                var rbPublishAddNote = bsDialog.findViewById<RadioButton>(R.id.rbPublishAddNote)
                var rbScheduleAddNote = bsDialog.findViewById<RadioButton>(R.id.rbScheduleAddNote)
                var btnCancelAddNote = bsDialog.findViewById<Button>(R.id.btnCancelAddNote)
                var btnOkAddNote = bsDialog.findViewById<Button>(R.id.btnOkAddNote)
                rgMainAddNote?.setOnCheckedChangeListener { group, checkedId ->
                    type =
                        if (R.id.rbPublishAddNote == checkedId) Cons.NOTEPUBLISH_TYPE else Cons.NOTESCHEDULE_TYPE
                }




                rbScheduleAddNote?.setOnClickListener {
                    DateHelper.openDatePicker(mCtx, object : CallBack<Calendar>() {
                        override fun onSuccess(cal: Calendar?) {
                            if (cal != null) {
                                startDate = cal
                                rbScheduleAddNote?.setText(Cons.INPUT_DATE_FORMAT1.format(startDate.time))
                                mStartDateTime = Cons.INPUT_TIME_FORMAT1.format(startDate.time)
                                dSchedule = rbScheduleAddNote.text.toString()
                                Log.d("selected........", "skjfsf" + dSchedule)
                            }
                        }
                    })
                }



                btnOkAddNote?.setOnClickListener {

                    if (type.equals(Cons.NOTEPUBLISH_TYPE)) {
                        if (editor.html.isNullOrEmpty() || editor.html.toString()
                                .replace("&nbsp;", "")
                                .trim().length == 0
                        ) {
                            Toaster.shortToast(Cons.NOTE_EMPTY_MSG)
                        } else {

                            if (isRight) {
                                isRight = false
                            } else {
                                SharePostData(bsDialog)
                                btnOkAddNote.isClickable = false
                                isRight = true
                            }

                        }
                    } else if (type.equals(Cons.NOTESCHEDULE_TYPE)) {

                        if (dSchedule.isNullOrEmpty()) {
                            Toaster.shortToast(Cons.SELECT_DATE_TIME)
                        } else {
                            if (isRight) {
                                isRight = false
                            } else {
                                scheduleNotes()
                                btnOkAddNote.isClickable = false
                                isRight = true
                            }
                        }
                    } else {
                        Toaster.shortToast(Cons.SELECTION_TYPE)
                    }
                }
                btnCancelAddNote?.setOnClickListener {
                    bsDialog.dismiss()
                }
                bsDialog.show()
                bsDialog.setCancelable(false)
            }
        }

        rlImage.setOnClickListener {

            if (isRight) {
                com.awakenwhitelabeling.others.Utils.viewGone(bottom_sheet_header)
                isRight = false

            } else {
                com.awakenwhitelabeling.others.Utils.viewVisible(bottom_sheet_header)
                isRight = true

            }
            var bsDialog = BottomSheetDialog(requireContext())
            bsDialog.setContentView(R.layout.add_selected_image_into_note_section)
            bsDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
            var tvGallery = bsDialog.findViewById<TextView>(R.id.tvGallery)
            var tvCamera = bsDialog.findViewById<TextView>(R.id.tvCamera)
            var btnCancellDialog = bsDialog.findViewById<TextView>(R.id.btnCancelImageSelection)
            bsDialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            tvGallery?.setOnClickListener {
                com.awakenwhitelabeling.others.Utils.viewVisible(bottom_sheet_header)
                choosePhotoFromGallary()
                bsDialog.dismiss()

            }
            tvCamera?.setOnClickListener {
                if (hasCameraAndStoragePermission()) {
                    takePhotoFromCamera()
                    com.awakenwhitelabeling.others.Utils.viewVisible(bottom_sheet_header)
                    bsDialog.dismiss()
                }
            }
            btnCancellDialog?.setOnClickListener {
                bsDialog.dismiss()
                com.awakenwhitelabeling.others.Utils.viewVisible(bottom_sheet_header)
            }
            bsDialog.show()
            bsDialog.setCancelable(false)
        }

        rlIHyper.setOnClickListener {
            val customDialog = Dialog(requireActivity())
            customDialog.setContentView(R.layout.dialog_generate_hyper_link)
            customDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            customDialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            val inputTitle = customDialog.findViewById(R.id.hlTitle) as EditText
            val inputLink = customDialog.findViewById(R.id.hlLink) as EditText
            val linkTextInputLayout = customDialog.findViewById(R.id.tlLink) as TextInputLayout
            val titleTextInputLayout = customDialog.findViewById(R.id.tlTitle) as TextInputLayout
            val btnCancelAN = customDialog.findViewById(R.id.btnCancelAN) as Button
            val btnOkAN = customDialog.findViewById(R.id.btnOkAN) as Button
            customDialog.setCancelable(false)
            btnOkAN.setOnClickListener {
                when {
                    inputTitle.text.isNullOrEmpty() -> {
                        titleTextInputLayout.error = Cons.HYPER_TITLE
                    }
                    inputLink.text.isNullOrEmpty() -> {
                        linkTextInputLayout.error = Cons.VALID_URL
                    }
                    !Patterns.WEB_URL.matcher(inputLink.text.toString()).matches() -> {
                        linkTextInputLayout.error = Cons.VALID_URL
                    }

                    else -> {
                        linkTextInputLayout.error = null
                        titleTextInputLayout.error = null
                        customDialog.dismiss()
                        editor.focusEditor()
                        editor.insertLink(
                            "https://" + inputLink.text.toString(),
                            inputTitle.text.toString()
                        )
                    }
                }
            }
            btnCancelAN.setOnClickListener {
                customDialog.dismiss()
            }
            customDialog.show()
        }


        val colors =
            arrayOf(
                "Select category",
                "Entertain",
                "Educate",
                "Motivate",
                "Lifestyle",
                "Call to Action"
            )
        val adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item, colors
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter;
        spinner.onItemSelectedListener = this;

        val c = Calendar.getInstance()
        //val dateformat = SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa")
        val dateformat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        datetime = dateformat.format(c.time)
        backAN.setOnClickListener {
            activity?.onBackPressed()
        }

        adapterR = AddNotesImageAdapter(mCallback, dataForImage)
        recyclerview.layoutManager =
            GridLayoutManager(requireContext(), 4, LinearLayoutManager.VERTICAL, false);
        recyclerview.adapter = adapterR
        return view
    }

    private fun isValidUrl(url: String): Boolean {

        val p: Pattern = Patterns.WEB_URL
        val m: Matcher = p.matcher(url.toLowerCase())
        return m.matches()
    }

    var mCallback = object : CallBack<Int>() {
        override fun onSuccess(t: Int) {
            dataForImage.removeAt(t)
            fil.removeAt(t)
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val str1 = parent?.getItemAtPosition(position).toString()
        if (str1 == "Call to Action") {
            selectedCategory = "call-to-action"
            Toaster.shortToast(selectedCategory.toString())
        } else if (str1 == "Select category") {
            selectedCategory = "blank"
        } else {
            selectedCategory = parent?.getItemAtPosition(position).toString()
            Log.d("Listtttttt", "onItemSelected: " + selectedCategory)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
//        TODO("Not yet implemented")
    }

    private fun postData() {
        if (editor.html.isNullOrEmpty() || editor.html.toString().replace("&nbsp;", "")
                .trim().length == 0
        ) {
            Toaster.shortToast(Cons.NOTE_EMPTY_MSG)
        } else {
            showLoader()
            var saveNotes = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
            val builder = MultipartBody.Builder()
            builder.setType(MultipartBody.FORM)
            builder.addFormDataPart("notes_description", editor.html.toString())
            builder.addFormDataPart("last_name", datetime)
            builder.addFormDataPart("post_type", "")
            builder.addFormDataPart("publish_date", datetime)
            builder.addFormDataPart("status", Cons.PUBLISH_NOW)
            builder.addFormDataPart("category", selectedCategory?.toLowerCase().toString())
            if (fil != null) {
                try {
                    System.out.println("New file array list: " + fil.toArray())
                    for (i in 0 until fil?.size!!) {
                        fil?.get(i)?.name?.let {
                            Log.e("postData: ", it)
                        }
                        builder.addFormDataPart(
                            "notes_image[]", fil?.get(i)?.name,
                            RequestBody.create(MediaType.parse("multipart/form-data"), fil?.get(i))
                        )
                    }
                } catch (e: Exception) {
                    Log.d("fggggsg", "dfsdfsfs: " + e.localizedMessage.toString())
                }
            } else {
                Log.d("File..Null", "File empty...." + fil)
            }
            val requestBody = builder.build()
            tokenServerApi = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
            call = tokenServerApi.addNotesInMultipart(requestBody)
            call.enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    if (response.isSuccessful && response.code() == 200) {
                        hideLoader()
                        Log.d("dfsdfsf", "dfsdfs" + response.body())
                        val userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                        Toaster.shortToast("Note saved Successfully")
                        activity?.finish()

                    } else if (response.code() == 400) {
                        hideLoader()
                        Toaster.shortToast("Images can not be more than 2048 kb per image.")
                    } else if (response.code() == 500) {
                        hideLoader()
                        Toaster.shortToast("Server not responding.")
                    } else {
                        Toaster.shortToast("Something went wrong.")
                    }

                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    hideLoader()
                    ErrorUtils.onFailure(t)
                }
            })
        }
    }

    private fun SharePostData(bsDialog: BottomSheetDialog) {

        if (editor.html.isNullOrEmpty() || editor.html.toString().replace("&nbsp;", "")
                .trim().length == 0
        ) {
            Toaster.shortToast(Cons.NOTE_EMPTY_MSG)
        } else {
            showLoader()

            val builder = MultipartBody.Builder()
            builder.setType(MultipartBody.FORM)
            builder.addFormDataPart("notes_description", editor.html.toString())
            builder.addFormDataPart("post_type", "")
            builder.addFormDataPart("publish_date", datetime)
            builder.addFormDataPart("post_type", Cons.FACEBOOK)
            builder.addFormDataPart("status", Cons.PUBLISH_NOW)
            builder.addFormDataPart("category", selectedCategory?.toLowerCase().toString())
            if (fil != null) {
                for (i in 0 until fil?.size!!) {
                    fil?.get(i)?.name?.let { Log.e("postData: ", it) }
                    builder.addFormDataPart(
                        "notes_image[]", fil?.get(i)?.name,
                        RequestBody.create(MediaType.parse("multipart/form-data"), fil?.get(i))
                    )
                }
            } else {
                Log.d("File..Null", "File empty...." + fil)
            }
            val requestBody = builder.build()
            tokenServerApi = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
            call = tokenServerApi.addNotesInMultipart(requestBody)
            call.enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    if (response.isSuccessful && response.code() == 200) {
                        hideLoader()
                        Log.d("dfsdfsf", "dfsdfs" + response.body())
                        val userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                        Toaster.shortToast("Note saved Successfully")
                        val hashTag =
                            ShareHashtag.Builder()
                                .setHashtag(selectedCategory?.uppercase())
                                .build()
                        val content =
                            ShareLinkContent.Builder()
                                .setQuote("\n" + Html.fromHtml(editor.html.toString()).toString())
                                .setShareHashtag(hashTag)
                                .setContentUrl(Uri.parse(RetrofitClient.WORKOUT_IMAGE_URL + "/awaken_logo.jpeg"))
                                .build()
                        ShareDialog.show(requireActivity(), content)
                        activity?.finish()

                    } else if (response.code() == 400) {
                        hideLoader()
                        Toaster.shortToast("Selected images can not be more than 2048 kb per image.")
                    } else if (response.code() == 500) {
                        hideLoader()
                        Toaster.shortToast("Server not responding.")
                    } else {
                        Toaster.shortToast("Something went wrong.")
                    }

                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    hideLoader()
                    ErrorUtils.onFailure(t)
                    Log.d("ddfsdf", "dfsdfksl" + t.message)
                }


            })
        }
    }

    private fun scheduleNotes() {
        if (editor.html.isNullOrEmpty() || editor.html.toString().replace("&nbsp;", "")
                .trim().length == 0
        ) {
            Toaster.shortToast(Cons.NOTE_EMPTY_MSG)
        } else {
            showLoader()

            val builder = MultipartBody.Builder()
            builder.setType(MultipartBody.FORM)
            builder.addFormDataPart("notes_description", editor.html.toString())
            builder.addFormDataPart("post_type", "")
            builder.addFormDataPart("publish_date", dSchedule.toString())
            builder.addFormDataPart("post_type", Cons.FACEBOOK)
            builder.addFormDataPart("status", Cons.SCHEDULE)
            builder.addFormDataPart("category", selectedCategory?.toLowerCase().toString())
            if (fil != null) {
                for (i in 0 until fil?.size!!) {
                    fil?.get(i)?.name?.let { Log.e("postData: ", it) }
                    builder.addFormDataPart(
                        "notes_image[]", fil?.get(i)?.name,
                        RequestBody.create(MediaType.parse("multipart/form-data"), fil?.get(i))
                    )
                }
            } else {
                Log.d("File..Null", "File empty...." + fil)
            }
            val requestBody = builder.build()
            tokenServerApi = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
            call = tokenServerApi.addNotesInMultipart(requestBody)
            call.enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    if (response.isSuccessful && response.code() == 200) {
                        hideLoader()
                        Log.d("dfsdfsf", "dfsdfs" + response.body())
                        val userData = Gson().fromJson(response.body(), UserDetailsBean::class.java)
                        Toaster.shortToast("Note saved Successfully")
                        activity?.finish()
                    } else if (response.code() == 400) {
                        hideLoader()
                        Toaster.shortToast("Selected images can not be more than 2048 kb per image.")
                    } else if (response.code() == 500) {
                        hideLoader()
                        Toaster.shortToast("Server not responding.")
                    } else {
                        hideLoader()
                        Toaster.shortToast("Something went wrong.")
                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    hideLoader()
                    ErrorUtils.onFailure(t)
                }
            })
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == gallery) {
            if (data?.clipData != null) {
                val count = data.clipData!!.itemCount
                if (count > 4) {
                    Toaster.shortToast("You can't select more than 4 images ")
                    return
                }
                for (i in 0 until count) {
                    val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        ImageDecoder.decodeBitmap(
                            ImageDecoder.createSource(
                                requireContext().contentResolver,
                                data?.clipData!!.getItemAt(i).uri
                            )
                        )
                    } else {
                        MediaStore.Images.Media.getBitmap(
                            requireContext().contentResolver,
                            data?.clipData!!.getItemAt(i).uri
                        )
                    }
                    dataForImage.add(bitmap)
//                        dataForImage.add(data?.clipData!!.getItemAt(i))
                    Log.d("UriList is empty", "myFileUriList: " + dataForImage?.size)
                }
                fil.clear()
                saveMultipleImage(dataForImage, fil, activity)
                adapterR.notifyDataSetChanged()
                if (dataForImage != null && dataForImage.size <= 4) {
                    Log.d("dfsdfs", "myFileUriList:" + fil?.size)
                } else {
                    Toaster.somethingWentWrong()
                    Log.d("UriList is empty", "myFileUriList: " + dataForImage?.size)
                }
                if (dataForImage.size > 4) {
                    newImageList(dataForImage)
                }
            } else if (data != null) {
                if (data.clipData == null) {
                    val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        ImageDecoder.decodeBitmap(
                            ImageDecoder.createSource(
                                requireContext().contentResolver,
                                data?.data!!
                            )
                        )
                    } else {
                        MediaStore.Images.Media.getBitmap(
                            requireContext().contentResolver,
                            data.data
                        )
                    }
                    if (dataForImage.size >= 4) {
                        Toaster.shortToast("You can not add more than 4 images")
                    } else {
                        dataForImage.add(bitmap)
                        adapterR.notifyDataSetChanged()
                        fil.clear()
                        saveMultipleImage(dataForImage, fil, activity)
                    }
                }
            }
        } else if (requestCode == camera) {
            if (data != null) {

                try {
                    if (dataForImage.size >= 4) {
                        Toaster.shortToast("You can not add more than 4 images")
                    } else {
                        val thumbnail = data!!.extras!!.get("data") as Bitmap
                        dataForImage.add(thumbnail)
                        adapterR.notifyDataSetChanged()
                        fil.add(File(saveImage(thumbnail)))
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace();
                } catch (e: IOException) {
                    e.printStackTrace();
                }
            } else {
                Toaster.shortToast("Selection cancel")
            }
        }
    }

    private fun newImageList(tempImageList: ArrayList<Bitmap>): Collection<Bitmap> {
        tempImageList.addAll(tempImageList)
        for (i in 0 until tempImageList.size) {
            dataForImage.add(tempImageList.get(i))
        }
        return tempImageList
    }

    private fun getRealPathFromURI(contentURI: Uri): String? {
        Log.e("getRealPathFromURI: ", contentURI.toString())
        val filePath: String?
        val cursor =
            requireActivity().contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) {
            filePath = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            filePath = cursor.getString(idx)
            cursor.close()
        }
        return filePath
    }

    private fun showPictureAndCameraPopup() {
        showPicturePickerDialog(object : CallBack<Int>() {
            override fun onSuccess(t: Int?) {
                when (t) {
                    1 -> {
                        picturePickerDialog?.hide()
                        takePhotoFromCamera()
                    }
                    2 -> {
                        picturePickerDialog?.hide()

                        choosePhotoFromGallary()

                    }
                }
            }
        })
    }

    private fun takePhotoFromCamera() {


        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, camera)


    }


    private fun choosePhotoFromGallary() {

        if (Build.VERSION.SDK_INT < 19) {
            var intent = Intent()
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                Intent.createChooser(intent, "Choose Pictures"), gallery
            )

        } else { // For latest versions API LEVEL 19+
            var intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "image/*"
            startActivityForResult(intent, gallery);
        }

    }


    private fun getDriveLink() {

        if (!App.get().isConnected()) {
            popUpInternetConnectionDialog()
            hideLoader()
            return
        }

        var userData = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userData.getAddMediaLink().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()

                Log.e("onFailure", t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                if (response.code() == 200) {
                    val addMediaResponse =
                        Gson().fromJson(response.body(), AddMediaBean::class.java)
                    mediaLink = addMediaResponse.data.googleDriveLink.toString()
                } else {
                    //Toaster.somethingWentWrong()
                    Log.e("AddNotes", "Something went wrong while getting google drive link.")
                }
            }
        })
    }

}
















