package com.awakenwhitelabeling.dashboard.notes.noteDetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity
import com.awakenwhitelabeling.others.Cons
import com.awakenwhitelabeling.others.Toaster

class NotesDetailsActivity : BaseActivity() {
private var uData:String?=null
    companion object {
        const val EXTRA_Notes = "extra.workout.data"
        private const val EXTRA_FULL_DETAIL = "extra.Notes.full.detail"

        fun noteDetails(context: Context, data: Int) {
            context.startActivity(
                Intent(context, NotesDetailsActivity::class.java)
                    .putExtra(EXTRA_Notes, data)
                    .putExtra(EXTRA_FULL_DETAIL, true)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_notes_details)
      /*  title = findViewById(R.id.tvTitleND)
        details = findViewById(R.id.tvDetailsND)*/
        // ivImageND=findViewById(R.id.ivImageND)

       // parseBundle()
        val bundle= intent.extras
        val fragment= NotesDetailFragment()
        fragment.arguments=bundle
        supportFragmentManager.beginTransaction()
            .add(R.id.flNotesDetails, fragment)
            .commit()
    }
    private fun getUpdatedData() {

        if (intent.hasExtra(Cons.MEMBERSHIP_DATA)) {
            uData = intent.getSerializableExtra(Cons.MEMBERSHIP_DATA) as String

            Toaster.shortToast(uData.toString())




        }
    }


}