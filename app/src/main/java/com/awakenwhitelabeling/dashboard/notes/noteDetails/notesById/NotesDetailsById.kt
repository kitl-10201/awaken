package com.awakenwhitelabeling.dashboard.notes.noteDetails.notesById


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class NotesDetailsById(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: Data
) :Serializable {
    data class Data(
        @SerializedName("id")
        val id: Int,
        @SerializedName("user_id")
        val userId: Int,
        @SerializedName("notes_description")
        val notesDescription: String,
        @SerializedName("post_type")
        val postType: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("category")
        val category: String,
        @SerializedName("notes_image")
        val notesImage: List<String>,
        @SerializedName("publish_date")
        val publishDate: String,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("updated_at")
        val updatedAt: String
    ) : Serializable
}