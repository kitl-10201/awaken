package com.socreates.base

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.App
import com.awakenwhitelabeling.base.BaseFragment
import com.awakenwhitelabeling.base.perDialog.ReqPermissionDialog
import com.awakenwhitelabeling.others.CallBack
import com.awakenwhitelabeling.others.Cons.Companion.IMAGE_DIRECTORY
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


/**
 * This is abstract classes for Permission, which includes Camera , Write Storage, Read Storage permissions.
 * - Checking the permission.
 * - Checking the rational permission, one time permission.
 * - Abstract functions for PermissionGranted, Permission is disabled and Permission is denied.
 * - Showing the custom popup for permission Required.
 * */
abstract class PermissionFragment : BaseFragment() {

    protected val CAMERA = Manifest.permission.CAMERA
    protected val R_E_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE
    protected val W_E_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
    protected val RQ_CAMERA = 1004
    protected val PM_CAMERA = arrayOf(CAMERA, W_E_STORAGE, R_E_STORAGE)

    protected val PC_GALLERY = 1
    protected val PC_CAMERA = 2

    @StringRes
    private var vTitle: Int = 0
    private var vMessage: Int = 0
    private var PERMISSIONS: Array<String>? = null
    var RQ_CODE: Int = 0

    protected abstract fun onPermissionGranted(REQUESTED_FOR: Int)

    protected abstract fun onPermissionDisabled(REQUESTED_FOR: Int)

    protected abstract fun onPermissionDenied(REQUESTED_FOR: Int)


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        processOnRequestPermission(requestCode, permissions, grantResults)
    }

    protected fun hasPermission(
        permissions: Array<String>,
        requestedCode: Int,
        @StringRes reqTitle: Int,
        @StringRes reqDsc: Int
    ): Boolean {
        PERMISSIONS = permissions
        RQ_CODE = requestedCode
        vTitle = reqTitle
        vMessage = reqDsc

        /* check if OS is not smaller than MARSHMALLOW*/
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }

        /*check if granted @PERMISSIONS*/
        if (hasPermissions(*PERMISSIONS!!)) {
            return true
        }

        /*  can show permission requirement cause*/
        if (shouldShowRationale(PERMISSIONS!!)) {
            /*initiate dialog*/
            val permissionDialog =
                activity?.let {
                    ReqPermissionDialog(
                        it,
                        reqTitle,
                        reqDsc,
                        onPreviouslyDinedListener
                    )
                }
            /* show permission dialog*/
            permissionDialog?.show()
        } else {

            /*asking permission for 1st time*/
            activity?.let { ActivityCompat.requestPermissions(it, PERMISSIONS!!, requestedCode) }
        }
        return false

    }

    private fun hasPermissions(vararg permission: String): Boolean {
        for (PERMISSION in permission) {
            /* checking permission 1 by 1*/
            if (activity?.let {
                    ActivityCompat.checkSelfPermission(
                        it,
                        PERMISSION
                    )
                } == PackageManager.PERMISSION_DENIED
            ) {
                /* if any permission is not granted
                @return false*/
                return false
            }
        }
        /*All permission are granted*/
        return true
    }

    private fun processOnRequestPermission(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        var isAllowed = true
        for (p in grantResults) {
            if (p == PackageManager.PERMISSION_DENIED) {
                isAllowed = false
                break
            }
        }
        if (isAllowed) {
            onPermissionGranted(requestCode)
        } else if (!shouldShowRationale(permissions)) {

            val dialog = activity?.let {
                ReqPermissionDialog(
                    it,
                    vTitle,
                    vMessage,
                    onManuallyDisabledListener
                )
            }
            dialog?.show()

        } else {
            onPermissionDenied(requestCode)
        }


    }

    private fun shouldShowRationale(permission: Array<String>): Boolean {
        for (p in permission) {
            if (activity?.let {
                    ActivityCompat.shouldShowRequestPermissionRationale(
                        it,
                        p
                    )
                } == true) {
                return true
            }
        }
        return false
    }

    private val onPreviouslyDinedListener = object : ReqPermissionDialog.Listener {
        override fun onPositive() {
            activity?.let { ActivityCompat.requestPermissions(it, PERMISSIONS!!, RQ_CODE) }
        }

        override fun onNegative() {
            onPermissionDisabled(RQ_CODE)
        }
    }
    private val onManuallyDisabledListener = object : ReqPermissionDialog.Listener {
        override fun onPositive() {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", activity?.packageName, null)
            intent.data = uri
            startActivity(intent)
        }

        override fun onNegative() {
            onPermissionDisabled(RQ_CODE)
        }
    }
    var picturePickerDialog: Dialog? = null
    fun showPicturePickerDialog(callBack: CallBack<Int>) {
        picturePickerDialog = activity?.let { Dialog(it) }
        picturePickerDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        activity?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        picturePickerDialog?.setCancelable(true)
        picturePickerDialog?.window?.setBackgroundDrawable(ColorDrawable(resources.getColor(android.R.color.transparent)))
        picturePickerDialog?.setContentView(R.layout.item_select_camera_storage)

        picturePickerDialog?.findViewById<LinearLayout>(R.id.chooses_camera)?.setOnClickListener {
            callBack.onSuccess(1)
        }
        picturePickerDialog?.findViewById<LinearLayout>(R.id.chooses_gallery)?.setOnClickListener {
            callBack.onSuccess(2)
        }
        picturePickerDialog?.show()

    }


    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Log.e("Android 11", "saveImage: ")
            File(context?.getExternalFilesDir(null).toString() + IMAGE_DIRECTORY)
        } else {
            File(
                (App.get().filesDir).toString() + IMAGE_DIRECTORY
            )
        }
        Log.d("fee", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            Log.d("heel", wallpaperDirectory.toString())
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .timeInMillis).toString() + ".jpg")
            )

            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                requireContext(),
                arrayOf(f.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("tag", "File Saved::--->" + f.absolutePath)
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }


    protected fun hasCameraAndStoragePermission(): Boolean {
        return hasPermission(
            PM_CAMERA,
            RQ_CAMERA,
            R.string.title_camera,
            R.string.msg_camera
        )
    }

}