package com.awakenwhitelabeling.dashboard.profile.topRanking

import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity
import com.awakenwhitelabeling.dashboard.profile.topRanking.bean.TopUsersRankinBean
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class UserRankingActivity : BaseActivity() {


    lateinit var rcvTopRanking: RecyclerView
    lateinit var layoutManager: GridLayoutManager
    private val list = ArrayList<TopUsersRankinBean.Data>()
    private var mAdapter = TopRankingUserAdapter(list)


    lateinit var ivBackTR: ImageView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_ranking)

        rcvTopRanking = findViewById(R.id.rcvTopRanking)
        ivBackTR = findViewById(R.id.ivBackTR)
        swipeRefresh = findViewById(R.id.swipeRefresh)
        layoutManager = GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false);
        rcvTopRanking.layoutManager = layoutManager
        mAdapter = TopRankingUserAdapter(list)
        rcvTopRanking.adapter = mAdapter

        ivBackTR.setOnClickListener {
            this.finish()
        }

        swipeRefresh.setOnRefreshListener {
            getuserRanking()
            swipeRefresh.isRefreshing =
                false   // reset the SwipeRefreshLayout (stop the loading spinner)
        }

    }


    override fun onResume() {
        super.onResume()
        getuserRanking()

    }


    private fun getuserRanking() {
        showLoader()
        var userData = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userData.topUserRanking().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()

                Log.e("onFailure", t.localizedMessage)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                hideLoader()
                if (response.code() == 200) {

                    Log.d("TopRanking","TopRanking user resp: "+response.body())
                    Log.d("TopRanking","TopRanking user resp: "+response.body()?.size())
                    val userData = Gson().fromJson(response.body(), TopUsersRankinBean::class.java)
                    list.clear()
                    list.addAll(userData.data)
                    rcvTopRanking.layoutManager = layoutManager
                    mAdapter = TopRankingUserAdapter(list)
                    rcvTopRanking.adapter = mAdapter

                } else {
                    Toaster.somethingWentWrong()
                }
            }
        })
    }

}