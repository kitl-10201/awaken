package com.awakenwhitelabeling.dashboard.profile.topRanking.bean

import com.google.gson.annotations.SerializedName

data class NotificationCount(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: Data
) {
    data class Data(
        @SerializedName("count")
        val count: Int
    )
}