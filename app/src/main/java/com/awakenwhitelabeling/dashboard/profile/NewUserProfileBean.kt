package com.awakenwhitelabeling.dashboard.profile


import com.google.gson.annotations.SerializedName

data class NewUserProfileBean(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("about_yourself")
        val aboutYourself: Any,
        @SerializedName("active_status")
        val activeStatus: Int,
        @SerializedName("address_line_1")
        val addressLine1: String,
        @SerializedName("address_line_2")
        val addressLine2: String,
        @SerializedName("city")
        val city: String,
        @SerializedName("coins_earned")
        val coinsEarned: Int,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("device_type")
        val deviceType: String,
        @SerializedName("dob")
        val dob: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("email_verified_at")
        val emailVerifiedAt: Any,
        @SerializedName("facebook_link")
        val facebookLink: String,
        @SerializedName("fcm_token")
        val fcmToken: String,
        @SerializedName("first_name")
        val firstName: String,
        @SerializedName("ibo_number")
        val iboNumber: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("image_path")
        val imagePath: String,
        @SerializedName("instagram_link")
        val instagramLink: String,
        @SerializedName("is_deleted")
        val isDeleted: Int,
        @SerializedName("last_name")
        val lastName: String,
        @SerializedName("last_updated_by")
        val lastUpdatedBy: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("phone")
        val phone: String,
        @SerializedName("previous_email")
        val previousEmail: Any,
        @SerializedName("qrcode_image_path")
        val qrcodeImagePath: String,
        @SerializedName("register_type")
        val registerType: String,
        @SerializedName("socialLogID")
        val socialLogID: Any,
        @SerializedName("state")
        val state: String,
        @SerializedName("updated_at")
        val updatedAt: String,
        @SerializedName("website_link")
        val websiteLink: String,
        @SerializedName("zip")
        val zip: String
    )
}