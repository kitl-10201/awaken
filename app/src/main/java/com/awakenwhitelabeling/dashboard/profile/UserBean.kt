package com.awakenwhitelabeling.dashboard.profile

data class UserBean(
    val `data`: Data,
    val message: String,
    val status: String
) {
    data class Data(
        val about_yourself: Any,
        val active_status: Int,
        val address_line_1: String,
        val address_line_2: String,
        val coins_earned: Int,
        val created_at: String,
        val device_type: String,
        val dob: Any,
        val email: String,
        val email_verified_at: Any,
        val fcm_token: Any,
        val first_name: String,
        val ibo_number: String,
        val id: Int,
        val image_path: String,
        val last_name: String,
        val last_updated_by: Int,
        val name: String,
        val phone: Any,
        val register_type: String,
        val socialLogID: Any,
        val state: String,
        val updated_at: String,
        val zip: String
    )
}