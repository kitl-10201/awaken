package com.awakenwhitelabeling.dashboard.profile.topRanking

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.dashboard.profile.topRanking.bean.TopUsersRankinBean
import com.awakenwhitelabeling.others.PicassoUtil

class TopRankingUserAdapter(var userData: MutableList<TopUsersRankinBean.Data>?) :
    RecyclerView.Adapter<TopRankingUserAdapter.ViewHolder>() {

    var context: Context? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TopRankingUserAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.top_user_ranking_list_for_activity, parent, false)
        return TopRankingUserAdapter.ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mData = userData?.get(position)
        holder.name.text = mData?.name
        holder.rank.text = "Rank: " + mData?.rank.toString()
        holder.points.text = " " + mData?.totalReward.toString()


        if (mData != null) {
            if (!mData.imagePath.isNullOrEmpty()) {
                PicassoUtil.loadProfileImage(holder.image, mData.imagePath)
            }
        }
        if (position > 19) {
            val params: ViewGroup.LayoutParams = holder.rlMainTopRanking.getLayoutParams()
            params.height = 0
            holder.rlMainTopRanking.layoutParams = params
        }
    }

    override fun getItemCount(): Int {
        return userData?.size ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val image = itemView.findViewById<ImageView>(R.id.topUserImage)
        val name = itemView.findViewById<TextView>(R.id.topUserName)
        val rank = itemView.findViewById<TextView>(R.id.topUserRank)
        val points = itemView.findViewById<TextView>(R.id.topUserPoints)
        val rlMainTopRanking = itemView.findViewById<ConstraintLayout>(R.id.rlMainTopRanking)


    }

}