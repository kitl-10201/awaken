package com.awakenwhitelabeling.dashboard.profile.topRanking.bean


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TopUsersRankinBean(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
):Serializable {
    data class Data(
        @SerializedName("address_line_1")
        val addressLine1: String,
        @SerializedName("city")
        val city: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("ibo_number")
        val iboNumber: String,
        @SerializedName("image_path")
        val imagePath: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("rank")
        val rank: Int,
        @SerializedName("state")
        val state: String,
        @SerializedName("total_reward")
        val totalReward: Int,
        @SerializedName("zip")
        val zip: String
    ):Serializable
}