package com.awakenwhitelabeling.dashboard.changePassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.awakenwhitelabeling.R
import com.awakenwhitelabeling.base.BaseActivity
import com.awakenwhitelabeling.others.CustomWatcher
import com.awakenwhitelabeling.others.ErrorUtils
import com.awakenwhitelabeling.others.Toaster
import com.awakenwhitelabeling.others.Utils
import com.awakenwhitelabeling.retrofit.RetrofitApi
import com.awakenwhitelabeling.retrofit.RetrofitClient
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePassword : BaseActivity() {

    lateinit var tilOldPassword: TextInputLayout
    lateinit var etOldPassword: EditText
    lateinit var tilNewPassword: TextInputLayout
    lateinit var etNewPassword: EditText
    lateinit var tilConfirmPassword: TextInputLayout
    lateinit var etConfirmPassword: EditText
    lateinit var btChangePassword: Button
    lateinit var progressBar: ProgressBar
    lateinit var ivBack: ImageView

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, ChangePassword::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_change_password)
        progressBar = findViewById(R.id.progressBar)
        ivBack = findViewById(R.id.ivBackCHange)
        tilOldPassword = findViewById(R.id.tilOldPassword)
        tilNewPassword = findViewById(R.id.tilNewPassword)
        tilConfirmPassword = findViewById(R.id.tilConfirmPassword)

        etOldPassword = findViewById(R.id.etOldPassword)
        etNewPassword = findViewById(R.id.etNewPassword)
        etConfirmPassword = findViewById(R.id.etConfirmPassword)

        btChangePassword = findViewById(R.id.btChangePassword)
        btChangePassword.setOnClickListener {
            if (isValid()) {
                if (etNewPassword.text.toString().trim() == etConfirmPassword.text.toString()
                        .trim()
                ) {
                    changePassword()
                } else {
                    tilConfirmPassword.error = "Confirm password must same as New Password"
                }
            }
        }
        ivBack.setOnClickListener {
            onBackPressed()
        }
        warnings()
    }

    private fun changePassword() {
        //showProgressView()
        showLoader()
        var changePasswordApi = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        val hashMap = HashMap<String, Any>()
        hashMap["oldPassword"] = etOldPassword.text.toString()
        hashMap["password"] = etNewPassword.text.toString()
        hashMap["c_password"] = etConfirmPassword.text.toString()
        Log.d("etOldPswd", etOldPassword.text.toString())
        Log.d("nNew", etOldPassword.text.toString())
        Log.d("cNew", etOldPassword.text.toString())

        changePasswordApi.changePassword(hashMap).enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                hideLoader()
                ErrorUtils.onFailure(t)
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                //hideProgressView()
                hideLoader()
                if (response.isSuccessful) {
                    Toaster.longToast("Password Updated Successfully")
                    Log.d("ppd", "ppd" + response.body().toString())
                    finish()
                    /* onBackPressed()
                     finish()*/

                } else {
                    val jObjError = JSONObject(response.errorBody()?.string())
                    Toaster.shortToast(jObjError.getString("message"))
                    Log.d("checkerrrrrr", "Checkerrrrr" + response.body())
                }
            }
        })
    }

    private fun warnings() {

        etOldPassword.addTextChangedListener(
            CustomWatcher(
                etOldPassword,
                tilOldPassword,

                "Old password required",
                CustomWatcher.EditTextType.FirstName
            )
        )


        etNewPassword.addTextChangedListener(
            CustomWatcher(
                etNewPassword,
                tilNewPassword,

                "New password required",
                CustomWatcher.EditTextType.FirstName
            )

        )

        etConfirmPassword.addTextChangedListener(
            CustomWatcher(
                etConfirmPassword,
                tilConfirmPassword,
                "Confirm password required",
                CustomWatcher.EditTextType.FirstName
            )

        )


    }

    private fun isValid(): Boolean {
        var validation = false

        when {
            Utils.isEmptyString(etOldPassword.text.toString().trim()) -> {
                tilOldPassword.error = "Old password required"
                validation = false
            }

            Utils.isEmptyString(etNewPassword.text.toString().trim()) -> {
                tilNewPassword.error = "New password required"
                validation = false

            }

            Utils.isEmptyString(etNewPassword.text.toString().trim()) -> {
                tilNewPassword.error = "Confirm password required"
                validation = false

            }

            Utils.isPasswordValid(etNewPassword.text.toString().trim()) -> {
                tilNewPassword.error = "New password must be at least 8 characters"
                validation = false
            }

            Utils.isPasswordValid(etConfirmPassword.text.toString().trim()) -> {
                tilConfirmPassword.error = "Confirm password must be at least 8 characters"
                validation = false
            }

/*

            Utils.isEmptyString(etConfirmPassword.text.toString().trim())
                    && !TextUtils.equals(
                etNewPassword.text.toString().trim(),
                etConfirmPassword.text.toString().trim()
            ) -> {
                tilConfirmPassword.error = "Confirm Password should be same as New Password"
                validation = false
            }
*/

            else -> {
                validation = true
            }

        }

        return validation

    }

}
